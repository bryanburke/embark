////////////////////////////////////////////////////////////////////////////////
// Bryan Burke, Joshua Lambert, and Elizabeth Rothermel                       //
// Group 4                                                                    //
// INFX 499                                                                   //
// Fall 2015                                                                  //
// University of Louisiana at Lafayette                                       //
////////////////////////////////////////////////////////////////////////////////

import java.io.IOException;
import java.util.ArrayList;

/**
 * Performs rudimentary unit testing on <code>Airport</code> objects.
 *
 * @author Bryan Burke
 */
public class TestAirport
{
  //////////////////////////////////////////////////////////////////////////////
  // METHODS                                                                  //
  //////////////////////////////////////////////////////////////////////////////

  /**
   * Creates and tests <code>Airport</code> objects.
   *
   * @param args command-line arguments; unused
   */
  public static void main(String[] args) throws IOException
  {
    ////////////////////////////////////////////////////////////////////////////
    // TEST CONSTRUCTORS                                                      //
    ////////////////////////////////////////////////////////////////////////////

    System.out.println("== BEGIN CONSTRUCTOR TESTS ==");

    ArrayList<Airport> airports = new ArrayList<>(3);

    // Airport(String, String, String, String, String)
    airports.add(new Airport("ATL",
                             "Hartsfield–Jackson Atlanta International Airport",
                             "Atlanta",
                             "GA",
                             "US"));
    System.out.println("  Airport ATL constructed as airports[0].");

    airports.add(new Airport("DFW",
                             "Dallas/Fort Worth International Airport",
                             "Dallas/Fort Worth",
                             "TX",
                             "US"));
    System.out.println("  Airport DFW constructed as airports[1].");

    // Airport(Airport)
    airports.add(new Airport(airports.get(0)));
    System.out.println("  Airport ATL copy constructed as airports[2].");

    System.out.println("== END CONSTRUCTOR TESTS ==");
    System.out.println();



    ////////////////////////////////////////////////////////////////////////////
    // TEST METHODS                                                           //
    ////////////////////////////////////////////////////////////////////////////

    System.out.println("== BEGIN METHOD TESTS ==");

    // getID()
    System.out.println("= getID() =");
    for (int i = 0; i < airports.size(); ++i)
      System.out.printf("  [%d] : %s\n",
                        i,
                        airports.get(i).getID());
    System.out.print("Verify the results and press ENTER to continue.");
    System.in.read();

    // getName()
    System.out.println("= getName() =");
    for (int i = 0; i < airports.size(); ++i)
      System.out.printf("  [%d] : %s\n",
                        i,
                        airports.get(i).getName());
    System.out.print("Verify the results and press ENTER to continue.");
    System.in.read();

    // getCity()
    System.out.println("= getCity() =");
    for (int i = 0; i < airports.size(); ++i)
      System.out.printf("  [%d] : %s\n",
                        i,
                        airports.get(i).getCity());
    System.out.print("Verify the results and press ENTER to continue.");
    System.in.read();

    // getRegion()
    System.out.println("= getRegion() =");
    for (int i = 0; i < airports.size(); ++i)
      System.out.printf("  [%d] : %s\n",
                        i,
                        airports.get(i).getRegion());
    System.out.print("Verify the results and press ENTER to continue.");
    System.in.read();

    // getCountry()
    System.out.println("= getCountry() =");
    for (int i = 0; i < airports.size(); ++i)
      System.out.printf("  [%d] : %s\n",
                        i,
                        airports.get(i).getCountry());
    System.out.print("Verify the results and press ENTER to continue.");
    System.in.read();

    // equals(Airport)
    System.out.println("= equals([0]) =");
    for (int i = 0; i < airports.size(); ++i)
      System.out.printf("  [%d] : %s\n",
                        i,
                        airports.get(i).equals(airports.get(0)));
    System.out.print("Verify the results and press ENTER to continue.");
    System.in.read();

    // toString()
    System.out.println("= toString() =");
    for (int i = 0; i < airports.size(); ++i)
      System.out.printf("  [%d] : %s\n",
                        i,
                        airports.get(i).toString());
    System.out.print("Verify the results and press ENTER to continue.");
    System.in.read();

    System.out.println("== END METHOD TESTS ==");
  }
}
