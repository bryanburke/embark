////////////////////////////////////////////////////////////////////////////////
// Bryan Burke, Joshua Lambert, and Elizabeth Rothermel                       //
// Group 4                                                                    //
// INFX 499                                                                   //
// Fall 2015                                                                  //
// University of Louisiana at Lafayette                                       //
////////////////////////////////////////////////////////////////////////////////

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

/**
 * Performs rudimentary unit testing on <code>DBEncoder</code> methods.
 *
 * @author Bryan Burke
 */
public class TestDBEncoder
{
  //////////////////////////////////////////////////////////////////////////////
  // METHODS                                                                  //
  //////////////////////////////////////////////////////////////////////////////

  /**
   * Calls <code>DBEncoder</code> methods to encode data for use with the
   * database.
   *
   * @param args command-line arguments; unused
   */
  public static void main(String[] args) throws ParseException
  {
    ////////////////////////////////////////////////////////////////////////////
    // TEST METHODS                                                           //
    ////////////////////////////////////////////////////////////////////////////

    System.out.println("== BEGIN METHOD TESTS ==");

    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

    // encodeDayOfWeek(Date)
    ArrayList<Date> dates = new ArrayList<>(7);
    dates.add(new Date(dateFormat.parse("2015-12-20").getTime())); // Sunday.
    dates.add(new Date(dateFormat.parse("2015-12-21").getTime())); // Monday.
    dates.add(new Date(dateFormat.parse("2015-12-22").getTime())); // Tuesday.
    dates.add(new Date(dateFormat.parse("2015-12-23").getTime())); // Wednesday.
    dates.add(new Date(dateFormat.parse("2015-12-24").getTime())); // Thursday.
    dates.add(new Date(dateFormat.parse("2015-12-25").getTime())); // Friday.
    dates.add(new Date(dateFormat.parse("2015-12-26").getTime())); // Saturday

    for (Date date : dates) {
      System.out.printf("= encodeDayOfWeek(\"%s\") =\n",
                        date);
      System.out.println(DBEncoder.encodeDayOfWeek(date));
    }

    System.out.println("== END METHOD TESTS ==");
  }
}
