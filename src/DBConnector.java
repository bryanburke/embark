////////////////////////////////////////////////////////////////////////////////
// Bryan Burke, Joshua Lambert, and Elizabeth Rothermel                       //
// Group 4                                                                    //
// INFX 499                                                                   //
// Fall 2015                                                                  //
// University of Louisiana at Lafayette                                       //
////////////////////////////////////////////////////////////////////////////////

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Provides methods, constants, and variables to manage the database connection.
 *
 * @author Bryan Burke
 */
public final class DBConnector
{
  //////////////////////////////////////////////////////////////////////////////
  // CLASS VARIABLES                                                          //
  //////////////////////////////////////////////////////////////////////////////

  /**
   * Database connector URL for JDBC.
   */
  public static final String JDBC_URL = "jdbc:derby:db";



  /**
   * Error message describing the most recent database access error.
   */
  public static String error = null;



  // JDBC Connection reference variable for accessing the database.
  private static Connection db = null;



  //////////////////////////////////////////////////////////////////////////////
  // CONSTRUCTORS                                                             //
  //////////////////////////////////////////////////////////////////////////////

  // This class is non-constructable.
  private DBConnector() {}



  //////////////////////////////////////////////////////////////////////////////
  // METHODS                                                                  //
  //////////////////////////////////////////////////////////////////////////////

  /**
   * Establishes a connection to the database and initializes <code>db</code>
   * with a <code>Connection</code> object for accessing it.
   * <p>
   * If connection is successful, returns <code>true</code>.
   * <p>
   * If connection fails, returns <code>false</code> and assigns a user-friendly
   * error message to <code>error</code>.
   *
   * @return <code>true</code> if connection succeeds; <code>false</code>
   *         otherwise
   */
  public static boolean connect()
  {
    try {
      // Attempt a connection to the database.
      if (db == null) // Do not re-open an existing connection.
        db = DriverManager.getConnection(JDBC_URL);

      // No JDBC errors occurred.
      return true;
    }
    catch (SQLException e) { // A JDBC error occurred.
      if (e.getSQLState().equals("XJ004"))
        error = "Unable to establish a connection to the database.";
      else {
        error = "He's dead, Jim. Please report this error.";
        e.printStackTrace(); // For debugging only.
      }

      return false;
    }
  }

  /**
   * Closes the connection to the database and frees all allocated JDBC
   * resources.
   * <p>
   * After this method is called, <code>db</code> is <code>null</code>.
   */
  public static void disconnect()
  {
    // Close connection and clean up.
    if (db != null) {
      try {
        db.close();
      }
      catch (SQLException e) {} // Nothing to do if already closed.

      db = null;
    }
  }

  /**
   * Creates a <code>Statement</code> object suitable for issuing most SQL
   * commands, optionally with support for a scrollable <code>ResultSet</code>.
   * <p>
   * Returns a <code>Statement</code> object with or without support for
   * scrollable results, depending on the value of <code>hasScrollable</code>.
   * <p>
   * If the database connection is inactive, returns <code>null</code> and
   * assigns a user-friendly error message to <code>error</code>.
   *
   * @param hasScrollable a <code>boolean</code> indicating whether or not to
   *                      enable support for scrollable results
   * @return              a <code>Statement</code> object if connection is
   *                      active; <code>null</code> otherwise
   */
  public static Statement createStatement(boolean hasScrollable)
  {
    try {
      Statement statement = null;

      // Check to see if database connection is active.
      if (db != null) { // Connection is active.
        if (hasScrollable)
          statement = db.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
                                         ResultSet.CONCUR_READ_ONLY);
        else
          statement = db.createStatement();
      }
      else // Connection is not active.
        error = "There is no active connection to the database.";

      // No JDBC errors occurred.
      return statement;
    }
    catch (SQLException e) { // A JDBC error occurred.
      error = "He's dead, Jim. Please report this error.";
      e.printStackTrace(); // For debugging only.

      return null;
    }
  }

  /**
   * Creates a <code>PreparedStatement</code> object suitable for issuing SQL
   * <code>INSERT</code> commands that automatically generate primary key
   * values.
   * <p>
   * Returns a <code>PreparedStatement</code> object with support for returning
   * auto-generated keys resulting from the specified SQL <code>INSERT</code>.
   * <p>
   * If the database connection is inactive, returns <code>null</code> and
   * assigns a user-friendly error message to <code>error</code>.
   *
   * @param sql a string containing the specified SQL <code>INSERT</code>
   *            command
   * @return    a <code>PreparedStatment</code> object if connection is active;
   *            <code>null</code> otherwise
   */
  public static PreparedStatement createPreparedStatement(String sql)
  {
    try {
      PreparedStatement statement = null;

      // Check to see if database connection is active.
      if (db != null) // Connection is active.
        statement = db.prepareStatement(sql,
                                        Statement.RETURN_GENERATED_KEYS);
      else // Connection is not active.
        error = "There is no active connection to the database.";

      // No JDBC errors occurred.
      return statement;
    }
    catch (SQLException e) { // A JDBC error occurred.
      error = "He's dead, Jim. Please report this error.";
      e.printStackTrace(); // For debugging only.

      return null;
    }
  }

  /**
   * Enables transactions for SQL commands that modify the database.
   * <p>
   * If the database connection is active, returns <code>true</code>.
   * <p>
   * If the database connection is inactive, returns <code>false</code> and
   * assigns a user-friendly error message to <code>error</code>.
   *
   * @return <code>true</code> if connection is active; <code>false</code>
   *         otherwise
   */
  public static boolean enableTransactions()
  {
    try {
      boolean isEnabled = true;

      // Check to see if database connection is active.
      if (db != null) // Connection is active.
        db.setAutoCommit(false);
      else { // Connection is not active.
        error = "There is no active connection to the database.";

        isEnabled = false;
      }

      // No JDBC errors occurred.
      return isEnabled;
    }
    catch (SQLException e) { // A JDBC error occurred.
      error = "He's dead, Jim. Please report this error.";
      e.printStackTrace(); // For debugging only.

      return false;
    }
  }

  /**
   * Disables transactions for SQL commands that modify the database.
   * <p>
   * If the database connection is active, returns <code>true</code>.
   * <p>
   * If the database connection is inactive, returns <code>false</code> and
   * assigns a user-friendly error message to <code>error</code>.
   *
   * @return <code>true</code> if connection is active; <code>false</code>
   *         otherwise
   */
  public static boolean disableTransactions()
  {
    try {
      boolean isDisabled = true;

      // Check to see if database connection is active.
      if (db != null) // Connection is active.
        db.setAutoCommit(true);
      else { // Connection is not active.
        error = "There is no active connection to the database.";

        isDisabled = false;
      }

      // No JDBC errors occurred.
      return isDisabled;
    }
    catch (SQLException e) { // A JDBC error occurred.
      error = "He's dead, Jim. Please report this error.";
      e.printStackTrace(); // For debugging only.

      return false;
    }
  }

  /**
   * Commits all pending database updates in the current transaction.
   */
  public static void commitTransaction()
  {
    try {
      // Attempt to commit the transaction.
      db.commit();
    }
    catch (SQLException e) { // A JDBC error occurred.
      error = "He's dead, Jim. Please report this error.";
      e.printStackTrace(); // For debugging only.
    }
  }

  /**
   * Cancels all pending database updates in the current transaction.
   */
  public static void rollbackTransaction()
  {
    try {
      // Attempt to rollback the transaction.
      db.rollback();
    }
    catch (SQLException e) { // A JDBC error occurred.
      error = "He's dead, Jim. Please report this error.";
      e.printStackTrace(); // For debugging only.
    }
  }
}
