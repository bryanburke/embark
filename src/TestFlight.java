////////////////////////////////////////////////////////////////////////////////
// Bryan Burke, Joshua Lambert, and Elizabeth Rothermel                       //
// Group 4                                                                    //
// INFX 499                                                                   //
// Fall 2015                                                                  //
// University of Louisiana at Lafayette                                       //
////////////////////////////////////////////////////////////////////////////////

import java.io.IOException;
import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

/**
 * Performs rudimentary unit testing on <code>Flight</code> objects.
 *
 * @author Bryan Burke
 */
public class TestFlight
{
  //////////////////////////////////////////////////////////////////////////////
  // METHODS                                                                  //
  //////////////////////////////////////////////////////////////////////////////

  /**
   * Creates and tests <code>Flight</code> objects.
   *
   * @param args command-line arguments; unused
   */
  public static void main(String[] args) throws IOException, ParseException
  {
    ////////////////////////////////////////////////////////////////////////////
    // TEST CONSTRUCTORS                                                      //
    ////////////////////////////////////////////////////////////////////////////

    System.out.println("== BEGIN CONSTRUCTOR TESTS ==");

    // Prerequisite objects.
    Airline aa  = new Airline("AA",
                              "American Airlines");
    Airline ua  = new Airline("UA",
                              "United Airlines");
    Airport lft = new Airport("LFT",
                              "Lafayette Regional Airport",
                              "Lafayette",
                              "LA",
                              "US");
    Airport dfw = new Airport("DFW",
                              "Dallas/Fort Worth International Airport",
                              "Dallas/Fort Worth",
                              "TX",
                              "US");
    Airport atl = new Airport("ATL",
                             "Hartsfield–Jackson Atlanta International Airport",
                              "Atlanta",
                              "GA",
                              "US");
    Airport ord = new Airport("ORD",
                              "O'Hare International Airport",
                              "Chicago",
                              "IL",
                              "US");

    ArrayList<Flight> flights    = new ArrayList<>(3);
    SimpleDateFormat  timeFormat = new SimpleDateFormat("HH:mm:ss");

    // Flight(String, Airline, Airport, Airport, String, Time, Time, int,
    //        double, double)
    flights.add(new Flight("3108",
                           aa,
                           lft,
                           dfw,
                           "NMTWRFS",
                           new Time(timeFormat.parse("17:55:00").getTime()),
                           new Time(timeFormat.parse("19:32:00").getTime()),
                           101,
                           144.0,
                           129.6));
    System.out.println("  Flight AA 3108 constructed as flights[0].");

    flights.add(new Flight("241",
                           ua,
                           atl,
                           ord,
                           "-MTWRF-",
                           new Time(timeFormat.parse("10:10:00").getTime()),
                           new Time(timeFormat.parse("11:15:00").getTime()),
                           191,
                           88,
                           79.2));
    System.out.println("  Flight UA 241 constructed as flights[1].");

    // Flight(Flight)
    flights.add(new Flight(flights.get(0)));
    System.out.println("  Flight AA 3108 copy constructed as flights[2].");

    System.out.println("== END CONSTRUCTOR TESTS ==");
    System.out.println();



    ////////////////////////////////////////////////////////////////////////////
    // TEST METHODS                                                           //
    ////////////////////////////////////////////////////////////////////////////

    System.out.println("== BEGIN METHOD TESTS ==");

    // getID()
    System.out.println("= getID() =");
    for (int i = 0; i < flights.size(); ++i)
      System.out.printf("  [%d] : %s\n",
                        i,
                        flights.get(i).getID());
    System.out.print("Verify the results and press ENTER to continue.");
    System.in.read();

    // getAirline()
    System.out.println("= getAirline() =");
    for (int i = 0; i < flights.size(); ++i)
      System.out.printf("  [%d] : %s\n",
                        i,
                        flights.get(i).getAirline());
    System.out.print("Verify the results and press ENTER to continue.");
    System.in.read();

    // getOriginAirport()
    System.out.println("= getOriginAirport() =");
    for (int i = 0; i < flights.size(); ++i)
      System.out.printf("  [%d] : %s\n",
                        i,
                        flights.get(i).getOriginAirport());
    System.out.print("Verify the results and press ENTER to continue.");
    System.in.read();

    // getDestinationAirport()
    System.out.println("= getDestinationAirport() =");
    for (int i = 0; i < flights.size(); ++i)
      System.out.printf("  [%d] : %s\n",
                        i,
                        flights.get(i).getDestinationAirport());
    System.out.print("Verify the results and press ENTER to continue.");
    System.in.read();

    // getDepartureDays()
    System.out.println("= getDepartureDays() =");
    for (int i = 0; i < flights.size(); ++i)
      System.out.printf("  [%d] : %s\n",
                        i,
                        flights.get(i).getDepartureDays());
    System.out.print("Verify the results and press ENTER to continue.");
    System.in.read();

    // getDepartureTime()
    System.out.println("= getDepartureTime() =");
    for (int i = 0; i < flights.size(); ++i)
      System.out.printf("  [%d] : %s\n",
                        i,
                        flights.get(i).getDepartureTime());
    System.out.print("Verify the results and press ENTER to continue.");
    System.in.read();

    // getArrivalTime()
    System.out.println("= getArrivalTime() =");
    for (int i = 0; i < flights.size(); ++i)
      System.out.printf("  [%d] : %s\n",
                        i,
                        flights.get(i).getArrivalTime());
    System.out.print("Verify the results and press ENTER to continue.");
    System.in.read();

    // getCapacity()
    System.out.println("= getCapacity() =");
    for (int i = 0; i < flights.size(); ++i)
      System.out.printf("  [%d] : %d\n",
                        i,
                        flights.get(i).getCapacity());
    System.out.print("Verify the results and press ENTER to continue.");
    System.in.read();

    // getFare()
    System.out.println("= getFare() =");
    for (int i = 0; i < flights.size(); ++i)
      System.out.printf("  [%d] : %.2f\n",
                        i,
                        flights.get(i).getFare());
    System.out.print("Verify the results and press ENTER to continue.");
    System.in.read();

    // getReserveFare()
    System.out.println("= getReserveFare() =");
    for (int i = 0; i < flights.size(); ++i)
      System.out.printf("  [%d] : %.2f\n",
                        i,
                        flights.get(i).getReserveFare());
    System.out.print("Verify the results and press ENTER to continue.");
    System.in.read();

    // equals(Flight)
    System.out.println("= equals([0]) =");
    for (int i = 0; i < flights.size(); ++i)
      System.out.printf("  [%d] : %s\n",
                        i,
                        flights.get(i).equals(flights.get(0)));
    System.out.print("Verify the results and press ENTER to continue.");
    System.in.read();

    // toString()
    System.out.println("= toString() =");
    for (int i = 0; i < flights.size(); ++i)
      System.out.printf("  [%d] : %s\n",
                        i,
                        flights.get(i).toString());
    System.out.print("Verify the results and press ENTER to continue.");
    System.in.read();

    System.out.println("== END METHOD TESTS ==");
  }
}
