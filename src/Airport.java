////////////////////////////////////////////////////////////////////////////////
// Bryan Burke, Joshua Lambert, and Elizabeth Rothermel                       //
// Group 4                                                                    //
// INFX 499                                                                   //
// Fall 2015                                                                  //
// University of Louisiana at Lafayette                                       //
////////////////////////////////////////////////////////////////////////////////

/**
 * Provides an in-memory mapping of a tuple from the <code>airport</code>
 * database relation.
 *
 * @author Bryan Burke
 */
public class Airport
{
  //////////////////////////////////////////////////////////////////////////////
  // CLASS VARIABLES                                                          //
  //////////////////////////////////////////////////////////////////////////////

  /**
   * Format specifier for <code>airport.airport_id</code>.
   */
  public static final String FORMAT_ID      = "%3.3s";
  /**
   * Format specifier for <code>airport.name</code>.
   */
  public static final String FORMAT_NAME    = "%30.30s";
  /**
   * Format specifier for <code>airport.city</code>.
   */
  public static final String FORMAT_CITY    = "%20.20s";
  /**
   * Format specifier for <code>airport.region</code>.
   */
  public static final String FORMAT_REGION  = "%2.2s";
  /**
   * Format specifier for <code>airport.country</code>.
   */
  public static final String FORMAT_COUNTRY = "%2.2s";



  //////////////////////////////////////////////////////////////////////////////
  // INSTANCE VARIABLES                                                       //
  //////////////////////////////////////////////////////////////////////////////

  private String id_;      // Airport identifier.
  private String name_;    // Official name.
  private String city_;    // Major city served by airport.
  private String region_;  // Major region served by airport.
  private String country_; // Country where airport is located.



  //////////////////////////////////////////////////////////////////////////////
  // CONSTRUCTORS                                                             //
  //////////////////////////////////////////////////////////////////////////////

  /**
   * Creates an object of this class with all instance variables initialized.
   * <p>
   * This constructor is used by <code>DBMapper.getAirport(String)</code> and
   * should not be called directly by frontend code.
   *
   * @param id      the unique airport identifier
   * @param name    the official name of the airport
   * @param city    the major city served by the airport
   * @param region  the major region served by the airport
   * @param country the country where the airport is located
   */
  public Airport(String id,
                 String name,
                 String city,
                 String region,
                 String country)
  {
    id_      = id;
    name_    = name;
    city_    = city;
    region_  = region;
    country_ = country;
  }

  /**
   * Creates a deep copy of an object of this class.
   *
   * @param other the object whose state should be copied
   */
  public Airport(Airport other)
  {
    this.id_      = other.id_;
    this.name_    = other.name_;
    this.city_    = other.city_;
    this.region_  = other.region_;
    this.country_ = other.country_;
  }



  //////////////////////////////////////////////////////////////////////////////
  // METHODS                                                                  //
  //////////////////////////////////////////////////////////////////////////////

  /**
   * Returns the airport's IATA code.
   *
   * @return the airport identifier
   */
  public String getID()
  {
    return id_;
  }

  /**
   * Returns the airport's official name.
   *
   * @return the official name
   */
  public String getName()
  {
    return name_;
  }

  /**
   * Returns the city's name.
   *
   * @return the major city served by the airport
   */
  public String getCity()
  {
    return city_;
  }

  /**
   * Returns the region's ISO 3166-2 code.
   *
   * @return the major region served by the airport
   */
  public String getRegion()
  {
    return region_;
  }

  /**
   * Returns the country's ISO 3166-1 alpha-2 code.
   *
   * @return the country where the airport is located
   */
  public String getCountry()
  {
    return country_;
  }



  /**
   * Peforms an equality comparison on two objects of this class.
   *
   * @param other the object whose state should be compared with the state of
   *              this object
   * @return      <code>true</code> if the object states are equal;
   *              <code>false</code> otherwise
   */
  public boolean equals(Airport other)
  {
    boolean equalsID      = this.id_.equals(other.id_);
    boolean equalsName    = this.name_.equals(other.name_);
    boolean equalsCity    = this.city_.equals(other.city_);
    boolean equalsRegion  = this.region_.equals(other.region_);
    boolean equalsCountry = this.country_.equals(other.country_);

    if (equalsID
        && equalsName
        && equalsCity
        && equalsRegion
        && equalsCountry)
      return true;
    else
      return false;
  }

  /**
   * Returns a string representation of this object.
   *
   * @return a string describing the state of this object
   */
  public String toString()
  {
    return String.format(FORMAT_ID, id_)
         + "   "
         + String.format(FORMAT_NAME, name_)
         + "   "
         + String.format(FORMAT_CITY, city_)
         + "   "
         + String.format(FORMAT_REGION, region_)
         + "   "
         + String.format(FORMAT_COUNTRY, country_);
  }
}
