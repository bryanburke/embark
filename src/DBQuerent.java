////////////////////////////////////////////////////////////////////////////////
// Bryan Burke, Joshua Lambert, and Elizabeth Rothermel                       //
// Group 4                                                                    //
// INFX 499                                                                   //
// Fall 2015                                                                  //
// University of Louisiana at Lafayette                                       //
////////////////////////////////////////////////////////////////////////////////

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;

/**
 * Provides methods to query the database, parse the results, and return data
 * needed by the frontend code.
 *
 * @author Bryan Burke
 */
public final class DBQuerent
{
  //////////////////////////////////////////////////////////////////////////////
  // CLASS VARIABLES                                                          //
  //////////////////////////////////////////////////////////////////////////////

  /**
   * Error message describing the most recent database access error.
   */
  public static String error = null;



  //////////////////////////////////////////////////////////////////////////////
  // CONSTRUCTORS                                                             //
  //////////////////////////////////////////////////////////////////////////////

  // This class is non-constructable.
  private DBQuerent() {}



  //////////////////////////////////////////////////////////////////////////////
  // METHODS                                                                  //
  //////////////////////////////////////////////////////////////////////////////

  /**
   * Queries the database for all flights.
   * <p>
   * Returns an <code>ArrayList</code> of flights, sorted by
   * <code>airline_id</code> ascending and <code>flight_id</code> ascending.
   * <p>
   * If there are no flights, returns an <code>ArrayList</code> that contains
   * no elements and assigns a user-friendly error message to
   * <code>error</code>.
   *
   * @return an <code>ArrayList</code> of one or more <code>Flight</code>
   *         objects if flights are found; an empty <code>ArrayList</code>
   *         otherwise
   */
  public static ArrayList<Flight> queryFlights()
  {
    try {
      // Ready the SQL statement.
      String sql = "SELECT airline_id, flight_id\n"
                 + "  FROM flight\n"
                 + " ORDER BY airline_id ASC, flight_id ASC";

      // Attempt a read from the database if the database connection is active.
      Statement statement = DBConnector.createStatement(true);
      ResultSet results   = null;
      if (statement != null) // Database connection is active.
        results = statement.executeQuery(sql);
      else { // Database connection is not active.
        error = DBConnector.error;

        return new ArrayList<Flight>(0);
      }

      ArrayList<Flight> flights = new ArrayList<>();

      // Check to see if flights were found.
      if (results.next()) { // Flights were found.
        results.beforeFirst(); // Reset cursor.

        while (results.next()) {
          Airline airline = DBMapper.getAirline(results.getString(1));

          flights.add(DBMapper.getFlight(results.getString(2),
                                         airline));
        }
      }
      else // No flights were found.
        error = "No flights were found in our records.";

      // No JDBC errors occurred. Clean up.
      statement.close();

      return flights;
    }
    catch (SQLException e) { // A JDBC error occurred.
      error = "He's dead, Jim. Please report this error.";
      e.printStackTrace(); // For debugging only.

      return new ArrayList<Flight>(0);
    }
  }

  /**
   * Queries the database for flights that depart from and arrive at the
   * specified airports and will depart on the specified date.
   * <p>
   * Returns an <code>ArrayList</code> of flights, sorted by <code>fare</code>
   * ascending and <code>capacity</code> descending.
   * <p>
   * If no matching flights are found, returns an <code>ArrayList</code> that
   * contains no elements and assigns a user-friendly error message to
   * <code>error</code>.
   *
   * @param originAirport      an <code>Airport</code> object representing the
   *                           specified origin airport
   * @param destinationAirport an <code>Airport</code> object representing the
   *                           specified destination airport
   * @param departureDate      a <code>java.sql.Date</code> object representing
   *                           the specified departure date
   * @return                   an <code>ArrayList</code> of one or more
   *                           <code>Flight</code> objects if flights are found;
   *                           an empty <code>ArrayList</code> otherwise
   */
  public static ArrayList<Flight> queryFlights(Airport originAirport,
                                               Airport destinationAirport,
                                               Date    departureDate)
  {
    try {
      // Ready the SQL statement.
      String sql = String.format("SELECT departure_days, airline_id,"
                               + "       flight_id\n"
                               + "  FROM flight\n"
                               + " WHERE origin_airport = '%s'\n"
                               + "   AND destination_airport = '%s'\n"
                               + " ORDER BY fare ASC, capacity DESC",
                                 originAirport.getID(),
                                 destinationAirport.getID());

      // Attempt a read from the database if the database connection is active.
      Statement statement = DBConnector.createStatement(true);
      ResultSet results   = null;
      if (statement != null) // Database connection is active.
        results = statement.executeQuery(sql);
      else { // Database connection is not active.
        error = DBConnector.error;

        return new ArrayList<Flight>(0);
      }

      ArrayList<Flight> flights = new ArrayList<>();

      // Check to see if matching flights were found.
      if (results.next()) { // Flights were found.
        results.beforeFirst(); // Reset cursor.

        while (results.next()) {
          // Retrieve the departure date's day of the week.
          String day = DBEncoder.encodeDayOfWeek(departureDate);

          // Check that flight departs on specified date.
          if (results.getString(1).contains(day)) {
            Airline airline = DBMapper.getAirline(results.getString(2));

            flights.add(DBMapper.getFlight(results.getString(3),
                                           airline));
          }
        }
      }
      else // No flights were found.
        error = "No flights from "
              + originAirport.getID()
              + " to "
              + destinationAirport.getID()
              + " departing on "
              + departureDate
              + " were found in our records.";

      // No JDBC errors occurred. Clean up.
      statement.close();

      return flights;
    }
    catch (SQLException e) { // A JDBC error occurred.
      error = "He's dead, Jim. Please report this error.";
      e.printStackTrace(); // For debugging only.

      return new ArrayList<Flight>(0);
    }
  }

  /**
   * Queries the database for direct flights that depart from and arrive at the
   * specified airports and will depart on the specified date.
   * <p>
   * Returns an <code>ArrayList</code> of flights, sorted by <code>fare</code>
   * ascending and <code>capacity</code> descending.
   * <p>
   * If no matching flights are found, returns an <code>ArrayList</code> that
   * contains no elements and assigns a user-friendly error message to
   * <code>error</code>.
   *
   * @param originAirport      an <code>Airport</code> object representing the
   *                           specified origin airport
   * @param destinationAirport an <code>Airport</code> object representing the
   *                           specified destination airport
   * @param departureDate      a <code>java.sql.Date</code> object representing
   *                           the specified departure date
   * @return                   an <code>ArrayList</code> of one or more
   *                           <code>Flight</code> objects if flights are found;
   *                           an empty <code>ArrayList</code> otherwise
   */
  public static ArrayList<Flight> queryDirectFlights(Airport originAirport,
                                                     Airport destinationAirport,
                                                     Date    departureDate)
  {
    try {
      // Ready the SQL statement.
      String sql = String.format("SELECT departure_days, airline_id,"
                               + "       flight_id\n"
                               + "  FROM flight\n"
                               + " WHERE origin_airport = '%s'\n"
                               + "   AND destination_airport = '%s'\n"
                               + "   AND airline_id <> 'ZZ'\n"
                               + " ORDER BY fare ASC, capacity DESC",
                                 originAirport.getID(),
                                 destinationAirport.getID());

      // Attempt a read from the database if the database connection is active.
      Statement statement = DBConnector.createStatement(true);
      ResultSet results   = null;
      if (statement != null) // Database connection is active.
        results = statement.executeQuery(sql);
      else { // Database connection is not active.
        error = DBConnector.error;

        return new ArrayList<Flight>(0);
      }

      ArrayList<Flight> flights = new ArrayList<>();

      // Check to see if matching flights were found.
      if (results.next()) { // Flights were found.
        results.beforeFirst(); // Reset cursor.

        while (results.next()) {
          // Retrieve the departure date's day of the week.
          String day = DBEncoder.encodeDayOfWeek(departureDate);

          // Check that flight departs on specified date.
          if (results.getString(1).contains(day)) {
            Airline airline = DBMapper.getAirline(results.getString(2));

            flights.add(DBMapper.getFlight(results.getString(3),
                                           airline));
          }
        }
      }
      else // No flights were found.
        error = "No flights from "
              + originAirport.getID()
              + " to "
              + destinationAirport.getID()
              + " departing on "
              + departureDate
              + " were found in our records.";

      // No JDBC errors occurred. Clean up.
      statement.close();

      return flights;
    }
    catch (SQLException e) { // A JDBC error occurred.
      error = "He's dead, Jim. Please report this error.";
      e.printStackTrace(); // For debugging only.

      return new ArrayList<Flight>(0);
    }
  }

  /**
   * Queries the database for flights that depart from or arrive at the
   * specified airport.
   * <p>
   * Returns an <code>ArrayList</code> of flights, sorted by
   * <code>origin_airport</code> ascending, <code>destination_airport</code>
   * ascending, and <code>fare</code> ascending.
   * <p>
   * If no matching flights are found, returns an <code>ArrayList</code> that
   * contains no elements and assigns a user-friendly error message to
   * <code>error</code>.
   *
   * @param airport an <code>Airport</code> object representing the specified
   *                airport
   * @return        an <code>ArrayList</code> of one or more
   *                <code>Flight</code> objects if flights are found; an empty
   *                <code>ArrayList</code> otherwise
   */
  public static ArrayList<Flight> queryFlights(Airport airport)
  {
    try {
      // Ready the SQL statement.
      String sql = String.format("SELECT airline_id, flight_id\n"
                               + "  FROM flight\n"
                               + " WHERE origin_airport = '%s'\n"
                               + "    OR destination_airport = '%s'\n"
                               + " ORDER BY origin_airport ASC,"
                               + "          destination_airport ASC, fare ASC",
                                 airport.getID(),
                                 airport.getID());

      // Attempt a read from the database if the database connection is active.
      Statement statement = DBConnector.createStatement(true);
      ResultSet results   = null;
      if (statement != null) // Database connection is active.
        results = statement.executeQuery(sql);
      else { // Database connection is not active.
        error = DBConnector.error;

        return new ArrayList<Flight>(0);
      }

      ArrayList<Flight> flights = new ArrayList<>();

      // Check to see if matching flights were found.
      if (results.next()) { // Flights were found.
        results.beforeFirst(); // Reset cursor.

        while (results.next()) {
          Airline airline = DBMapper.getAirline(results.getString(1));

          flights.add(DBMapper.getFlight(results.getString(2),
                                         airline));
        }
      }
      else // No flights were found.
        error = "No flights from or to "
              + airport.getID()
              + " were found in our records.";

      // No JDBC errors occurred. Clean up.
      statement.close();

      return flights;
    }
    catch (SQLException e) { // A JDBC error occurred.
      error = "He's dead, Jim. Please report this error.";
      e.printStackTrace(); // For debugging only.

      return new ArrayList<Flight>(0);
    }
  }

  /**
   * Queries the database for flights that are scheduled as part of the
   * specified reservation.
   * <p>
   * Returns an <code>ArrayList</code> of flights, sorted by
   * <code>flight_order</code> ascending.
   * <p>
   * If no matching flights are found, returns an <code>ArrayList</code> that
   * contains no elements and assigns a user-friendly error message to
   * <code>error</code>.
   *
   * @param reservation a <code>Reservation</code> object representing the
   *                    specified reservation
   * @return            an <code>ArrayList</code> of one or more
   *                    <code>Flight</code> objects if flights are found; an
   *                    empty <code>ArrayList</code> otherwise
   */
  public static ArrayList<Flight> queryFlights(Reservation reservation)
  {
    try {
      // Ready the SQL statement.
      String sql = String.format("SELECT DISTINCT x.airline_id, x.flight_id\n"
                               + "  FROM (SELECT airline_id, flight_id\n"
                               + "          FROM booked\n"
                               + "         WHERE reservation_id = %d\n"
                               + "         ORDER BY flight_order ASC) AS x",
                                 reservation.getID());

      // Attempt a read from the database if the database connection is active.
      Statement statement = DBConnector.createStatement(true);
      ResultSet results   = null;
      if (statement != null) // Database connection is active.
        results = statement.executeQuery(sql);
      else { // Database connection is not active.
        error = DBConnector.error;

        return new ArrayList<Flight>(0);
      }

      ArrayList<Flight> flights = new ArrayList<>();

      // Check to see if matching flights were found.
      if (results.next()) { // Flights were found.
        results.beforeFirst(); // Reset cursor.

        while (results.next()) {
          Airline airline = DBMapper.getAirline(results.getString(1));

          flights.add(DBMapper.getFlight(results.getString(2),
                                         airline));
        }
      }
      else // No flights were found.
        error = "No flights for reservation #"
              + reservation.getID()
              + " were found in our records.";

      // No JDBC errors occurred. Clean up.
      statement.close();

      return flights;
    }
    catch (SQLException e) { // A JDBC error occurred.
      error = "He's dead, Jim. Please report this error.";
      e.printStackTrace(); // For debugging only.

      return new ArrayList<Flight>(0);
    }
  }

  /**
   * Queries the database for flights that serve as legs within the specified
   * multi-leg flight.
   * <p>
   * Returns an <code>ArrayList</code> of flights, sorted by
   * <code>leg_order</code> ascending.
   * <p>
   * If no matching legs are found, returns an <code>ArrayList</code> that
   * contains no elements and assigns a user-friendly error message to
   * <code>error</code>.
   * <p>
   * If the specified flight is direct (i.e. has no legs), returns an
   * <code>ArrayList</code> containing only the direct flight.
   *
   * @param flight a <code>Flight</code> object representing the specified
   *               flight
   * @return       an <code>ArrayList</code> of one or more <code>Flight</code>
   *               objects if legs are found; an empty <code>ArrayList</code>
   *               if no legs are found; an <code>ArrayList</code> with only
   *               the passed <code>Flight</code> object if a direct flight
   */
  public static ArrayList<Flight> queryLegs(Flight flight)
  {
    // Check to see if specified flight is direct.
    if (!flight.getAirline().getID().equals("ZZ")) { // Flight is direct.
      ArrayList<Flight> legs = new ArrayList<>(1);
      legs.add(flight);

      return legs;
    }

    try {
      // Ready the SQL statement.
      String sql = String.format("SELECT leg_airline, leg_id\n"
                               + "  FROM route\n"
                               + " WHERE flight_id = '%s'\n"
                               + "   AND flight_airline = '%s'\n"
                               + " ORDER BY leg_order ASC",
                                 flight.getID(),
                                 flight.getAirline().getID());

      // Attempt a read from the database if the database connection is active.
      Statement statement = DBConnector.createStatement(true);
      ResultSet results   = null;
      if (statement != null) // Database connection is active.
        results = statement.executeQuery(sql);
      else { // Database connection is not active.
        error = DBConnector.error;

        return new ArrayList<Flight>(0);
      }

      ArrayList<Flight> legs = new ArrayList<>();

      // Check to see if matching legs were found.
      if (results.next()) { // Legs were found.
        results.beforeFirst(); // Reset cursor.

        while (results.next()) {
          Airline airline = DBMapper.getAirline(results.getString(1));

          legs.add(DBMapper.getFlight(results.getString(2),
                                      airline));
        }
      }
      else // No legs were found.
        error = "No legs for flight "
              + flight.getAirline().getID()
              + " "
              + flight.getID()
              + " were found in our records.";

      // No JDBC errors occurred. Clean up.
      statement.close();

      return legs;
    }
    catch (SQLException e) { // A JDBC error occurred.
      error = "He's dead, Jim. Please report this error.";
      e.printStackTrace(); // For debugging only.

      return new ArrayList<Flight>(0);
    }
  }

  /**
   * Queries the database for bookings that make up the travel itinerary for the
   * specified reservation.
   * <p>
   * Returns an <code>ArrayList</code> of bookings, sorted by
   * <code>account_id</code> ascending and <code>flight_order</code> ascending.
   * <p>
   * If no matching bookings are found, returns an <code>ArrayList</code> that
   * contains no elements and assigns a user-friendly error message to
   * <code>error</code>.
   *
   * @param reservation a <code>Reservation</code> object representing the
   *                    specified reservation
   * @return            an <code>ArrayList</code> of one or more
   *                    <code>Booked</code> objects if bookings are found; an
   *                    empty <code>ArrayList</code> otherwise
   */
  public static ArrayList<Booked> queryItinerary(Reservation reservation)
  {
    try {
      // Ready the SQL statement.
      String sql = String.format("SELECT account_id, airline_id, flight_id,"
                               + "       flight_order\n"
                               + "  FROM booked\n"
                               + " WHERE reservation_id = %d\n"
                               + " ORDER BY account_id ASC, flight_order ASC",
                                 reservation.getID());

      // Attempt a read from the database if the database connection is active.
      Statement statement = DBConnector.createStatement(true);
      ResultSet results   = null;
      if (statement != null) // Database connection is active.
        results = statement.executeQuery(sql);
      else { // Database connection is not active.
        error = DBConnector.error;

        return new ArrayList<Booked>(0);
      }

      ArrayList<Booked> itinerary = new ArrayList<>();

      // Check to see if matching bookings were found.
      if (results.next()) { // Bookings were found.
        results.beforeFirst(); // Reset cursor.

        while (results.next()) {
          Account account = DBMapper.getAccount(results.getInt(1));
          Airline airline = DBMapper.getAirline(results.getString(2));
          Flight  flight  = DBMapper.getFlight(results.getString(3),
                                               airline);

          itinerary.add(DBMapper.getBooked(reservation,
                                           account,
                                           flight,
                                           results.getInt(4)));
        }
      }
      else // No bookings were found.
        error = "No bookings for reservation #"
              + reservation.getID()
              + " were found in our records.";

      // No JDBC errors occurred. Clean up.
      statement.close();

      return itinerary;
    }
    catch (SQLException e) { // A JDBC error occurred.
      error = "He's dead, Jim. Please report this error.";
      e.printStackTrace(); // For debugging only.

      return new ArrayList<Booked>(0);
    }
  }

  /**
   * Queries the database for the ten most booked flights.
   * <p>
   * Returns an <code>ArrayList</code> of flights, sorted by number of bookings
   * descending.
   * <p>
   * If no matching flights are found, returns an <code>ArrayList</code> that
   * contains no elements and assigns a user-friendly error message to
   * <code>error</code>.
   *
   * @return an <code>ArrayList</code> of one or more <code>Flight</code>
   *         objects if flights are found; an empty <code>ArrayList</code>
   *         otherwise
   */
  public static ArrayList<Flight> queryMostActiveFlights()
  {
    try {
      // Ready the SQL statement.
      String sql = "SELECT x.airline_id, x.flight_id\n"
                 + "  FROM (SELECT airline_id, flight_id,"
                 + "               COUNT(*) AS num_bookings\n"
                 + "          FROM booked\n"
                 + "         GROUP BY airline_id, flight_id\n"
                 + "         ORDER BY num_bookings DESC\n"
                 + "         FETCH FIRST 10 ROWS ONLY) AS x";

      // Attempt a read from the database if the database connection is active.
      Statement statement = DBConnector.createStatement(true);
      ResultSet results   = null;
      if (statement != null) // Database connection is active.
        results = statement.executeQuery(sql);
      else { // Database connection is not active.
        error = DBConnector.error;

        return new ArrayList<Flight>(0);
      }

      ArrayList<Flight> flights = new ArrayList<>();

      // Check to see if matching flights were found.
      if (results.next()) { // Flights were found.
        results.beforeFirst(); // Reset cursor.

        while (results.next()) {
          Airline airline = DBMapper.getAirline(results.getString(1));

          flights.add(DBMapper.getFlight(results.getString(2),
                                         airline));
        }
      }
      else // No flights were found.
        error = "No flight bookings were found in our records.";

      // No JDBC errors occurred. Clean up.
      statement.close();

      return flights;
    }
    catch (SQLException e) { // A JDBC error occurred.
      error = "He's dead, Jim. Please report this error.";
      e.printStackTrace(); // For debugging only.

      return new ArrayList<Flight>(0);
    }
  }

  /**
   * Queries the database for the specified customer's five most booked flights.
   * <p>
   * Returns an <code>ArrayList</code> of flights, sorted by number of bookings
   * descending.
   * <p>
   * If no matching flights are found, returns an <code>ArrayList</code> that
   * contains no elements and assigns a user-friendly error message to
   * <code>error</code>.
   *
   * @param customer a <code>Customer</code> object representing the specified
   *                 customer
   * @return         an <code>ArrayList</code> of one or more
   *                 <code>Flight</code> objects if flights are found; an empty
   *                 <code>ArrayList</code> otherwise
   */
  public static ArrayList<Flight> querySuggestedFlights(Customer customer)
  {
    try {
      // Ready the SQL statement.
      String sql = String.format("SELECT x.airline_id, x.flight_id\n"
                               + "  FROM (SELECT b.airline_id, b.flight_id,"
                               + "               COUNT(*) AS num_bookings\n"
                               + "          FROM booked AS b\n"
                               + "          JOIN account AS a\n"
                               + "            ON b.account_id = a.account_id\n"
                               + "         WHERE a.customer_id = %d\n"
                               + "         GROUP BY b.airline_id, b.flight_id\n"
                               + "         ORDER BY num_bookings DESC\n"
                               + "         FETCH FIRST 5 ROWS ONLY) AS x",
                                 customer.getID());

      // Attempt a read from the database if the database connection is active.
      Statement statement = DBConnector.createStatement(true);
      ResultSet results   = null;
      if (statement != null) // Database connection is active.
        results = statement.executeQuery(sql);
      else { // Database connection is not active.
        error = DBConnector.error;

        return new ArrayList<Flight>(0);
      }

      ArrayList<Flight> flights = new ArrayList<>();

      // Check to see if matching flights were found.
      if (results.next()) { // Flights were found.
        results.beforeFirst(); // Reset cursor.

        while (results.next()) {
          Airline airline = DBMapper.getAirline(results.getString(1));

          flights.add(DBMapper.getFlight(results.getString(2),
                                         airline));
        }
      }
      else // No flights were found.
        error = "No flights booked by customer #"
              + customer.getID()
              + " were found in our records.";

      // No JDBC errors occurred. Clean up.
      statement.close();

      return flights;
    }
    catch (SQLException e) { // A JDBC error occurred.
      error = "He's dead, Jim. Please report this error.";
      e.printStackTrace(); // For debugging only.

      return new ArrayList<Flight>(0);
    }
  }

  /**
   * Queries the database for customers who have seats reserved on the specified
   * flight.
   * <p>
   * Returns an <code>ArrayList</code> of customers, sorted by
   * <code>departure_date</code> ascending, <code>last_name</code> ascending,
   * and <code>first_name</code> ascending.
   * <p>
   * If no matching customers are found, returns an <code>ArrayList</code> that
   * contains no elements and assigns a user-friendly error message to
   * <code>error</code>.
   *
   * @param flight a <code>Flight</code> object representing the specified
   *               flight
   * @return       an <code>ArrayList</code> of one or more
   *               <code>Customer</code> objects if customers are found; an
   *               empty <code>ArrayList</code> otherwise
   */
  public static ArrayList<Customer> queryCustomers(Flight flight)
  {
    try {
      // Ready the SQL statement.
      String sql = String.format("SELECT DISTINCT a.customer_id,"
                               + "                b.departure_date,"
                               + "                c.last_name, c.first_name\n"
                               + "  FROM account AS a\n"
                               + "  JOIN booked AS b\n"
                               + "    ON a.account_id = b.account_id\n"
                               + "  JOIN customer AS c\n"
                               + "    ON a.customer_id = c.customer_id\n"
                               + "  JOIN flight AS f\n"
                               + "    ON b.airline_id = f.airline_id\n"
                               + "   AND b.flight_id = f.flight_id\n"
                               + " WHERE b.airline_id = '%s'\n"
                               + "   AND b.flight_id = '%s'\n"
                               + "   AND b.departure_date > CURRENT_DATE\n"
                               + "    OR (b.departure_date = CURRENT_DATE\n"
                               + "       AND f.departure_time > CURRENT_TIME)\n"
                               + " ORDER BY b.departure_date ASC,"
                               + "          c.last_name ASC, c.first_name ASC",
                                 flight.getAirline().getID(),
                                 flight.getID());

      // Attempt a read from the database if the database connection is active.
      Statement statement = DBConnector.createStatement(true);
      ResultSet results   = null;
      if (statement != null) // Database connection is active.
        results = statement.executeQuery(sql);
      else { // Database connection is not active.
        error = DBConnector.error;

        return new ArrayList<Customer>(0);
      }

      ArrayList<Customer> customers = new ArrayList<>();

      // Check to see if matching customers were found.
      if (results.next()) { // Customers were found.
        results.beforeFirst(); // Reset cursor.

        while (results.next())
          customers.add(DBMapper.getCustomer(results.getInt(1)));
      }
      else // No customers were found.
        error = "No customers with seats reserved on flight "
              + flight.getAirline().getID()
              + " "
              + flight.getID()
              + " were found in our records.";

      // No JDBC errors occurred. Clean up.
      statement.close();

      return customers;
    }
    catch (SQLException e) { // A JDBC error occurred.
      error = "He's dead, Jim. Please report this error.";
      e.printStackTrace(); // For debugging only.

      return new ArrayList<Customer>(0);
    }
  }

  /**
   * Queries the database for reservations that include bookings for the
   * specified flight.
   * <p>
   * Returns an <code>ArrayList</code> of reservations, sorted by
   * <code>reservation_id</code> descending.
   * <p>
   * If no matching reservations are found, returns an <code>ArrayList</code>
   * that contains no elements and assigns a user-friendly error message to
   * <code>error</code>.
   *
   * @param flight a <code>Flight</code> object containing the
   *               <code>flight</code> and <code>airline</code> identifiers
   * @return       an <code>ArrayList</code> of one or more
   *               <code>Reservation</code> objects if reservations are found;
   *               an empty <code>ArrayList</code> otherwise
   */
  public static ArrayList<Reservation> queryReservations(Flight flight)
  {
    try {
      // Ready the SQL statement.
      String sql = String.format("SELECT DISTINCT reservation_id\n"
                               + "  FROM booked\n"
                               + " WHERE airline_id = '%s'\n"
                               + "   AND flight_id = '%s'\n"
                               + " ORDER BY reservation_id DESC",
                                 flight.getAirline().getID(),
                                 flight.getID());

      // Attempt a read from the database if the database connection is active.
      Statement statement = DBConnector.createStatement(true);
      ResultSet results   = null;
      if (statement != null) // Database connection is active.
        results = statement.executeQuery(sql);
      else { // Database connection is not active.
        error = DBConnector.error;

        return new ArrayList<Reservation>(0);
      }

      ArrayList<Reservation> reservations = new ArrayList<>();

      // Check to see if matching reservations were found.
      if (results.next()) { // Reservations were found.
        results.beforeFirst(); // Reset cursor.

        while (results.next())
          reservations.add(DBMapper.getReservation(results.getInt(1)));
      }
      else // No reservations were found.
        error = "No reservations that include bookings for flight "
              + flight.getAirline().getID()
              + " "
              + flight.getID()
              + " were found in our records.";

      // No JDBC errors occurred. Clean up.
      statement.close();

      return reservations;
    }
    catch (SQLException e) { // A JDBC error occurred.
      error = "He's dead, Jim. Please report this error.";
      e.printStackTrace(); // For debugging only.

      return new ArrayList<Reservation>(0);
    }
  }

  /**
   * Queries the database for the specified customer's current and past
   * reservations.
   * <p>
   * Returns an <code>ArrayList</code> of reservations, sorted by
   * <code>reservation_id</code> descending.
   * <p>
   * If no matching reservations are found, returns an <code>ArrayList</code>
   * that contains no elements and assigns a user-friendly error message to
   * <code>error</code>.
   *
   * @param customer a <code>Customer</code> object representing the specified
   *                 customer
   * @return         an <code>ArrayList</code> of one or more
   *                 <code>Reservation</code> objects if reservations are found;
   *                 an empty <code>ArrayList</code> otherwise
   */
  public static ArrayList<Reservation> queryReservations(Customer customer)
  {
    try {
      // Ready the SQL statement.
      String sql = String.format("SELECT DISTINCT b.reservation_id\n"
                               + "  FROM booked AS b\n"
                               + "  JOIN account AS a\n"
                               + "    ON b.account_id = a.account_id\n"
                               + " WHERE a.customer_id = %d\n"
                               + " ORDER BY b.reservation_id DESC",
                                 customer.getID());

      // Attempt a read from the database if the database connection is active.
      Statement statement = DBConnector.createStatement(true);
      ResultSet results   = null;
      if (statement != null) // Database connection is active.
        results = statement.executeQuery(sql);
      else { // Database connection is not active.
        error = DBConnector.error;

        return new ArrayList<Reservation>(0);
      }

      ArrayList<Reservation> reservations = new ArrayList<>();

      // Check to see if matching reservations were found.
      if (results.next()) { // Reservations were found.
        results.beforeFirst(); // Reset cursor.

        while (results.next())
          reservations.add(DBMapper.getReservation(results.getInt(1)));
      }
      else // No reservations were found.
        error = "No reservations for customer #"
              + customer.getID()
              + " were found in our records.";

      // No JDBC errors occurred. Clean up.
      statement.close();

      return reservations;
    }
    catch (SQLException e) { // A JDBC error occurred.
      error = "He's dead, Jim. Please report this error.";
      e.printStackTrace(); // For debugging only.

      return new ArrayList<Reservation>(0);
    }
  }

  /**
   * Queries the database for the specified customer's current reservations.
   * <p>
   * Returns an <code>ArrayList</code> of reservations, sorted by
   * <code>reservation_id</code> ascending.
   * <p>
   * If no matching reservations are found, returns an <code>ArrayList</code>
   * that contains no elements and assigns a user-friendly error message to
   * <code>error</code>.
   *
   * @param customer a <code>Customer</code> object representing the specified
   *                 customer
   * @return         an <code>ArrayList</code> of one or more
   *                 <code>Reservation</code> objects if reservations are found;
   *                 an empty <code>ArrayList</code> otherwise
   */
  public static ArrayList<Reservation>
                                      queryActiveReservations(Customer customer)
  {
    try {
      // Ready the SQL statement.
      String sql = String.format("SELECT DISTINCT b.reservation_id\n"
                               + "  FROM booked AS b\n"
                               + "  JOIN account AS a\n"
                               + "    ON b.account_id = a.account_id\n"
                               + "  JOIN flight AS f\n"
                               + "    ON b.airline_id = f.airline_id\n"
                               + "   AND b.flight_id = f.flight_id\n"
                               + " WHERE a.customer_id = %d\n"
                               + "   AND b.departure_date > CURRENT_DATE\n"
                               + "    OR (b.departure_date = CURRENT_DATE\n"
                               + "       AND f.departure_time > CURRENT_TIME)\n"
                               + " ORDER BY b.reservation_id ASC",
                                 customer.getID());

      // Attempt a read from the database if the database connection is active.
      Statement statement = DBConnector.createStatement(true);
      ResultSet results   = null;
      if (statement != null) // Database connection is active.
        results = statement.executeQuery(sql);
      else { // Database connection is not active.
        error = DBConnector.error;

        return new ArrayList<Reservation>(0);
      }

      ArrayList<Reservation> reservations = new ArrayList<>();

      // Check to see if matching reservations were found.
      if (results.next()) { // Reservations were found.
        results.beforeFirst(); // Reset cursor.

        while (results.next())
          reservations.add(DBMapper.getReservation(results.getInt(1)));
      }
      else // No reservations were found.
        error = "No active reservations for customer #"
              + customer.getID()
              + " were found in our records.";

      // No JDBC errors occurred. Clean up.
      statement.close();

      return reservations;
    }
    catch (SQLException e) { // A JDBC error occurred.
      error = "He's dead, Jim. Please report this error.";
      e.printStackTrace(); // For debugging only.

      return new ArrayList<Reservation>(0);
    }
  }

  /**
   * Queries the database for the current and past bids made on the specified
   * reverse auction.
   * <p>
   * Returns an <code>ArrayList</code> of bids, sorted by
   * <code>bid_amount</code> descending.
   * <p>
   * If the reverse auction does not exist, returns an <code>ArrayList</code>
   * that contains no elements and assigns a user-friendly error message to
   * <code>error</code>.
   *
   * @param reservation a <code>Reservation</code> object representing the
   *                    specified reservation
   * @param account     an <code>Account</code> object representing the
   *                    specified account
   * @return            an <code>ArrayList</code> of one or more
   *                    <code>Bid</code> objects if the reverse auction exists;
   *                    an empty <code>ArrayList</code> otherwise
   */
  public static ArrayList<Bid> queryBids(Reservation reservation,
                                         Account account)
  {
    try {
      // Ready the SQL statement.
      String sql = String.format("SELECT bid_amount\n"
                               + "  FROM bid\n"
                               + " WHERE reservation_id = %d\n"
                               + "   AND account_id = %d\n"
                               + " ORDER BY bid_amount DESC",
                                 reservation.getID(),
                                 account.getID());

      // Attempt a read from the database if the database connection is active.
      Statement statement = DBConnector.createStatement(true);
      ResultSet results   = null;
      if (statement != null) // Database connection is active.
        results = statement.executeQuery(sql);
      else { // Database connection is not active.
        error = DBConnector.error;

        return new ArrayList<Bid>(0);
      }

      ArrayList<Bid> bids = new ArrayList<>();

      // Check to see if the reverse auction was found.
      if (results.next()) { // Reverse auction was found.
        results.beforeFirst(); // Reset cursor.

        while (results.next()) {
          double bidAmount = results.getBigDecimal(1).doubleValue();

          bids.add(DBMapper.getBid(reservation,
                                   account,
                                   bidAmount));
        }
      }
      else // Reverse auction was not found.
        error = "No reverse auction for reservation #"
              + reservation.getID()
              + " and account #"
              + account.getID()
              + " was found in our records.";

      // No JDBC errors occurred. Clean up.
      statement.close();

      return bids;
    }
    catch (SQLException e) { // A JDBC error occurred.
      error = "He's dead, Jim. Please report this error.";
      e.printStackTrace(); // For debugging only.

      return new ArrayList<Bid>(0);
    }
  }

  /**
   * Queries the database for the current bid made on the specified reverse
   * auction.
   * <p>
   * Returns a <code>Bid</code> object that maps the matching bid.
   * <p>
   * If the reverse auction does not exist, returns a <code>null</code>
   * reference and assigns a user-friendly error message to <code>error</code>.
   *
   * @param reservation a <code>Reservation</code> object representing the
   *                    specified reservation
   * @param account     an <code>Account</code> object representing the
   *                    specified account
   * @return            a fully instantiated <code>Bid</code> object if the
   *                    reverse auction exists; <code>null</code> otherwise
   */
  public static Bid queryCurrentBid(Reservation reservation,
                                    Account account)
  {
    try {
      // Ready the SQL statement.
      String sql = String.format("SELECT bid_amount\n"
                               + "  FROM bid\n"
                               + " WHERE reservation_id = %d\n"
                               + "   AND account_id = %d\n"
                               + " ORDER BY bid_amount DESC\n"
                               + " FETCH FIRST ROW ONLY",
                                 reservation.getID(),
                                 account.getID());

      // Attempt a read from the database if the database connection is active.
      Statement statement = DBConnector.createStatement(false);
      ResultSet results   = null;
      if (statement != null) // Database connection is active.
        results = statement.executeQuery(sql);
      else { // Database connection is not active.
        error = DBConnector.error;

        return null;
      }

      Bid bid = null;

      // Check to see if the reverse auction was found.
      if (results.next()) { // Reverse auction was found.
        double bidAmount = results.getBigDecimal(1).doubleValue();

        bid = DBMapper.getBid(reservation,
                              account,
                              bidAmount);
      }
      else // Reverse auction was not found.
        error = "No reverse auction for reservation #"
              + reservation.getID()
              + " and account #"
              + account.getID()
              + " was found in our records.";

      // No JDBC errors occurred. Clean up.
      statement.close();

      return bid;
    }
    catch (SQLException e) { // A JDBC error occurred.
      error = "He's dead, Jim. Please report this error.";
      e.printStackTrace(); // For debugging only.

      return null;
    }
  }

  /**
   * Queries the database for the revenue generated within the specified month.
   * <p>
   * Returns the sum of fees.
   * <p>
   * If no matching revenue is found, returns <code>0.0</code> and assigns a
   * user-friendly error message to <code>error</code>.
   *
   * @param month the month within the year, starting at 1 for January
   * @param year  the 4-digit year
   * @return      the sum of fees if revenue is found; <code>0.0</code>
   *              otherwise
   */
  public static double queryRevenue(int month,
                                    int year)
  {
    try {
      // Find the last day of the specified month within the specified year.
      Calendar calendar = Calendar.getInstance();
      calendar.set(Calendar.YEAR, year);
      calendar.set(Calendar.MONTH, month - 1);
      calendar.set(Calendar.DAY_OF_MONTH, 1);
      int lastDay = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);

      // Ready the SQL statement.
      String sql = String.format("SELECT SUM(fee) AS revenue\n"
                               + "  FROM reservation\n"
                               + " WHERE booking_date BETWEEN '%s' AND '%s'",
                                 year + "-" + month + "-01",
                                 year + "-" + month + "-" + lastDay);

      // Attempt a read from the database if the database connection is active.
      Statement statement = DBConnector.createStatement(false);
      ResultSet results   = null;
      if (statement != null) // Database connection is active.
        results = statement.executeQuery(sql);
      else { // Database connection is not active.
        error = DBConnector.error;

        return 0.0;
      }

      double revenue = 0.0;

      // Check to see if matching revenue was found.
      if (results.next()
          && results.getBigDecimal(1) != null) { // Revenue was found.
        revenue = results.getBigDecimal(1).doubleValue();
      }
      else // No revenue was found.
        error = "No revenue generated in "
              + month
              + "/"
              + year
              + " was found in our records.";

      // No JDBC errors occurred. Clean up.
      statement.close();

      return revenue;
    }
    catch (SQLException e) { // A JDBC error occurred.
      error = "He's dead, Jim. Please report this error.";
      e.printStackTrace(); // For debugging only.

      return 0.0;
    }
  }

  /**
   * Queries the database for the revenue generated by the specified flight.
   * <p>
   * Returns the sum of fees.
   * <p>
   * If no matching revenue is found, returns <code>0.0</code> and assigns a
   * user-friendly error message to <code>error</code>.
   *
   * @param flight a <code>Flight</code> object representing the specified
   *               flight
   * @return       the sum of fees if revenue is found; <code>0.0</code>
   *               otherwise
   */
  public static double queryRevenue(Flight flight)
  {
    try {
      // Ready the SQL statement. Note: Duplicates are intentional.
      String sql = String.format("SELECT SUM(r.fee) AS revenue\n"
                               + "  FROM reservation AS r\n"
                               + "  JOIN booked AS b\n"
                               + "    ON r.reservation_id = b.reservation_id\n"
                               + " WHERE airline_id = '%s'\n"
                               + "   AND flight_id = '%s'",
                                 flight.getAirline().getID(),
                                 flight.getID());

      // Attempt a read from the database if the database connection is active.
      Statement statement = DBConnector.createStatement(false);
      ResultSet results   = null;
      if (statement != null) // Database connection is active.
        results = statement.executeQuery(sql);
      else { // Database connection is not active.
        error = DBConnector.error;

        return 0.0;
      }

      double revenue = 0.0;

      // Check to see if matching revenue was found.
      if (results.next()
          && results.getBigDecimal(1) != null) { // Revenue was found.
        revenue = results.getBigDecimal(1).doubleValue();
      }
      else // No revenue was found.
        error = "No revenue generated by flight "
              + flight.getAirline().getID()
              + " "
              + flight.getID()
              + " was found in our records.";

      // No JDBC errors occurred. Clean up.
      statement.close();

      return revenue;
    }
    catch (SQLException e) { // A JDBC error occurred.
      error = "He's dead, Jim. Please report this error.";
      e.printStackTrace(); // For debugging only.

      return 0.0;
    }
  }

  /**
   * Queries the database for the revenue generated by the specified destination
   * city.
   * <p>
   * Returns the sum of fees.
   * <p>
   * If no matching revenue is found, returns <code>0.0</code> and assigns a
   * user-friendly error message to <code>error</code>.
   *
   * @param city    the city's name
   * @param region  the ISO 3166-2 code of the region within the country where
   *                the city is located
   * @param country the ISO 3166-1 alpha-2 code of the country where the city is
   *                located
   * @return        the sum of fees if revenue is found; <code>0.0</code>
   *                otherwise
   */
  public static double queryRevenue(String city,
                                    String region,
                                    String country)
  {
    try {
      // Ready the SQL statement. Note: Duplicates are intentional.
      String sql = String.format("SELECT SUM(r.fee) AS revenue\n"
                               + "  FROM reservation AS r\n"
                               + "  JOIN booked AS b\n"
                               + "    ON r.reservation_id = b.reservation_id\n"
                               + "  JOIN flight AS f\n"
                               + "    ON b.airline_id = f.airline_id\n"
                               + "   AND b.flight_id = f.flight_id\n"
                               + "  JOIN airport AS a\n"
                               + "    ON f.destination_airport = a.airport_id\n"
                               + " WHERE a.city = '%s'\n"
                               + "   AND a.region = '%s'\n"
                               + "   AND a.country = '%s'",
                                 city,
                                 region,
                                 country);

      // Attempt a read from the database if the database connection is active.
      Statement statement = DBConnector.createStatement(false);
      ResultSet results   = null;
      if (statement != null) // Database connection is active.
        results = statement.executeQuery(sql);
      else { // Database connection is not active.
        error = DBConnector.error;

        return 0.0;
      }

      double revenue = 0.0;

      // Check to see if matching revenue was found.
      if (results.next()
          && results.getBigDecimal(1) != null) { // Revenue was found.
        revenue = results.getBigDecimal(1).doubleValue();
      }
      else // No revenue was found.
        error = "No revenue generated by airports in "
              + city
              + ", "
              + region
              + ", "
              + country
              + ", was found in our records.";

      // No JDBC errors occurred. Clean up.
      statement.close();

      return revenue;
    }
    catch (SQLException e) { // A JDBC error occurred.
      error = "He's dead, Jim. Please report this error.";
      e.printStackTrace(); // For debugging only.

      return 0.0;
    }
  }

  /**
   * Queries the database for the revenue generated by the specified customer.
   * <p>
   * Returns the sum of fees.
   * <p>
   * If no matching revenue is found, returns <code>0.0</code> and assigns a
   * user-friendly error message to <code>error</code>.
   *
   * @param customer a <code>Customer</code> object representing the specified
   *                 customer
   * @return         the sum of fees if revenue is found; <code>0.0</code>
   *                 otherwise
   */
  public static double queryRevenue(Customer customer)
  {
    try {
      // Ready the SQL statement. Note: Duplicates are intentional.
      String sql = String.format("SELECT SUM(r.fee) AS revenue\n"
                               + "  FROM reservation AS r\n"
                               + "  JOIN booked AS b\n"
                               + "    ON r.reservation_id = b.reservation_id\n"
                               + "  JOIN account AS a\n"
                               + "    ON b.account_id = a.account_id\n"
                               + " WHERE a.customer_id = %d",
                                 customer.getID());

      // Attempt a read from the database if the database connection is active.
      Statement statement = DBConnector.createStatement(false);
      ResultSet results   = null;
      if (statement != null) // Database connection is active.
        results = statement.executeQuery(sql);
      else { // Database connection is not active.
        error = DBConnector.error;

        return 0.0;
      }

      double revenue = 0.0;

      // Check to see if matching revenue was found.
      if (results.next()
          && results.getBigDecimal(1) != null) { // Revenue was found.
        revenue = results.getBigDecimal(1).doubleValue();
      }
      else // No revenue was found.
        error = "No revenue generated by customer #"
              + customer.getID()
              + " was found in our records.";

      // No JDBC errors occurred. Clean up.
      statement.close();

      return revenue;
    }
    catch (SQLException e) { // A JDBC error occurred.
      error = "He's dead, Jim. Please report this error.";
      e.printStackTrace(); // For debugging only.

      return 0.0;
    }
  }

  /**
   * Queries the database for the five most revenue-generating customers.
   * <p>
   * Returns an <code>ArrayList</code> of customers, sorted by revenue
   * descending.
   * <p>
   * If no matching customers are found, returns an <code>ArrayList</code> that
   * contains no elements and assigns a user-friendly error message to
   * <code>error</code>.
   *
   * @return an <code>ArrayList</code> of one or more <code>Customer</code>
   *         objects if customers are found; an empty <code>ArrayList</code>
   *         otherwise
   */
  public static ArrayList<Customer> queryCustomersWithMostRevenue()
  {
    try {
      // Ready the SQL statement.
      String sql = "SELECT x.customer_id\n"
                 + "  FROM (SELECT a.customer_id, SUM(r.fee) AS revenue\n"
                 + "          FROM account AS a\n"
                 + "          JOIN booked AS b\n"
                 + "            ON a.account_id = b.account_id\n"
                 + "          JOIN reservation AS r\n"
                 + "            ON b.reservation_id = r.reservation_id\n"
                 + "         GROUP BY a.customer_id\n"
                 + "         ORDER BY revenue DESC\n"
                 + "         FETCH FIRST 5 ROWS ONLY) AS x";

      // Attempt a read from the database if the database connection is active.
      Statement statement = DBConnector.createStatement(true);
      ResultSet results   = null;
      if (statement != null) // Database connection is active.
        results = statement.executeQuery(sql);
      else { // Database connection is not active.
        error = DBConnector.error;

        return new ArrayList<Customer>(0);
      }

      ArrayList<Customer> customers = new ArrayList<>();

      // Check to see if matching customers were found.
      if (results.next()) { // Customers were found.
        results.beforeFirst(); // Reset cursor.

        while (results.next())
          customers.add(DBMapper.getCustomer(results.getInt(1)));
      }
      else // No customers were found.
        error = "No customer revenue was found in our records.";

      // No JDBC errors occurred. Clean up.
      statement.close();

      return customers;
    }
    catch (SQLException e) { // A JDBC error occurred.
      error = "He's dead, Jim. Please report this error.";
      e.printStackTrace(); // For debugging only.

      return new ArrayList<Customer>(0);
    }
  }

  /**
   * Queries the database for the five most revenue-generating employees.
   * <p>
   * Returns an <code>ArrayList</code> of employees, sorted by revenue
   * descending.
   * <p>
   * If no matching employees are found, returns an <code>ArrayList</code> that
   * contains no elements and assigns a user-friendly error message to
   * <code>error</code>.
   *
   * @return an <code>ArrayList</code> of one or more <code>Employee</code>
   *         objects if employees are found; an empty <code>ArrayList</code>
   *         otherwise
   */
  public static ArrayList<Employee> queryEmployeesWithMostRevenue()
  {
    try {
      // Ready the SQL statement.
      String sql = "SELECT x.employee_id\n"
                 + "  FROM (SELECT employee_id, SUM(fee) AS revenue\n"
                 + "          FROM reservation\n"
                 + "         GROUP BY employee_id\n"
                 + "         ORDER BY revenue DESC\n"
                 + "         FETCH FIRST 5 ROWS ONLY) AS x";

      // Attempt a read from the database if the database connection is active.
      Statement statement = DBConnector.createStatement(true);
      ResultSet results   = null;
      if (statement != null) // Database connection is active.
        results = statement.executeQuery(sql);
      else { // Database connection is not active.
        error = DBConnector.error;

        return new ArrayList<Employee>(0);
      }

      ArrayList<Employee> employees = new ArrayList<>();

      // Check to see if matching employees were found.
      if (results.next()) { // Employees were found.
        results.beforeFirst(); // Reset cursor.

        while (results.next())
          employees.add(DBMapper.getEmployee(results.getInt(1)));
      }
      else // Employees were not found.
        error = "No employee revenue was found in our records.";

      // No JDBC errors occurred. Clean up.
      statement.close();

      return employees;
    }
    catch (SQLException e) { // A JDBC error occurred.
      error = "He's dead, Jim. Please report this error.";
      e.printStackTrace(); // For debugging only.

      return new ArrayList<Employee>(0);
    }
  }

  /**
   * Queries the database for a mailing list of medium- and high-rated
   * customers.
   * <p>
   * Returns an <code>ArrayList</code> of customer addresses, sorted by
   * <code>customer_id</code> ascending.
   * <p>
   * If no matching customers are found, returns an empty <code>ArrayList</code>
   * and assigns a user-friendly error message to <code>error</code>.
   *
   * @return an <code>ArrayList</code> of one or more addresses if customers are
   *         found; an empty <code>ArrayList</code> otherwise
   */
  public static ArrayList<String> queryMailingList()
  {
    try {
      // Ready the SQL statement.
      String sql = "SELECT first_name, last_name, address, city, state,"
                 + "       zip_code\n"
                 + "  FROM customer\n"
                 + " WHERE rating = 1\n"
                 + "    OR rating = 2\n"
                 + " ORDER BY customer_id ASC";

      // Attempt a read from the database if the database connection is active.
      Statement statement = DBConnector.createStatement(true);
      ResultSet results   = null;
      if (statement != null) // Database connection is active.
        results = statement.executeQuery(sql);
      else { // Database connection is not active.
        error = DBConnector.error;

        return new ArrayList<String>(0);
      }

      ArrayList<String> mailingList = new ArrayList<>();

      // Check to see if matching customers were found.
      if (results.next()) { // Customers were found.
        results.beforeFirst(); // Reset cursor.

        while (results.next())
          mailingList.add(results.getString(1)   // first_name
                        + " "
                        + results.getString(2)   // last_name
                        + "\n"
                        + results.getString(3)   // address
                        + "\n"
                        + results.getString(4)   // city
                        + ", "
                        + results.getString(5)   // state
                        + " "
                        + results.getString(6)); // zip_code
      }
      else // No customers were found.
        error = "No high-rated customers were found in our records.";

      // No JDBC errors occurred. Clean up.
      statement.close();

      return mailingList;
    }
    catch (SQLException e) { // A JDBC error occurred.
      error = "He's dead, Jim. Please report this error.";
      e.printStackTrace(); // For debugging only.

      return new ArrayList<String>(0);
    }
  }
}
