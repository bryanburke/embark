////////////////////////////////////////////////////////////////////////////////
// Bryan Burke, Joshua Lambert, and Elizabeth Rothermel                       //
// Group 4                                                                    //
// INFX 499                                                                   //
// Fall 2015                                                                  //
// University of Louisiana at Lafayette                                       //
////////////////////////////////////////////////////////////////////////////////

import java.io.IOException;
import java.util.ArrayList;

/**
 * Performs rudimentary unit testing on <code>Customer</code> objects.
 *
 * @author Bryan Burke
 */
public class TestCustomer
{
  //////////////////////////////////////////////////////////////////////////////
  // METHODS                                                                  //
  //////////////////////////////////////////////////////////////////////////////

  /**
   * Creates and tests <code>Customer</code> objects.
   *
   * @param args command-line arguments; unused
   */
  public static void main(String[] args) throws IOException
  {
    ////////////////////////////////////////////////////////////////////////////
    // TEST CONSTRUCTORS                                                      //
    ////////////////////////////////////////////////////////////////////////////

    System.out.println("== BEGIN CONSTRUCTOR TESTS ==");

    ArrayList<Customer> customers = new ArrayList<>(3);

    // Customer(int, String, String, String, String, String, String, String,
    //          int)
    customers.add(new Customer(5,
                               "Patel",
                               "Kimberly",
                               "3548 Orchard Street",
                               "Osseo",
                               "MN",
                               "55369",
                               "9528552611",
                               DBEncoder.RATING_HIGH));
    System.out.println("  Customer #5 constructed as customers[0].");

    // Customer(String, String, String, String, String, String, String)
    customers.add(new Customer("Phillips",
                               "Valeria",
                               "4254 Gladwell Street",
                               "Dallas",
                               "TX",
                               "75207",
                               "9032214245"));
    System.out.println("  New customer constructed as customers[1].");

    // Customer(Customer)
    customers.add(new Customer(customers.get(0)));
    System.out.println("  Customer #5 copy constructed as customers[2].");

    System.out.println("== END CONSTRUCTOR TESTS ==");
    System.out.println();



    ////////////////////////////////////////////////////////////////////////////
    // TEST METHODS                                                           //
    ////////////////////////////////////////////////////////////////////////////

    System.out.println("== BEGIN METHOD TESTS ==");

    // getID()
    System.out.println("= getID() =");
    for (int i = 0; i < customers.size(); ++i)
      System.out.printf("  [%d] : %d\n",
                        i,
                        customers.get(i).getID());
    System.out.print("Verify the results and press ENTER to continue.");
    System.in.read();

    // getLastName()
    System.out.println("= getLastName() =");
    for (int i = 0; i < customers.size(); ++i)
      System.out.printf("  [%d] : %s\n",
                        i,
                        customers.get(i).getLastName());
    System.out.print("Verify the results and press ENTER to continue.");
    System.in.read();

    // getFirstName()
    System.out.println("= getFirstName() =");
    for (int i = 0; i < customers.size(); ++i)
      System.out.printf("  [%d] : %s\n",
                        i,
                        customers.get(i).getFirstName());
    System.out.print("Verify the results and press ENTER to continue.");
    System.in.read();

    // getAddress()
    System.out.println("= getAddress() =");
    for (int i = 0; i < customers.size(); ++i)
      System.out.printf("  [%d] : %s\n",
                        i,
                        customers.get(i).getAddress());
    System.out.print("Verify the results and press ENTER to continue.");
    System.in.read();

    // getCity()
    System.out.println("= getCity() =");
    for (int i = 0; i < customers.size(); ++i)
      System.out.printf("  [%d] : %s\n",
                        i,
                        customers.get(i).getCity());
    System.out.print("Verify the results and press ENTER to continue.");
    System.in.read();

    // getState()
    System.out.println("= getState() =");
    for (int i = 0; i < customers.size(); ++i)
      System.out.printf("  [%d] : %s\n",
                        i,
                        customers.get(i).getState());
    System.out.print("Verify the results and press ENTER to continue.");
    System.in.read();

    // getZipCode()
    System.out.println("= getZipCode() =");
    for (int i = 0; i < customers.size(); ++i)
      System.out.printf("  [%d] : %s\n",
                        i,
                        customers.get(i).getZipCode());
    System.out.print("Verify the results and press ENTER to continue.");
    System.in.read();

    // getTelephone()
    System.out.println("= getTelephone() =");
    for (int i = 0; i < customers.size(); ++i)
      System.out.printf("  [%d] : %s\n",
                        i,
                        customers.get(i).getTelephone());
    System.out.print("Verify the results and press ENTER to continue.");
    System.in.read();

    // getRating()
    System.out.println("= getRating() =");
    for (int i = 0; i < customers.size(); ++i)
      System.out.printf("  [%d] : %d\n",
                        i,
                        customers.get(i).getRating());
    System.out.print("Verify the results and press ENTER to continue.");
    System.in.read();

    // equals(Customer)
    System.out.println("= equals([0]) =");
    for (int i = 0; i < customers.size(); ++i)
      System.out.printf("  [%d] : %s\n",
                        i,
                        customers.get(i).equals(customers.get(0)));
    System.out.print("Verify the results and press ENTER to continue.");
    System.in.read();

    // toString()
    System.out.println("= toString() =");
    for (int i = 0; i < customers.size(); ++i)
      System.out.printf("  [%d] : %s\n",
                        i,
                        customers.get(i).toString());
    System.out.print("Verify the results and press ENTER to continue.");
    System.in.read();

    // setLastName(String)
    System.out.println("= [1].setLastName(\"Quiles\") =");
    customers.get(1).setLastName("Quiles");

    // setFirstName(String)
    System.out.println("= [1].setFirstName(\"Lillian\") =");
    customers.get(1).setFirstName("Lillian");

    // setAddress(String)
    System.out.println("= [1].setAddress(\"2389 Koontz Lane\") =");
    customers.get(1).setAddress("2389 Koontz Lane");

    // setCity(String)
    System.out.println("= [1].setCity(\"Los Angeles\") =");
    customers.get(1).setCity("Los Angeles");

    // setState(String)
    System.out.println("= [1].setState(\"CA\") =");
    customers.get(1).setState("CA");

    // setZipCode(String)
    System.out.println("= [1].setZipCode(\"90017\") =");
    customers.get(1).setZipCode("90017");

    // setTelephone(String)
    System.out.println("= [1].setTelephone(\"8188297157\") =");
    customers.get(1).setTelephone("8188297157");

    // setRating(int)
    System.out.println("= [1].setRating(DBEncoder.RATING_MEDIUM) =");
    customers.get(1).setRating(DBEncoder.RATING_MEDIUM);

    System.out.printf("  [1] : %s\n",
                      customers.get(1).toString());
    System.out.print("Verify the results and press ENTER to continue.");
    System.in.read();

    System.out.println("== END METHOD TESTS ==");
  }
}
