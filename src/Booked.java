////////////////////////////////////////////////////////////////////////////////
// Bryan Burke, Joshua Lambert, and Elizabeth Rothermel                       //
// Group 4                                                                    //
// INFX 499                                                                   //
// Fall 2015                                                                  //
// University of Louisiana at Lafayette                                       //
////////////////////////////////////////////////////////////////////////////////

import java.sql.Date;

/**
 * Provides an in-memory mapping of a tuple from the <code>booked</code>
 * database relation.
 *
 * @author Bryan Burke
 */
public class Booked
{
  //////////////////////////////////////////////////////////////////////////////
  // CLASS VARIABLES                                                          //
  //////////////////////////////////////////////////////////////////////////////

  /**
   * Format specifier for <code>booked.flight_order</code>.
   */
  public static final String FORMAT_FLIGHT_ORDER   = "%1d";
  /**
   * Format specifier for <code>booked.departure_date</code>.
   */
  public static final String FORMAT_DEPARTURE_DATE = "%10.10s";
  /**
   * Format specifier for <code>booked.seat_number</code>.
   */
  public static final String FORMAT_SEAT_NUMBER    = "%3d";
  /**
   * Format specifier for <code>booked.special_meal</code>.
   */
  public static final String FORMAT_SPECIAL_MEAL   = "%1d";



  //////////////////////////////////////////////////////////////////////////////
  // INSTANCE VARIABLES                                                       //
  //////////////////////////////////////////////////////////////////////////////

  private Reservation reservation_;   // Associated reservation.
  private Account     account_;       // Associated account.
  private Flight      flight_;        // Associated flight.
  private int         flightOrder_;   // Ordinal position of flight.
  private Date        departureDate_; // Scheduled departure date.
  private int         seatNumber_;    // Passenger seat number.
  private int         specialMeal_;   // Special meal ordered.



  //////////////////////////////////////////////////////////////////////////////
  // CONSTRUCTORS                                                             //
  //////////////////////////////////////////////////////////////////////////////

  /**
   * Creates an object of this class with all instance variables initialized.
   * <p>
   * This constructor should be called by frontend code when instantiating an
   * object to pass to
   * <code>DBInserter.insertReservation(ArrayList<Booked>)</code>.
   *
   * @param reservation   the associated reservation
   * @param account       the associated account
   * @param flight        the associated flight
   * @param flightOrder   the ordinal position of the flight
   * @param departureDate the scheduled departure date
   * @param seatNumber    the passenger seat number
   * @param specialMeal   the special meal ordered
   */
  public Booked(Reservation reservation,
                Account     account,
                Flight      flight,
                int         flightOrder,
                Date        departureDate,
                int         seatNumber,
                int         specialMeal)
  {
    reservation_   = reservation; // Immutable.
    account_       = new Account(account);
    flight_        = flight;      // Immutable.
    flightOrder_   = flightOrder;
    departureDate_ = new Date(departureDate.getTime());
    seatNumber_    = seatNumber;
    specialMeal_   = specialMeal;
  }

  /**
   * Creates an object of this class, initializing only those instance variables
   * necessary to add a new tuple describing a booking entry with no meal to the
   * <code>booked</code> relation.
   * <p>
   * This constructor should be called by frontend code when instantiating an
   * object to pass to
   * <code>DBInserter.insertReservation(ArrayList<Booked>)</code>.
   *
   * @param reservation   the associated reservation
   * @param account       the associated account
   * @param flight        the associated flight
   * @param flightOrder   the ordinal position of the flight
   * @param departureDate the scheduled departure date
   * @param seatNumber    the passenger seat number
   */
  public Booked(Reservation reservation,
                Account     account,
                Flight      flight,
                int         flightOrder,
                Date        departureDate,
                int         seatNumber)
  {
    reservation_   = reservation;         // Immutable.
    account_       = new Account(account);
    flight_        = flight;              // Immutable.
    flightOrder_   = flightOrder;
    departureDate_ = new Date(departureDate.getTime());
    seatNumber_    = seatNumber;
    specialMeal_   = DBEncoder.MEAL_NONE; // Default database value.
  }

  /**
   * Creates a deep copy of an object of this class.
   *
   * @param other the object whose state should be copied
   */
  public Booked(Booked other)
  {
    this.reservation_   = other.reservation_; // Immutable.
    this.account_       = new Account(other.account_);
    this.flight_        = other.flight_;      // Immutable.
    this.flightOrder_   = other.flightOrder_;
    this.departureDate_ = new Date(other.departureDate_.getTime());
    this.seatNumber_    = other.seatNumber_;
    this.specialMeal_   = other.specialMeal_;
  }



  //////////////////////////////////////////////////////////////////////////////
  // METHODS                                                                  //
  //////////////////////////////////////////////////////////////////////////////

  /**
   * Returns a <code>Reservation</code> object that maps the associated
   * reservation's relational data.
   *
   * @return the associated reservation
   */
  public Reservation getReservation()
  {
    return reservation_; // Immutable.
  }

  /**
   * Returns an <code>Account</code> object that maps the associated account's
   * relational data.
   *
   * @return the associated account
   */
  public Account getAccount()
  {
    return new Account(account_);
  }

  /**
   * Returns a <code>Flight</code> object that maps the associated flight's
   * relational data.
   *
   * @return the associated flight
   */
  public Flight getFlight()
  {
    return flight_; // Immutable.
  }

  /**
   * Returns the order of the flight in the reservation itinerary.
   *
   * @return the ordinal position of the flight
   */
  public int getFlightOrder()
  {
    return flightOrder_;
  }

  /**
   * Returns a <code>java.sql.Date</code> object that maps the departure date's
   * ISO 8601 (yyyy-mm-dd) representation.
   *
   * @return the scheduled departure date
   */
  public Date getDepartureDate()
  {
    return new Date(departureDate_.getTime());
  }

  /**
   * Returns the seat number assigned to the account for the flight.
   *
   * @return the passenger seat number
   */
  public int getSeatNumber()
  {
    return seatNumber_;
  }

  /**
   * Returns the account's ordered flight meal encoded as follows:
   * <ul>
   * <li>0 = none
   * <li>1 = kosher
   * <li>2 = halal
   * <li>3 = vegetarian
   * <li>4 = vegan
   * <li>5 = diabetic
   * <li>6 = gluten-free
   * </ul>
   *
   * @return the special meal ordered
   */
  public int getSpecialMeal()
  {
    return specialMeal_;
  }



  /**
   * Peforms an equality comparison on two objects of this class.
   *
   * @param other the object whose state should be compared with the state of
   *              this object
   * @return      <code>true</code> if the object states are equal;
   *              <code>false</code> otherwise
   */
  public boolean equals(Booked other)
  {
    boolean equalsReservation   = this.reservation_.equals(other.reservation_);
    boolean equalsAccount       = this.account_.equals(other.account_);
    boolean equalsFlight        = this.flight_.equals(other.flight_);
    boolean equalsFlightOrder   = this.flightOrder_ == other.flightOrder_;
    boolean equalsDepartureDate =
                               this.departureDate_.equals(other.departureDate_);
    boolean equalsSeatNumber    = this.seatNumber_ == other.seatNumber_;
    boolean equalsSpecialMeal   = this.specialMeal_ == other.specialMeal_;

    if (equalsReservation
        && equalsAccount
        && equalsFlight
        && equalsFlightOrder
        && equalsDepartureDate
        && equalsSeatNumber
        && equalsSpecialMeal)
      return true;
    else
      return false;
  }

  /**
   * Returns a string representation of this object.
   *
   * @return a string describing the state of this object
   */
  public String toString()
  {
    return String.format(Reservation.FORMAT_ID, reservation_.getID())
         + "   "
         + String.format(Account.FORMAT_ID, account_.getID())
         + "   "
         + String.format(Flight.FORMAT_ID, flight_.getID())
         + "   "
         + String.format(Airline.FORMAT_ID, flight_.getAirline().getID())
         + "   "
         + String.format(FORMAT_FLIGHT_ORDER, flightOrder_)
         + "   "
         + String.format(FORMAT_DEPARTURE_DATE, departureDate_)
         + "   "
         + String.format(FORMAT_SEAT_NUMBER, seatNumber_)
         + "   "
         + String.format(FORMAT_SPECIAL_MEAL, specialMeal_);
  }
}
