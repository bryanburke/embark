////////////////////////////////////////////////////////////////////////////////
// Bryan Burke, Joshua Lambert, and Elizabeth Rothermel                       //
// Group 4                                                                    //
// INFX 499                                                                   //
// Fall 2015                                                                  //
// University of Louisiana at Lafayette                                       //
////////////////////////////////////////////////////////////////////////////////

import java.sql.Date;
import java.util.Calendar;

/**
 * Provides methods and constants to encode data for use with the database.
 *
 * @author Bryan Burke
 */
public final class DBEncoder
{
  //////////////////////////////////////////////////////////////////////////////
  // CLASS VARIABLES                                                          //
  //////////////////////////////////////////////////////////////////////////////

  /**
   * Value of the database attribute <code>reservation.flight_type</code> that
   * indicates a one way flight.
   */
  public static final int FLIGHT_ONE_WAY    = 0;
  /**
   * Value of the database attribute <code>reservation.flight_type</code> that
   * indicates a round trip flight.
   */
  public static final int FLIGHT_ROUND_TRIP = 1;
  /**
   * Value of the database attribute <code>reservation.flight_type</code> that
   * indicates a multi-city flight.
   */
  public static final int FLIGHT_MULTI_CITY = 2;



  /**
   * Value of the database attribute <code>reservation.flight_class</code> that
   * indicates a first class flight.
   */
  public static final int CLASS_FIRST    = 0;
  /**
   * Value of the database attribute <code>reservation.flight_class</code> that
   * indicates a business class flight.
   */
  public static final int CLASS_BUSINESS = 1;
  /**
   * Value of the database attribute <code>reservation.flight_class</code> that
   * indicates an economy class flight.
   */
  public static final int CLASS_ECONOMY  = 2;



  /**
   * Value of the database attribute <code>preference.preference_type</code>
   * that indicates a seat preference.
   */
  public static final int PREFERENCE_SEAT = 0;
  /**
   * Value of the database attribute <code>preference.preference_type</code>
   * that indicates a meal preference.
   */
  public static final int PREFERENCE_MEAL = 1;



  /**
   * Value of the database attribute <code>preference.seat_choice</code> that
   * indicates an aisle seat.
   */
  public static final int SEAT_AISLE  = 0;
  /**
   * Value of the database attribute <code>preference.seat_choice</code> that
   * indicates a window seat.
   */
  public static final int SEAT_WINDOW = 1;



  /**
   * Value of the database attributes <code>preference.meal_choice</code> and
   * <code>booked.special_meal</code> that indicates no meal.
   */
  public static final int MEAL_NONE        = 0;
  /**
   * Value of the database attributes <code>preference.meal_choice</code> and
   * <code>booked.special_meal</code> that indicates a kosher meal.
   */
  public static final int MEAL_KOSHER      = 1;
  /**
   * Value of the database attributes <code>preference.meal_choice</code> and
   * <code>booked.special_meal</code> that indicates a halal meal.
   */
  public static final int MEAL_HALAL       = 2;
  /**
   * Value of the database attributes <code>preference.meal_choice</code> and
   * <code>booked.special_meal</code> that indicates a vegetarian meal.
   */
  public static final int MEAL_VEGETARIAN  = 3;
  /**
   * Value of the database attributes <code>preference.meal_choice</code> and
   * <code>booked.special_meal</code> that indicates a vegan meal.
   */
  public static final int MEAL_VEGAN       = 4;
  /**
   * Value of the database attributes <code>preference.meal_choice</code> and
   * <code>booked.special_meal</code> that indicates a diabetic meal.
   */
  public static final int MEAL_DIABETIC    = 5;
  /**
   * Value of the database attributes <code>preference.meal_choice</code> and
   * <code>booked.special_meal</code> that indicates a gluten-free meal.
   */
  public static final int MEAL_GLUTEN_FREE = 6;



  /**
   * Value of the database attribute
   * <code>fare_restriction.restriction_type</code> that indicates an advance
   * days restriction.
   */
  public static final int RESTRICTION_ADVANCE = 0;
  /**
   * Value of the database attribute
   * <code>fare_restriction.restriction_type</code> that indicates a minimum
   * stay restriction.
   */
  public static final int RESTRICTION_MINIMUM = 1;
  /**
   * Value of the database attribute
   * <code>fare_restriction.restriction_type</code> that indicates a maximum
   * stay restriction.
   */
  public static final int RESTRICTION_MAXIMUM = 2;



  /**
   * Value of the database attribute <code>customer.rating</code> that indicates
   * a low rating.
   */
  public static final int RATING_LOW    = 0;
  /**
   * Value of the database attribute <code>customer.rating</code> that indicates
   * a medium rating.
   */
  public static final int RATING_MEDIUM = 1;
  /**
   * Value of the database attribute <code>customer.rating</code> that indicates
   * a high rating.
   */
  public static final int RATING_HIGH   = 2;



  //////////////////////////////////////////////////////////////////////////////
  // CONSTRUCTORS                                                             //
  //////////////////////////////////////////////////////////////////////////////

  // This class is non-constructable.
  private DBEncoder() {}



  //////////////////////////////////////////////////////////////////////////////
  // METHODS                                                                  //
  //////////////////////////////////////////////////////////////////////////////

  /**
   * Encodes a date's day of the week to a form useable with the database.
   * <p>
   * Returns the day of the week encoded as follows:
   * <ul>
   * <li>"N" = Sunday
   * <li>"M" = Monday
   * <li>"T" = Tuesday
   * <li>"W" = Wednesday
   * <li>"R" = Thursday
   * <li>"F" = Friday
   * <li>"S" = Saturday
   * </ul>
   *
   * @param date a <code>java.sql.Date</code> object that maps the date's ISO
   *             8601 (yyyy-mm-dd) representation
   * @return     the day of the week associated with the date
   */
  public static String encodeDayOfWeek(Date date)
  {
    Calendar calendar = Calendar.getInstance();
    calendar.setTime(date);

    int day = calendar.get(Calendar.DAY_OF_WEEK);
    if (day == Calendar.SUNDAY)
      return "N";
    else if (day == Calendar.MONDAY)
      return "M";
    else if (day == Calendar.TUESDAY)
      return "T";
    else if (day == Calendar.WEDNESDAY)
      return "W";
    else if (day == Calendar.THURSDAY)
      return "R";
    else if (day == Calendar.FRIDAY)
      return "F";
    else
      return "S";
  }
}
