////////////////////////////////////////////////////////////////////////////////
// Bryan Burke, Joshua Lambert, and Elizabeth Rothermel                       //
// Group 4                                                                    //
// INFX 499                                                                   //
// Fall 2015                                                                  //
// University of Louisiana at Lafayette                                       //
////////////////////////////////////////////////////////////////////////////////

import java.io.IOException;
import java.sql.Date;
import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

/**
 * Performs rudimentary unit testing on <code>Booked</code> objects.
 *
 * @author Bryan Burke
 */
public class TestBooked
{
  //////////////////////////////////////////////////////////////////////////////
  // METHODS                                                                  //
  //////////////////////////////////////////////////////////////////////////////

  /**
   * Creates and tests <code>Booked</code> objects.
   *
   * @param args command-line arguments; unused
   */
  public static void main(String[] args) throws IOException, ParseException
  {
    ////////////////////////////////////////////////////////////////////////////
    // TEST CONSTRUCTORS                                                      //
    ////////////////////////////////////////////////////////////////////////////

    System.out.println("== BEGIN CONSTRUCTOR TESTS ==");

    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");

    // Prerequisite objects.
    Employee    sigdelP = new Employee(2,
                                       "Sigdel",
                                       "Purushottam",
                                       "304 Kenwood Place",
                                       "Lafayette",
                                       "LA",
                                       "70504",
                                       "3375833335",
                                       "26560XXXX",
                             new Date(dateFormat.parse("2008-04-19").getTime()),
                                       20.0,
                                       1);
    Reservation r1      = new Reservation(1,
                             new Date(dateFormat.parse("2009-07-19").getTime()),
                                          DBEncoder.FLIGHT_ONE_WAY,
                                          DBEncoder.CLASS_ECONOMY,
                                          101.0,
                                          10.1,
                                          sigdelP);
    Customer    patelK  = new Customer(5,
                                       "Patel",
                                       "Kimberly",
                                       "3548 Orchard Street",
                                       "Osseo",
                                       "MN",
                                       "55369",
                                       "9528552611",
                                       2);
    Account     a5      = new Account(5,
                                      patelK,
                                      "patel.kimberly@locationcalculator.com",
                                      "542802306410XXXX",
                            new Date(dateFormat.parse("2009-07-19").getTime()));
    Airline     aa      = new Airline("AA",
                                      "American Airlines");
    Airport     lft     = new Airport("LFT",
                                      "Lafayette Regional Airport",
                                      "Lafayette",
                                      "LA",
                                      "US");
    Airport     dfw     = new Airport("DFW",
                                      "Dallas/Fort Worth International Airport",
                                      "Dallas/Fort Worth",
                                      "TX",
                                      "US");
    Flight      aa3108  = new Flight("3108",
                                     aa,
                                     lft,
                                     dfw,
                                     "NMTWRFS",
                               new Time(timeFormat.parse("17:55:00").getTime()),
                               new Time(timeFormat.parse("19:32:00").getTime()),
                                     101,
                                     144.0,
                                     129.6);
    Reservation r3      = new Reservation(3,
                             new Date(dateFormat.parse("2012-02-28").getTime()),
                                          DBEncoder.FLIGHT_ONE_WAY,
                                          DBEncoder.CLASS_ECONOMY,
                                          96.0,
                                          9.6,
                                          sigdelP);
    Account     a6      = new Account(6,
                                      patelK,
                                      "patel.kimberly@talkscope.com",
                                      "559652950074XXXX",
                            new Date(dateFormat.parse("2012-02-28").getTime()));

    ArrayList<Booked> bookings = new ArrayList<>(3);

    // Booked(Reservation, Account, Flight, int, Date, int, int)
    bookings.add(new Booked(r1,
                            a5,
                            aa3108,
                            1,
                            new Date(dateFormat.parse("2009-07-24").getTime()),
                            84,
                            DBEncoder.MEAL_NONE));
    System.out.println("  Booking for reservation #1, account #5, and flight "
                     + "AA 3108 constructed as bookings[0].");

    // Booked(Reservation, Account, Flight, int, Date, int)
    bookings.add(new Booked(r3,
                            a6,
                            aa3108,
                            1,
                            new Date(dateFormat.parse("2012-03-19").getTime()),
                            45));
    System.out.println("  Booking for reservation #3, account #6, and flight "
                     + "AA 3108 constructed as bookings[1].");

    // Booked(Booked)
    bookings.add(new Booked(bookings.get(0)));
    System.out.println("  Booking for reservation #1, account #5, and flight "
                     + "AA 3108 copy constructed as bookings[2].");

    System.out.println("== END CONSTRUCTOR TESTS ==");
    System.out.println();



    ////////////////////////////////////////////////////////////////////////////
    // TEST METHODS                                                           //
    ////////////////////////////////////////////////////////////////////////////

    System.out.println("== BEGIN METHOD TESTS ==");

    // getReservation()
    System.out.println("= getReservation() =");
    for (int i = 0; i < bookings.size(); ++i)
      System.out.printf("  [%d] : %s\n",
                        i,
                        bookings.get(i).getReservation());
    System.out.print("Verify the results and press ENTER to continue.");
    System.in.read();

    // getAccount()
    System.out.println("= getAccount() =");
    for (int i = 0; i < bookings.size(); ++i)
      System.out.printf("  [%d] : %s\n",
                        i,
                        bookings.get(i).getAccount());
    System.out.print("Verify the results and press ENTER to continue.");
    System.in.read();

    // getFlight()
    System.out.println("= getFlight() =");
    for (int i = 0; i < bookings.size(); ++i)
      System.out.printf("  [%d] : %s\n",
                        i,
                        bookings.get(i).getFlight());
    System.out.print("Verify the results and press ENTER to continue.");
    System.in.read();

    // getFlightOrder()
    System.out.println("= getFlightOrder() =");
    for (int i = 0; i < bookings.size(); ++i)
      System.out.printf("  [%d] : %d\n",
                        i,
                        bookings.get(i).getFlightOrder());
    System.out.print("Verify the results and press ENTER to continue.");
    System.in.read();

    // getDepartureDate()
    System.out.println("= getDepartureDate() =");
    for (int i = 0; i < bookings.size(); ++i)
      System.out.printf("  [%d] : %s\n",
                        i,
                        bookings.get(i).getDepartureDate());
    System.out.print("Verify the results and press ENTER to continue.");
    System.in.read();

    // getSeatNumber()
    System.out.println("= getSeatNumber() =");
    for (int i = 0; i < bookings.size(); ++i)
      System.out.printf("  [%d] : %d\n",
                        i,
                        bookings.get(i).getSeatNumber());
    System.out.print("Verify the results and press ENTER to continue.");
    System.in.read();

    // getSpecialMeal()
    System.out.println("= getSpecialMeal() =");
    for (int i = 0; i < bookings.size(); ++i)
      System.out.printf("  [%d] : %d\n",
                        i,
                        bookings.get(i).getSpecialMeal());
    System.out.print("Verify the results and press ENTER to continue.");
    System.in.read();

    // equals(Booked)
    System.out.println("= equals([0]) =");
    for (int i = 0; i < bookings.size(); ++i)
      System.out.printf("  [%d] : %s\n",
                        i,
                        bookings.get(i).equals(bookings.get(0)));
    System.out.print("Verify the results and press ENTER to continue.");
    System.in.read();

    // toString()
    System.out.println("= toString() =");
    for (int i = 0; i < bookings.size(); ++i)
      System.out.printf("  [%d] : %s\n",
                        i,
                        bookings.get(i).toString());
    System.out.print("Verify the results and press ENTER to continue.");
    System.in.read();

    System.out.println("== END METHOD TESTS ==");
  }
}
