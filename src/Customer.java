////////////////////////////////////////////////////////////////////////////////
// Bryan Burke, Joshua Lambert, and Elizabeth Rothermel                       //
// Group 4                                                                    //
// INFX 499                                                                   //
// Fall 2015                                                                  //
// University of Louisiana at Lafayette                                       //
////////////////////////////////////////////////////////////////////////////////

/**
 * Provides an in-memory mapping of a tuple from the <code>customer</code>
 * database relation.
 *
 * @author Bryan Burke
 */
public class Customer
{
  //////////////////////////////////////////////////////////////////////////////
  // CLASS VARIABLES                                                          //
  //////////////////////////////////////////////////////////////////////////////

  /**
   * Format specifier for <code>customer.customer_id</code>.
   */
  public static final String FORMAT_ID         = "%8d";
  /**
   * Format specifier for <code>customer.last_name</code>.
   */
  public static final String FORMAT_LAST_NAME  = "%20.20s";
  /**
   * Format specifier for <code>customer.first_name</code>.
   */
  public static final String FORMAT_FIRST_NAME = "%20.20s";
  /**
   * Format specifier for <code>customer.address</code>.
   */
  public static final String FORMAT_ADDRESS    = "%30.30s";
  /**
   * Format specifier for <code>customer.city</code>.
   */
  public static final String FORMAT_CITY       = "%20.20s";
  /**
   * Format specifier for <code>customer.state</code>.
   */
  public static final String FORMAT_STATE      = "%2.2s";
  /**
   * Format specifier for <code>customer.zip_code</code>.
   */
  public static final String FORMAT_ZIP_CODE   = "%5.5s";
  /**
   * Format specifier for <code>customer.telephone</code>.
   */
  public static final String FORMAT_TELEPHONE  = "%10.10s";
  /**
   * Format specifier for <code>customer.rating</code>.
   */
  public static final String FORMAT_RATING     = "%1d";



  /**
   * Maximum string length for <code>customer.last_name</code>.
   */
  public static final int MAX_LENGTH_LAST_NAME  = 50;
  /**
   * Maximum string length for <code>customer.first_name</code>.
   */
  public static final int MAX_LENGTH_FIRST_NAME = 50;
  /**
   * Maximum string length for <code>customer.address</code>.
   */
  public static final int MAX_LENGTH_ADDRESS    = 100;
  /**
   * Maximum string length for <code>customer.city</code>.
   */
  public static final int MAX_LENGTH_CITY       = 40;
  /**
   * Maximum string length for <code>customer.state</code>.
   */
  public static final int MAX_LENGTH_STATE      = 2;
  /**
   * Maximum string length for <code>customer.zip_code</code>.
   */
  public static final int MAX_LENGTH_ZIP_CODE   = 5;
  /**
   * Maximum string length for <code>customer.telephone</code>.
   */
  public static final int MAX_LENGTH_TELEPHONE  = 10;



  //////////////////////////////////////////////////////////////////////////////
  // INSTANCE VARIABLES                                                       //
  //////////////////////////////////////////////////////////////////////////////

  private int    id_;        // Auto-generated customer identifier.
  private String lastName_;  // Family name.
  private String firstName_; // Given name.
  private String address_;   // Mailing address.
  private String city_;      // City of residence.
  private String state_;     // State of residence.
  private String zipCode_;   // Mailing zip code.
  private String telephone_; // Phone number.
  private int    rating_;    // Customer activity rating.



  //////////////////////////////////////////////////////////////////////////////
  // CONSTRUCTORS                                                             //
  //////////////////////////////////////////////////////////////////////////////

  /**
   * Creates an object of this class with all instance variables initialized.
   * <p>
   * This constructor is used by <code>DBMapper.getCustomer(int)</code> and
   * should not be called directly by frontend code.
   *
   * @param id        the unique customer identifier
   * @param lastName  the family name
   * @param firstName the given name
   * @param address   the mailing address
   * @param city      the city of residence
   * @param state     the state of residence
   * @param zipCode   the mailing zip code
   * @param telephone the phone number
   * @param rating    the customer activity rating
   */
  public Customer(int    id,
                  String lastName,
                  String firstName,
                  String address,
                  String city,
                  String state,
                  String zipCode,
                  String telephone,
                  int    rating)
  {
    id_        = id;
    lastName_  = lastName;
    firstName_ = firstName;
    address_   = address;
    city_      = city;
    state_     = state;
    zipCode_   = zipCode;
    telephone_ = telephone;
    rating_    = rating;
  }

  /**
   * Creates an object of this class, initializing only those instance variables
   * necessary to add a new tuple to the <code>customer</code> relation.
   * <p>
   * This constructor should be called by frontend code when instantiating an
   * object to pass to <code>DBInserter.insertCustomer(Customer)</code>.
   *
   * @param lastName  the family name
   * @param firstName the given name
   * @param address   the mailing address
   * @param city      the city of residence
   * @param state     the state of residence
   * @param zipCode   the mailing zip code
   * @param telephone the phone number
   */
  public Customer(String lastName,
                  String firstName,
                  String address,
                  String city,
                  String state,
                  String zipCode,
                  String telephone)
  {
    id_        = 0;                    // Default database value.
    lastName_  = lastName;
    firstName_ = firstName;
    address_   = address;
    city_      = city;
    state_     = state;
    zipCode_   = zipCode;
    telephone_ = telephone;
    rating_    = DBEncoder.RATING_LOW; // Default database value.
  }

  /**
   * Creates a deep copy of an object of this class.
   *
   * @param other the object whose state should be copied
   */
  public Customer(Customer other)
  {
    this.id_        = other.id_;
    this.lastName_  = other.lastName_;
    this.firstName_ = other.firstName_;
    this.address_   = other.address_;
    this.city_      = other.city_;
    this.state_     = other.state_;
    this.zipCode_   = other.zipCode_;
    this.telephone_ = other.telephone_;
    this.rating_    = other.rating_;
  }



  //////////////////////////////////////////////////////////////////////////////
  // METHODS                                                                  //
  //////////////////////////////////////////////////////////////////////////////

  /**
   * Returns the customer number auto-generated by the database.
   *
   * @return the customer identifier
   */
  public int getID()
  {
    return id_;
  }

  /**
   * Returns the customer's legal surname.
   *
   * @return the family name
   */
  public String getLastName()
  {
    return lastName_;
  }

  /**
   * Returns the customer's legal forename.
   *
   * @return the given name
   */
  public String getFirstName()
  {
    return firstName_;
  }

  /**
   * Returns the USPS mailing address.
   *
   * @return the mailing address
   */
  public String getAddress()
  {
    return address_;
  }

  /**
   * Returns the city's name.
   *
   * @return the city of residence
   */
  public String getCity()
  {
    return city_;
  }

  /**
   * Returns the state's ISO 3166-2:US code.
   *
   * @return the state of residence
   */
  public String getState()
  {
    return state_;
  }

  /**
   * Returns the USPS postal code.
   *
   * @return the mailing zip code
   */
  public String getZipCode()
  {
    return zipCode_;
  }

  /**
   * Returns the NANP telephone number.
   *
   * @return the phone number
   */
  public String getTelephone()
  {
    return telephone_;
  }

  /**
   * Returns the rating encoded as follows:
   * <ul>
   * <li>0 = low
   * <li>1 = medium
   * <li>2 = high
   * </ul>
   *
   * @return the customer activity rating
   */
  public int getRating()
  {
    return rating_;
  }



  /**
   * Sets the customer's family name.
   *
   * @param lastName the legal surname
   */
  public void setLastName(String lastName)
  {
    lastName_ = lastName;
  }

  /**
   * Sets the customer's given name.
   *
   * @param firstName the legal forename
   */
  public void setFirstName(String firstName)
  {
    firstName_ = firstName;
  }

  /**
   * Sets the mailing address.
   *
   * @param address the USPS mailing address
   */
  public void setAddress(String address)
  {
    address_ = address;
  }

  /**
   * Sets the city of residence.
   *
   * @param city the city's name
   */
  public void setCity(String city)
  {
    city_ = city;
  }

  /**
   * Sets the state of residence.
   *
   * @param state the state's ISO 3166-2:US code
   */
  public void setState(String state)
  {
    state_ = state;
  }

  /**
   * Sets the mailing zip code.
   *
   * @param zipCode the USPS postal code
   */
  public void setZipCode(String zipCode)
  {
    zipCode_ = zipCode;
  }

  /**
   * Sets the phone number.
   *
   * @param telephone the NANP telephone number
   */
  public void setTelephone(String telephone)
  {
    telephone_ = telephone;
  }

  /**
   * Sets the customer activity rating.
   *
   * @param rating the rating encoded as <code>0</code> for low, <code>1</code>
   *               for medium, or <code>2</code> for high
   */
  public void setRating(int rating)
  {
    rating_ = rating;
  }



  /**
   * Peforms an equality comparison on two objects of this class.
   *
   * @param other the object whose state should be compared with the state of
   *              this object
   * @return      <code>true</code> if the object states are equal;
   *              <code>false</code> otherwise
   */
  public boolean equals(Customer other)
  {
    boolean equalsID        = this.id_ == other.id_;
    boolean equalsLastName  = this.lastName_.equals(other.lastName_);
    boolean equalsFirstName = this.firstName_.equals(other.firstName_);
    boolean equalsAddress   = this.address_.equals(other.address_);
    boolean equalsCity      = this.city_.equals(other.city_);
    boolean equalsState     = this.state_.equals(other.state_);
    boolean equalsZipCode   = this.zipCode_.equals(other.zipCode_);
    boolean equalsTelephone = this.telephone_.equals(other.telephone_);
    boolean equalsRating    = this.rating_ == other.rating_;

    if (equalsID
        && equalsLastName
        && equalsFirstName
        && equalsAddress
        && equalsCity
        && equalsState
        && equalsZipCode
        && equalsTelephone
        && equalsRating)
      return true;
    else
      return false;
  }

  /**
   * Returns a string representation of this object.
   *
   * @return a string describing the state of this object
   */
  public String toString()
  {
    return String.format(FORMAT_ID, id_)
         + "   "
         + String.format(FORMAT_LAST_NAME, lastName_)
         + "   "
         + String.format(FORMAT_FIRST_NAME, firstName_)
         + "   "
         + String.format(FORMAT_ADDRESS, address_)
         + "   "
         + String.format(FORMAT_CITY, city_)
         + "   "
         + String.format(FORMAT_STATE, state_)
         + "   "
         + String.format(FORMAT_ZIP_CODE, zipCode_)
         + "   "
         + String.format(FORMAT_TELEPHONE, telephone_)
         + "   "
         + String.format(FORMAT_RATING, rating_);
  }
}
