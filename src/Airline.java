////////////////////////////////////////////////////////////////////////////////
// Bryan Burke, Joshua Lambert, and Elizabeth Rothermel                       //
// Group 4                                                                    //
// INFX 499                                                                   //
// Fall 2015                                                                  //
// University of Louisiana at Lafayette                                       //
////////////////////////////////////////////////////////////////////////////////

/**
 * Provides an in-memory mapping of a tuple from the <code>airline</code>
 * database relation.
 *
 * @author Bryan Burke
 */
public class Airline
{
  //////////////////////////////////////////////////////////////////////////////
  // CLASS VARIABLES                                                          //
  //////////////////////////////////////////////////////////////////////////////

  /**
   * Format specifier for <code>airline.airline_id</code>.
   */
  public static final String FORMAT_ID   = "%2.2s";
  /**
   * Format specifier for <code>airline.name</code>.
   */
  public static final String FORMAT_NAME = "%15.15s";



  //////////////////////////////////////////////////////////////////////////////
  // INSTANCE VARIABLES                                                       //
  //////////////////////////////////////////////////////////////////////////////

  private String id_;   // Airline identifier.
  private String name_; // Official name.



  //////////////////////////////////////////////////////////////////////////////
  // CONSTRUCTORS                                                             //
  //////////////////////////////////////////////////////////////////////////////

  /**
   * Creates an object of this class with all instance variables initialized.
   * <p>
   * This constructor is used by <code>DBMapper.getAirline(String)</code> and
   * should not be called directly by frontend code.
   *
   * @param id   the unique airline identifier
   * @param name the official name of the airline
   */
  public Airline(String id,
                 String name)
  {
    id_   = id;
    name_ = name;
  }

  /**
   * Creates a deep copy of an object of this class.
   *
   * @param other the object whose state should be copied
   */
  public Airline(Airline other)
  {
    this.id_   = other.id_;
    this.name_ = other.name_;
  }



  //////////////////////////////////////////////////////////////////////////////
  // METHODS                                                                  //
  //////////////////////////////////////////////////////////////////////////////

  /**
   * Returns the airline's IATA code.
   *
   * @return the airline identifier
   */
  public String getID()
  {
    return id_;
  }

  /**
   * Returns the airline's official name.
   *
   * @return the official name
   */
  public String getName()
  {
    return name_;
  }



  /**
   * Peforms an equality comparison on two objects of this class.
   *
   * @param other the object whose state should be compared with the state of
   *              this object
   * @return      <code>true</code> if the object states are equal;
   *              <code>false</code> otherwise
   */
  public boolean equals(Airline other)
  {
    boolean equalsID   = this.id_.equals(other.id_);
    boolean equalsName = this.name_.equals(other.name_);

    if (equalsID
        && equalsName)
      return true;
    else
      return false;
  }

  /**
   * Returns a string representation of this object.
   *
   * @return a string describing the state of this object
   */
  public String toString()
  {
    return String.format(FORMAT_ID, id_)
         + "   "
         + String.format(FORMAT_NAME, name_);
  }
}
