////////////////////////////////////////////////////////////////////////////////
// Bryan Burke, Joshua Lambert, and Elizabeth Rothermel                       //
// Group 4                                                                    //
// INFX 499                                                                   //
// Fall 2015                                                                  //
// University of Louisiana at Lafayette                                       //
////////////////////////////////////////////////////////////////////////////////

import java.sql.Time;

/**
 * Provides an in-memory mapping of a tuple from the <code>flight</code>
 * database relation.
 *
 * @author Bryan Burke
 */
public class Flight
{
  //////////////////////////////////////////////////////////////////////////////
  // CLASS VARIABLES                                                          //
  //////////////////////////////////////////////////////////////////////////////

  /**
   * Format specifier for <code>flight.flight_id</code>.
   */
  public static final String FORMAT_ID             = "%4.4s";
  /**
   * Format specifier for <code>flight.departure_days</code>.
   */
  public static final String FORMAT_DEPARTURE_DAYS = "%7.7s";
  /**
   * Format specifier for <code>flight.departure_time</code>.
   */
  public static final String FORMAT_DEPARTURE_TIME = "%8.8s";
  /**
   * Format specifier for <code>flight.arrival_time</code>.
   */
  public static final String FORMAT_ARRIVAL_TIME   = "%8.8s";
  /**
   * Format specifier for <code>flight.capacity</code>.
   */
  public static final String FORMAT_CAPACITY       = "%3d";
  /**
   * Format specifier for <code>flight.fare</code>.
   */
  public static final String FORMAT_FARE           = "%9.2f";



  //////////////////////////////////////////////////////////////////////////////
  // INSTANCE VARIABLES                                                       //
  //////////////////////////////////////////////////////////////////////////////

  private String  id_;                 // Flight identifier.
  private Airline airline_;            // Operating airline.
  private Airport originAirport_;      // Starting airport.
  private Airport destinationAirport_; // Ending airport.
  private String  departureDays_;      // Days when flight operates.
  private Time    departureTime_;      // Local time when flight departs.
  private Time    arrivalTime_;        // Local time when flight arrives.
  private int     capacity_;           // Number of seats on flight.
  private double  fare_;               // Per-person economy price.
  private double  reserveFare_;        // Minimum accepted economy price.



  //////////////////////////////////////////////////////////////////////////////
  // CONSTRUCTORS                                                             //
  //////////////////////////////////////////////////////////////////////////////

  /**
   * Creates an object of this class with all instance variables initialized.
   * <p>
   * This constructor is used by
   * <code>DBMapper.getFlight(String, Airline)</code> and should not be called
   * directly by frontend code.
   *
   * @param id                 the unique flight identifier
   * @param airline            the operating airline
   * @param originAirport      the starting airport
   * @param destinationAirport the ending airport
   * @param departureDays      the days when the flight operates
   * @param departureTime      the local time when the flight departs
   * @param arrivalTime        the local time when the flight arrives
   * @param capacity           the number of seats on the flight
   * @param fare               the per-person economy price
   * @param reserveFare        the minimum accepted economy price
   */
  public Flight(String  id,
                Airline airline,
                Airport originAirport,
                Airport destinationAirport,
                String  departureDays,
                Time    departureTime,
                Time    arrivalTime,
                int     capacity,
                double  fare,
                double  reserveFare)
  {
    id_                 = id;
    airline_            = airline;            // Immutable.
    originAirport_      = originAirport;      // Immutable.
    destinationAirport_ = destinationAirport; // Immutable.
    departureDays_      = departureDays;
    departureTime_      = new Time(departureTime.getTime());
    arrivalTime_        = new Time(arrivalTime.getTime());
    capacity_           = capacity;
    fare_               = fare;
    reserveFare_        = reserveFare;
  }

  /**
   * Creates a deep copy of an object of this class.
   *
   * @param other the object whose state should be copied
   */
  public Flight(Flight other)
  {
    this.id_                 = other.id_;
    this.airline_            = other.airline_;            // Immutable.
    this.originAirport_      = other.originAirport_;      // Immutable.
    this.destinationAirport_ = other.destinationAirport_; // Immutable.
    this.departureDays_      = other.departureDays_;
    this.departureTime_      = new Time(other.departureTime_.getTime());
    this.arrivalTime_        = new Time(other.arrivalTime_.getTime());
    this.capacity_           = other.capacity_;
    this.fare_               = other.fare_;
    this.reserveFare_        = other.reserveFare_;
  }



  //////////////////////////////////////////////////////////////////////////////
  // METHODS                                                                  //
  //////////////////////////////////////////////////////////////////////////////

  /**
   * Returns the flight number assigned by the operating airline.
   *
   * @return the flight identifier
   */
  public String getID()
  {
    return id_;
  }

  /**
   * Returns an <code>Airline</code> object that maps the operating airline's
   * relational data.
   *
   * @return the operating airline
   */
  public Airline getAirline()
  {
    return airline_; // Immutable.
  }

  /**
   * Returns an <code>Airport</code> object that maps the origin airport's
   * relational data.
   *
   * @return the starting airport
   */
  public Airport getOriginAirport()
  {
    return originAirport_; // Immutable.
  }

  /**
   * Returns an <code>Airport</code> object that maps the destination airport's
   * relational data.
   *
   * @return the ending airport
   */
  public Airport getDestinationAirport()
  {
    return destinationAirport_; // Immutable.
  }

  /**
   * Returns the days of the week encoded as "NMTWRFS" with "-" representing an
   * inactive day.
   *
   * @return the days when the flight operates
   */
  public String getDepartureDays()
  {
    return departureDays_;
  }

  /**
   * Returns a <code>java.sql.Time</code> object that maps the departure time's
   * ISO 8601 (hh:mm:ss) representation.
   *
   * @return the local time when the flight departs
   */
  public Time getDepartureTime()
  {
    return new Time(departureTime_.getTime());
  }

  /**
   * Returns a <code>java.sql.Time</code> object that maps the arrival time's
   * ISO 8601 (hh:mm:ss) representation.
   *
   * @return the local time when the flight arrives
   */
  public Time getArrivalTime()
  {
    return new Time(arrivalTime_.getTime());
  }

  /**
   * Returns the total number of seats the flight can accommodate.
   *
   * @return the number of seats on the flight
   */
  public int getCapacity()
  {
    return capacity_;
  }

  /**
   * Returns the USD price for a single seat in economy class.
   *
   * @return the per-person economy price
   */
  public double getFare()
  {
    return fare_;
  }

  /**
   * Returns the bare minimum USD economy class price that can be accepted in a
   * reverse auction.
   *
   * @return the minimum accepted economy price
   */
  public double getReserveFare()
  {
    return reserveFare_;
  }



  /**
   * Peforms an equality comparison on two objects of this class.
   *
   * @param other the object whose state should be compared with the state of
   *              this object
   * @return      <code>true</code> if the object states are equal;
   *              <code>false</code> otherwise
   */
  public boolean equals(Flight other)
  {
    boolean equalsID            = this.id_.equals(other.id_);
    boolean equalsAirline       = this.airline_.equals(other.airline_);
    boolean equalsOrigin        =
                               this.originAirport_.equals(other.originAirport_);
    boolean equalsDestination   =
                     this.destinationAirport_.equals(other.destinationAirport_);
    boolean equalsDepartureDays =
                               this.departureDays_.equals(other.departureDays_);
    boolean equalsDepartureTime =
                               this.departureTime_.equals(other.departureTime_);
    boolean equalsArrivalTime   = this.arrivalTime_.equals(other.arrivalTime_);
    boolean equalsCapacity      = this.capacity_ == other.capacity_;
    boolean equalsFare          = this.fare_ == other.fare_;
    boolean equalsReserveFare   = this.reserveFare_ == other.reserveFare_;

    if (equalsID
        && equalsAirline
        && equalsOrigin
        && equalsDestination
        && equalsDepartureDays
        && equalsDepartureTime
        && equalsArrivalTime
        && equalsCapacity
        && equalsFare
        && equalsReserveFare)
      return true;
    else
      return false;
  }

  /**
   * Returns a string representation of this object.
   *
   * @return a string describing the state of this object
   */
  public String toString()
  {
    return String.format(FORMAT_ID, id_)
         + "   "
         + String.format(Airline.FORMAT_ID, airline_.getID())
         + "   "
         + String.format(Airport.FORMAT_ID, originAirport_.getID())
         + "   "
         + String.format(Airport.FORMAT_ID, destinationAirport_.getID())
         + "   "
         + String.format(FORMAT_DEPARTURE_DAYS, departureDays_)
         + "   "
         + String.format(FORMAT_DEPARTURE_TIME, departureTime_)
         + "   "
         + String.format(FORMAT_ARRIVAL_TIME, arrivalTime_)
         + "   "
         + String.format(FORMAT_CAPACITY, capacity_)
         + "   "
         + String.format(FORMAT_FARE, fare_);
    // flight.reserve_fare is hidden from the user.
  }
}
