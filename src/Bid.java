////////////////////////////////////////////////////////////////////////////////
// Bryan Burke, Joshua Lambert, and Elizabeth Rothermel                       //
// Group 4                                                                    //
// INFX 499                                                                   //
// Fall 2015                                                                  //
// University of Louisiana at Lafayette                                       //
////////////////////////////////////////////////////////////////////////////////

/**
 * Provides an in-memory mapping of a tuple from the <code>bid</code> database
 * relation.
 *
 * @author Bryan Burke
 */
public class Bid
{
  //////////////////////////////////////////////////////////////////////////////
  // CLASS VARIABLES                                                          //
  //////////////////////////////////////////////////////////////////////////////

  /**
   * Format specifier for <code>bid.amount</code>.
   */
  public static final String FORMAT_AMOUNT = "%9.2f";



  //////////////////////////////////////////////////////////////////////////////
  // INSTANCE VARIABLES                                                       //
  //////////////////////////////////////////////////////////////////////////////

  private Reservation reservation_; // Associated reservation.
  private Account     account_;     // Associated account.
  private double      amount_;      // Amount of bid.



  //////////////////////////////////////////////////////////////////////////////
  // CONSTRUCTORS                                                             //
  //////////////////////////////////////////////////////////////////////////////

  /**
   * Creates an object of this class with all instance variables initialized.
   * <p>
   * This constructor is used by
   * <code>DBMapper.getBid(Reservation, Account, double)</code> and should also
   * be called by frontend code when instantiating an object to pass to
   * <code>DBInserter.insertBid(Bid)</code>.
   *
   * @param reservation the associated reservation
   * @param account     the associated account
   * @param amount      the amount of the bid
   */
  public Bid(Reservation reservation,
             Account     account,
             double      amount)
  {
    reservation_ = reservation; // Immutable.
    account_     = new Account(account);
    amount_      = amount;
  }

  /**
   * Creates a deep copy of an object of this class.
   *
   * @param other the object whose state should be copied
   */
  public Bid(Bid other)
  {
    this.reservation_ = other.reservation_; // Immutable.
    this.account_     = new Account(other.account_);
    this.amount_      = other.amount_;
  }



  //////////////////////////////////////////////////////////////////////////////
  // METHODS                                                                  //
  //////////////////////////////////////////////////////////////////////////////

  /**
   * Returns a <code>Reservation</code> object that maps the associated
   * reservation's relational data.
   *
   * @return the associated reservation
   */
  public Reservation getReservation()
  {
    return reservation_; // Immutable.
  }

  /**
   * Returns an <code>Account</code> object that maps the associated account's
   * relational data.
   *
   * @return the associated account
   */
  public Account getAccount()
  {
    return new Account(account_);
  }

  /**
   * Returns the USD amount bid by the account for the reservation.
   *
   * @return the amount of the bid
   */
  public double getAmount()
  {
    return amount_;
  }



  /**
   * Peforms an equality comparison on two objects of this class.
   *
   * @param other the object whose state should be compared with the state of
   *              this object
   * @return      <code>true</code> if the object states are equal;
   *              <code>false</code> otherwise
   */
  public boolean equals(Bid other)
  {
    boolean equalsReservation = this.reservation_.equals(other.reservation_);
    boolean equalsAccount     = this.account_.equals(other.account_);
    boolean equalsAmount      = this.amount_ == other.amount_;

    if (equalsReservation
        && equalsAccount
        && equalsAmount)
      return true;
    else
      return false;
  }

  /**
   * Returns a string representation of this object.
   *
   * @return a string describing the state of this object
   */
  public String toString()
  {
    return String.format(Reservation.FORMAT_ID, reservation_.getID())
         + "   "
         + String.format(Account.FORMAT_ID, account_.getID())
         + "   "
         + String.format(FORMAT_AMOUNT, amount_);
  }
}
