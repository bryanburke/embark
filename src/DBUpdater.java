////////////////////////////////////////////////////////////////////////////////
// Bryan Burke, Joshua Lambert, and Elizabeth Rothermel                       //
// Group 4                                                                    //
// INFX 499                                                                   //
// Fall 2015                                                                  //
// University of Louisiana at Lafayette                                       //
////////////////////////////////////////////////////////////////////////////////

import java.sql.SQLException;
import java.sql.SQLWarning;
import java.sql.Statement;

/**
 * Provides methods to update tuples from certain database relations.
 *
 * @author Bryan Burke
 */
public final class DBUpdater
{
  //////////////////////////////////////////////////////////////////////////////
  // CLASS VARIABLES                                                          //
  //////////////////////////////////////////////////////////////////////////////

  /**
   * Error message describing the most recent database access error.
   */
  public static String error = null;



  //////////////////////////////////////////////////////////////////////////////
  // CONSTRUCTORS                                                             //
  //////////////////////////////////////////////////////////////////////////////

  // This class is non-constructable.
  private DBUpdater() {}



  //////////////////////////////////////////////////////////////////////////////
  // METHODS                                                                  //
  //////////////////////////////////////////////////////////////////////////////

  /**
   * Updates the tuple from the <code>customer</code> database relation with the
   * matching <code>customer_id</code> attribute.
   * <p>
   * If update is successful, returns <code>true</code>.
   * <p>
   * If update fails, returns <code>false</code> and assigns a user-friendly
   * error message to <code>error</code>.
   *
   * @param customer a <code>Customer</code> object representing the tuple to
   *                 update
   * @return         <code>true</code> if update succeeded; <code>false</code>
   *                 otherwise
   */
  public static boolean updateCustomer(Customer customer)
  {
    try {
      // Ready the SQL statement.
      String sql = String.format("UPDATE customer\n"
                               + "   SET last_name = '%s', first_name = '%s',"
                               + "       address = '%s', city = '%s',"
                               + "       state = '%s', zip_code = '%s',"
                               + "       telephone = '%s', rating = %d\n"
                               + " WHERE customer_id = %d",
                                 customer.getLastName(),
                                 customer.getFirstName(),
                                 customer.getAddress(),
                                 customer.getCity(),
                                 customer.getState(),
                                 customer.getZipCode(),
                                 customer.getTelephone(),
                                 customer.getRating(),
                                 customer.getID());

      // Attempt an update to the customer relation if the database connection
      // is active.
      Statement statement = DBConnector.createStatement(false);
      int       nRows     = 0;
      if (statement != null) // Database connection is active.
        nRows = statement.executeUpdate(sql);
      else { // Database connection is not active.
        error = DBConnector.error;

        return false;
      }

      boolean isUpdated = true;

      // Check for JDBC warnings.
      SQLWarning w = statement.getWarnings();
      if (w != null) {
        if (w.getSQLState().equals("02000")) {
          error = "Customer #"
                + customer.getID()
                + " was not found in our records.";

          isUpdated = false;
        }
      }

      // No JDBC errors occurred. Clean up.
      statement.close();

      return isUpdated;
    }
    catch (SQLException e) { // A JDBC error occurred.
      if (e.getSQLState().equals("42Z23"))
        error = "Customer IDs cannot be modified.";
      else {
        error = "He's dead, Jim. Please report this error.";
        e.printStackTrace(); // For debugging only.
      }

      return false;
    }
  }

  /**
   * Updates the tuple from the <code>account</code> database relation with the
   * matching <code>account_id</code> attribute.
   * <p>
   * If update is successful, returns <code>true</code>.
   * <p>
   * If update fails, returns <code>false</code> and assigns a user-friendly
   * error message to <code>error</code>.
   *
   * @param account an <code>Account</code> object representing the tuple to
   *                update
   * @return        <code>true</code> if update succeeded; <code>false</code>
   *                otherwise
   */
  public static boolean updateAccount(Account account)
  {
    try {
      // Ready the SQL statement.
      String sql = String.format("UPDATE account\n"
                               + "   SET email = '%s', credit_card = '%s'\n"
                               + " WHERE account_id = %d",
                                 account.getEmail(),
                                 account.getCreditCard(),
                                 account.getID());

      // Attempt an update to the account relation if the database connection is
      // active.
      Statement statement = DBConnector.createStatement(false);
      int       nRows     = 0;
      if (statement != null) // Database connection is active.
        nRows = statement.executeUpdate(sql);
      else { // Database connection is not active.
        error = DBConnector.error;

        return false;
      }

      boolean isUpdated = true;

      // Check for JDBC warnings.
      SQLWarning w = statement.getWarnings();
      if (w != null) {
        if (w.getSQLState().equals("02000")) {
          error = "Account #"
                + account.getID()
                + " was not found in our records.";

          isUpdated = false;
        }
      }

      // No JDBC errors occurred. Clean up.
      statement.close();

      return isUpdated;
    }
    catch (SQLException e) { // A JDBC error occurred.
      if (e.getSQLState().equals("42Z23"))
        error = "Account IDs cannot be modified.";
      else {
        error = "He's dead, Jim. Please report this error.";
        e.printStackTrace(); // For debugging only.
      }

      return false;
    }
  }

  /**
   * Updates the tuple from the <code>employee</code> database relation with the
   * matching <code>employee_id</code> attribute.
   * <p>
   * If update is successful, returns <code>true</code>.
   * <p>
   * If update fails, returns <code>false</code> and assigns a user-friendly
   * error message to <code>error</code>.
   *
   * @param employee an <code>Employee</code> object representing the tuple to
   *                 update
   * @return         <code>true</code> if update succeeded; <code>false</code>
   *                 otherwise
   */
  public static boolean updateEmployee(Employee employee)
  {
    try {
      // Ready the SQL statement.
      String sql = String.format("UPDATE employee\n"
                               + "   SET last_name = '%s', first_name = '%s',"
                               + "       address = '%s', city = '%s',"
                               + "       state = '%s', zip_code = '%s',"
                               + "       telephone = '%s', wage = %.2f,"
                               + "       manager_id = %d\n"
                               + " WHERE employee_id = %d",
                                 employee.getLastName(),
                                 employee.getFirstName(),
                                 employee.getAddress(),
                                 employee.getCity(),
                                 employee.getState(),
                                 employee.getZipCode(),
                                 employee.getTelephone(),
                                 employee.getWage(),
                                 employee.getManagerID(),
                                 employee.getID());

      // Attempt an update to the employee relation if the database connection
      // is active.
      Statement statement = DBConnector.createStatement(false);
      int       nRows     = 0;
      if (statement != null) // Database connection is active.
        nRows = statement.executeUpdate(sql);
      else { // Database connection is not active.
        error = DBConnector.error;

        return false;
      }

      boolean isUpdated = true;

      // Check for JDBC warnings.
      SQLWarning w = statement.getWarnings();
      if (w != null) {
        if (w.getSQLState().equals("02000")) {
          error = "Employee #"
                + employee.getID()
                + " was not found in our records.";

          isUpdated = false;
        }
      }

      // No JDBC errors occurred. Clean up.
      statement.close();

      return isUpdated;
    }
    catch (SQLException e) { // A JDBC error occurred.
      if (e.getSQLState().equals("42Z23"))
        error = "Employee IDs cannot be modified.";
      else {
        error = "He's dead, Jim. Please report this error.";
        e.printStackTrace(); // For debugging only.
      }

      return false;
    }
  }
}
