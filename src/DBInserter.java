////////////////////////////////////////////////////////////////////////////////
// Bryan Burke, Joshua Lambert, and Elizabeth Rothermel                       //
// Group 4                                                                    //
// INFX 499                                                                   //
// Fall 2015                                                                  //
// University of Louisiana at Lafayette                                       //
////////////////////////////////////////////////////////////////////////////////

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 * Provides methods to insert new tuples into certain database relations.
 *
 * @author Bryan Burke
 */
public final class DBInserter
{
  //////////////////////////////////////////////////////////////////////////////
  // CLASS VARIABLES                                                          //
  //////////////////////////////////////////////////////////////////////////////

  /**
   * Error message describing the most recent database access error.
   */
  public static String error = null;



  //////////////////////////////////////////////////////////////////////////////
  // CONSTRUCTORS                                                             //
  //////////////////////////////////////////////////////////////////////////////

  // This class is non-constructable.
  private DBInserter() {}



  //////////////////////////////////////////////////////////////////////////////
  // METHODS                                                                  //
  //////////////////////////////////////////////////////////////////////////////

  /**
   * Inserts a tuple describing the specified customer into the
   * <code>customer</code> relation.
   * <p>
   * Returns the auto-generated identifier of the newly inserted customer.
   * <p>
   * If insertion fails, returns <code>0</code> and assigns a user-friendly
   * error message to <code>error</code>.
   *
   * @param customer a <code>Customer</code> object representing the tuple to
   *                 insert
   * @return         the auto-generated identifier if tuple is inserted;
   *                 <code>0</code> otherwise
   */
  public static int insertCustomer(Customer customer)
  {
    try {
      // Ready the SQL statement.
      String sql = String.format("INSERT INTO customer (last_name, first_name,"
                               + "                      address, city, state,"
                               + "                      zip_code, telephone)\n"
                               + "VALUES ('%s', '%s', '%s', '%s', '%s', '%s',"
                               + "        '%s')",
                                 customer.getLastName(),
                                 customer.getFirstName(),
                                 customer.getAddress(),
                                 customer.getCity(),
                                 customer.getState(),
                                 customer.getZipCode(),
                                 customer.getTelephone());

      // Attempt an insert into the customer relation if the database connection
      // is active.
      PreparedStatement statement = DBConnector.createPreparedStatement(sql);
      int               nRows     = 0;
      if (statement != null) // Database connection is active.
        nRows = statement.executeUpdate();
      else { // Database connection is not active.
        error = DBConnector.error;

        return 0;
      }

      // Retrieve the auto-generated customer identifier.
      ResultSet autoKeys   = statement.getGeneratedKeys();
      autoKeys.next();
      int       customerID = autoKeys.getInt(1);

      // No JDBC errors occurred. Clean up.
      statement.close();

      return customerID;
    }
    catch (SQLException e) { // A JDBC error occurred.
      if (e.getSQLState().equals("23505"))
        error = "Customer "
              + customer.getFirstName()
              + " "
              + customer.getLastName()
              + " with address\n  "
              + customer.getAddress()
              + ", "
              + customer.getZipCode()
              + "\nalready exists in our records.";
      else {
        error = "He's dead, Jim. Please report this error.";
        e.printStackTrace(); // For debugging only.
      }

      return 0;
    }
  }

  /**
   * Inserts a tuple describing the specified account into the
   * <code>account</code> relation.
   * <p>
   * Returns the auto-generated identifier of the newly inserted account.
   * <p>
   * If insertion fails, returns <code>0</code> and assigns a user-friendly
   * error message to <code>error</code>.
   *
   * @param account an <code>Account</code> object representing the tuple to
   *                insert
   * @return        the auto-generated identifier if tuple is inserted;
   *                <code>0</code> otherwise
   */
  public static int insertAccount(Account account)
  {
    try {
      // Ready the SQL statement.
      String sql = String.format("INSERT INTO account (customer_id, email,"
                               + "                     credit_card)\n"
                               + "VALUES (%d, '%s', '%s')",
                                 account.getCustomer().getID(),
                                 account.getEmail(),
                                 account.getCreditCard());

      // Attempt an insert into the account relation if the database connection
      // is active.
      PreparedStatement statement = DBConnector.createPreparedStatement(sql);
      int               nRows     = 0;
      if (statement != null) // Database connection is active.
        nRows = statement.executeUpdate();
      else { // Database connection is not active.
        error = DBConnector.error;

        return 0;
      }

      // Retrieve the auto-generated account identifier.
      ResultSet autoKeys  = statement.getGeneratedKeys();
      autoKeys.next();
      int       accountID = autoKeys.getInt(1);

      // No JDBC errors occurred. Clean up.
      statement.close();

      return accountID;
    }
    catch (SQLException e) { // A JDBC error occurred.
      if (e.getSQLState().equals("23505"))
        error = "Account with email address "
              + account.getEmail()
              + " already exists in our records.";
      else {
        error = "He's dead, Jim. Please report this error.";
        e.printStackTrace(); // For debugging only.
      }

      return 0;
    }
  }

  /**
   * Inserts a tuple describing the specified employee into the
   * <code>employee</code> relation.
   * <p>
   * Returns the auto-generated identifier of the newly inserted employee.
   * <p>
   * If insertion fails, returns <code>0</code> and assigns a user-friendly
   * error message to <code>error</code>.
   *
   * @param employee an <code>Employee</code> object representing the tuple to
   *                 insert
   * @return         the auto-generated identifier if tuple is inserted;
   *                 <code>0</code> otherwise
   */
  public static int insertEmployee(Employee employee)
  {
    try {
      String sql = null;

      // Check if specified employee is a manager.
      if (employee.getManagerID() == 0) { // Employee is a manager.
        // Ready the SQL statement.
        sql = String.format("INSERT INTO employee (last_name,"
                          + "                      first_name, address,"
                          + "                      city, state,"
                          + "                      zip_code, telephone,"
                          + "                      ssn, wage)\n"
                          + "VALUES ('%s', '%s', '%s', '%s', '%s', '%s',"
                          + "        '%s', '%s', %.2f)",
                            employee.getLastName(),
                            employee.getFirstName(),
                            employee.getAddress(),
                            employee.getCity(),
                            employee.getState(),
                            employee.getZipCode(),
                            employee.getTelephone(),
                            employee.getSSN(),
                            employee.getWage());
      }
      else { // Employee is not a manager.
        // Ready the SQL statement.
        sql = String.format("INSERT INTO employee (last_name,"
                          + "                      first_name, address,"
                          + "                      city, state,"
                          + "                      zip_code, telephone,"
                          + "                      ssn, wage,"
                          + "                      manager_id)\n"
                          + "VALUES ('%s', '%s', '%s', '%s', '%s', '%s',"
                          + "        '%s', '%s', %.2f, %d)",
                            employee.getLastName(),
                            employee.getFirstName(),
                            employee.getAddress(),
                            employee.getCity(),
                            employee.getState(),
                            employee.getZipCode(),
                            employee.getTelephone(),
                            employee.getSSN(),
                            employee.getWage(),
                            employee.getManagerID());
      }

      // Attempt an insert into the employee relation if the database connection
      // is active.
      PreparedStatement statement = DBConnector.createPreparedStatement(sql);
      int               nRows     = 0;
      if (statement != null) // Database connection is active.
        nRows = statement.executeUpdate();
      else { // Database connection is not active.
        error = DBConnector.error;

        return 0;
      }

      // Retrieve the auto-generated employee identifier.
      ResultSet autoKeys   = statement.getGeneratedKeys();
      autoKeys.next();
      int       employeeID = autoKeys.getInt(1);

      // No JDBC errors occurred. Clean up.
      statement.close();

      return employeeID;
    }
    catch (SQLException e) { // A JDBC error occurred.
      if (e.getSQLState().equals("23505"))
        error = "Employee with SSN "
              + employee.getSSN()
              + " already exists in our records.";
      else {
        error = "He's dead, Jim. Please report this error.";
        e.printStackTrace(); // For debugging only.
      }

      return 0;
    }
  }

  /**
   * Inserts into the <code>reservation</code> and <code>booked</code> relations
   * tuples that describe the reservation to which the specified bookings
   * belong.
   * <p>
   * Returns the auto-generated identifier of the newly inserted reservation.
   * <p>
   * If insertion fails, returns <code>0</code> and assigns a user-friendly
   * error message to <code>error</code>.
   *
   * @param bookings an <code>ArrayList</code> of <code>Booked</code> objects
   *                 representing the tuples to insert
   * @return         the auto-generated identifier if tuples are inserted;
   *                 <code>0</code> otherwise
   */
  public static int insertReservation(ArrayList<Booked> bookings)
  {
    try {
      // All elements in bookings should belong to the same reservation.
      Reservation reservation = bookings.get(0).getReservation();

      // Ready the SQL statement.
      String sql = String.format("INSERT INTO reservation (flight_type,"
                               + "                         flight_class, price,"
                               + "                         employee_id)\n"
                               + "VALUES (%d, %d, %.2f, %d)",
                                 reservation.getFlightType(),
                                 reservation.getFlightClass(),
                                 reservation.getPrice(),
                                 reservation.getEmployee().getID());

      // Disable auto-commit.
      DBConnector.enableTransactions();

      // Attempt to insert into the reservation relation if the database
      // connection is active.
      PreparedStatement statement = DBConnector.createPreparedStatement(sql);
      int               nRows     = 0;
      if (statement != null) // Database connection is active.
        nRows = statement.executeUpdate();
      else { // Database connection is not active.
        error = DBConnector.error;

        return 0;
      }

      // Retrieve the auto-generated reservation identifier.
      ResultSet autoKeys      = statement.getGeneratedKeys();
      autoKeys.next();
      int       reservationID = autoKeys.getInt(1);

      // Attempt to insert the associated bookings.
      for (Booked booking : bookings)
        insertBooked(reservationID, booking);

      // No JDBC errors occurred. Commit and clean up.
      DBConnector.commitTransaction();
      statement.close();

      // Re-enable auto-commit.
      DBConnector.disableTransactions();

      return reservationID;
    }
    catch (SQLException e) { // A JDBC error occurred. Rollback.
      DBConnector.rollbackTransaction();

      // Re-enable auto-commit.
      DBConnector.disableTransactions();

      if (e.getMessage().equals("NO CONNECTION"))
        error = DBConnector.error;
      else {
        error = "He's dead, Jim. Please report this error.";
        e.printStackTrace(); // For debugging only.
      }

      return 0;
    }
  }

  /**
   * Inserts a tuple describing the specified reverse auction bid into the
   * <code>bid</code> relation.
   * <p>
   * If insertion is successful, returns <code>true</code>.
   * <p>
   * If insertion fails, returns <code>false</code> and assigns a user-friendly
   * error message to <code>error</code>.
   *
   * @param bid a <code>Bid</code> object representing the tuple to insert
   * @return    <code>true</code> if tuple is inserted; <code>false</code>
   *            otherwise
   */
  public static boolean insertBid(Bid bid)
  {
    try {
      // Ready the SQL statement.
      String sql = String.format("INSERT INTO bid (reservation_id, account_id,"
                               + "                 bid_amount)\n"
                               + "VALUES (%d, %d, %.2f)",
                                 bid.getReservation().getID(),
                                 bid.getAccount().getID(),
                                 bid.getAmount());

      // Attempt an insert into the bid relation if the database connection is
      // active.
      Statement statement = DBConnector.createStatement(false);
      int       nRows     = 0;
      if (statement != null) // Database connection is active.
        nRows = statement.executeUpdate(sql);
      else { // Database connection is not active.
        error = DBConnector.error;

        return false;
      }

      // No JDBC errors occurred. Clean up.
      statement.close();

      return true;
    }
    catch (SQLException e) { // A JDBC error occurred.
      if (e.getSQLState().equals("23505"))
        error = String.format("A $%.2f bid for reservation #%d by account #%d "
                            + "already exists in our records.",
                              bid.getAmount(),
                              bid.getReservation().getID(),
                              bid.getAccount().getID());
      else {
        error = "He's dead, Jim. Please report this error.";
        e.printStackTrace(); // For debugging only.
      }

      return false;
    }
  }



  /**
   * Inserts a tuple describing the specified booking into the
   * <code>booked</code> relation.
   *
   * @param  reservationID the newly inserted reservation's auto-generated
   *                       identifier
   * @param  booked        a <code>Booked</code> object representing the tuple
   *                       to insert
   * @throws SQLException  if insertion fails
   */
  private static void insertBooked(int reservationID,
                                   Booked booked) throws SQLException
  {
    String sql = null;

    // Check if specified booking includes a special meal.
    if (booked.getSpecialMeal() != DBEncoder.MEAL_NONE) { // Booking includes
      // Ready the SQL statement.                         // a special meal.
      sql = String.format("INSERT INTO booked (reservation_id, account_id,"
                        + "                    flight_id, airline_id,"
                        + "                    flight_order, departure_date,"
                        + "                    seat_number, special_meal)\n"
                        + "VALUES (%d, %d, '%s', '%s', %d, '%s', %d, %d)",
                          reservationID,
                          booked.getAccount().getID(),
                          booked.getFlight().getID(),
                          booked.getFlight().getAirline().getID(),
                          booked.getFlightOrder(),
                          booked.getDepartureDate(),
                          booked.getSeatNumber(),
                          booked.getSpecialMeal());
    }
    else { // Booking does not include a special meal.
      // Ready the SQL statement.
      sql = String.format("INSERT INTO booked (reservation_id, account_id,"
                        + "                    flight_id, airline_id,"
                        + "                    flight_order, departure_date,"
                        + "                    seat_number)\n"
                        + "VALUES (%d, %d, '%s', '%s', %d, '%s', %d)",
                          reservationID,
                          booked.getAccount().getID(),
                          booked.getFlight().getID(),
                          booked.getFlight().getAirline().getID(),
                          booked.getFlightOrder(),
                          booked.getDepartureDate(),
                          booked.getSeatNumber());
    }

    // Attempt an insert into the booked relation if the database connection is
    // active.
    Statement statement = DBConnector.createStatement(false);
    int       nRows     = 0;
    if (statement != null) // Database connection is active.
      nRows = statement.executeUpdate(sql);
    else // Database connection is not active.
      throw new SQLException("NO CONNECTION");

    // No JDBC errors occurred. Clean up.
    statement.close();
  }
}
