////////////////////////////////////////////////////////////////////////////////
// Bryan Burke, Joshua Lambert, and Elizabeth Rothermel                       //
// Group 4                                                                    //
// INFX 499                                                                   //
// Fall 2015                                                                  //
// University of Louisiana at Lafayette                                       //
////////////////////////////////////////////////////////////////////////////////

import java.io.IOException;
import java.sql.Date;
import java.sql.ResultSet;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

/**
 * Performs rudimentary unit testing on <code>DBQuerent</code> methods.
 *
 * @author Bryan Burke
 */
public class TestDBQuerent
{
  //////////////////////////////////////////////////////////////////////////////
  // METHODS                                                                  //
  //////////////////////////////////////////////////////////////////////////////

  /**
   * Calls <code>DBQuerent</code> methods to query the database and print the
   * parsed results.
   *
   * @param args command-line arguments; unused
   */
  public static void main(String[] args) throws IOException, ParseException
  {
    // Attempt to connect to the database.
    if (DBConnector.connect()) { // Connection succeeded.
      //////////////////////////////////////////////////////////////////////////
      // TEST METHODS                                                         //
      //////////////////////////////////////////////////////////////////////////

      System.out.println("== BEGIN METHOD TESTS ==");

      SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

      // queryFlights()
      System.out.println("= queryFlights() =");
      ArrayList<Flight> allFlights = DBQuerent.queryFlights();
      if (!allFlights.isEmpty())
        for (Flight flight : allFlights)
          System.out.println(flight);
      else
        System.out.println(DBQuerent.error);
      System.out.print("Verify the results and press ENTER to continue.");
      System.in.read();

      // queryFlights(Airport, Airport, Date)
      System.out.println("= queryFlights(lft, hnl, \"2015-12-20\") =");
      Airport lft           = DBMapper.getAirport("LFT");
      Airport hnl           = DBMapper.getAirport("HNL");
      Date    departureDate =
                             new Date(dateFormat.parse("2015-12-20").getTime());

      ArrayList<Flight> flights = DBQuerent.queryFlights(lft,
                                                         hnl,
                                                         departureDate);
      if (!flights.isEmpty())
        for (Flight flight : flights)
          System.out.println(flight);
      else
        System.out.println(DBQuerent.error);
      System.out.print("Verify the results and press ENTER to continue.");
      System.in.read();

      // queryDirectFlights(Airport, Airport, Date)
      System.out.println("= queryDirectFlights(atl, ord, \"2015-12-20\") =");
      Airport atl = DBMapper.getAirport("ATL");
      Airport ord = DBMapper.getAirport("ORD");

      // Does not include UA 241 because 2015-12-20 is a Sunday.
      ArrayList<Flight> directFlights = DBQuerent.queryFlights(atl,
                                                               ord,
                                                               departureDate);
      if (!directFlights.isEmpty())
        for (Flight flight : directFlights)
          System.out.println(flight);
      else
        System.out.println(DBQuerent.error);
      System.out.print("Verify the results and press ENTER to continue.");
      System.in.read();

      // queryFlights(Airport)
      System.out.println("= queryFlights(atl) =");
      ArrayList<Flight> atlFlights = DBQuerent.queryFlights(atl);
      if (!atlFlights.isEmpty())
        for (Flight flight : atlFlights)
          System.out.println(flight);
      else
        System.out.println(DBQuerent.error);
      System.out.print("Verify the results and press ENTER to continue.");
      System.in.read();

      // queryFlights(Reservation)
      System.out.println("= queryFlights(r7) =");
      Reservation r7 = DBMapper.getReservation(7);

      ArrayList<Flight> r7Flights = DBQuerent.queryFlights(r7);
      if (!r7Flights.isEmpty())
        for (Flight flight : r7Flights)
          System.out.println(flight);
      else
        System.out.println(DBQuerent.error);
      System.out.print("Verify the results and press ENTER to continue.");
      System.in.read();

      // queryLegs(Flight)
      System.out.println("= queryLegs(zz7521) =");
      Airline zz     = DBMapper.getAirline("ZZ");
      Flight  zz7521 = DBMapper.getFlight("7521", zz);

      ArrayList<Flight> zz7521Legs = DBQuerent.queryLegs(zz7521);
      if (!zz7521Legs.isEmpty())
        for (Flight flight : zz7521Legs)
          System.out.println(flight);
      else
        System.out.println(DBQuerent.error);
      System.out.print("Verify the results and press ENTER to continue.");
      System.in.read();

      // queryItinerary(Reservation)
      System.out.println("= queryItinerary(r7) =");
      ArrayList<Booked> r7Itinerary = DBQuerent.queryItinerary(r7);
      if (!r7Itinerary.isEmpty())
        for (Booked booking : r7Itinerary)
          System.out.println(booking);
      else
        System.out.println(DBQuerent.error);
      System.out.print("Verify the results and press ENTER to continue.");
      System.in.read();

      // queryMostActiveFlights()
      System.out.println("= queryMostActiveFlights() =");
      ArrayList<Flight> mostActiveFlights = DBQuerent.queryMostActiveFlights();
      if (!mostActiveFlights.isEmpty())
        for (Flight flight : mostActiveFlights)
          System.out.println(flight);
      else
        System.out.println(DBQuerent.error);
      System.out.print("Verify the results and press ENTER to continue.");
      System.in.read();

      // querySuggestedFlights(Customer)
      System.out.println("= querySuggestedFlights(c5) =");
      Customer c5 = DBMapper.getCustomer(5);

      ArrayList<Flight> c5SuggestedFlights =
                                            DBQuerent.querySuggestedFlights(c5);
      if (!c5SuggestedFlights.isEmpty())
        for (Flight flight : c5SuggestedFlights)
          System.out.println(flight);
      else
        System.out.println(DBQuerent.error);
      System.out.print("Verify the results and press ENTER to continue.");
      System.in.read();

      // queryCustomers(Flight)
      System.out.println("= queryCustomers(ha9) =");
      Airline ha  = DBMapper.getAirline("HA");
      Flight  ha9 = DBMapper.getFlight("9", ha);

      ArrayList<Customer> ha9Customers = DBQuerent.queryCustomers(ha9);
      if (!ha9Customers.isEmpty())
        for (Customer customer : ha9Customers)
          System.out.println(customer);
      else
        System.out.println(DBQuerent.error);
      System.out.print("Verify the results and press ENTER to continue.");
      System.in.read();

      // queryReservations(Flight)
      System.out.println("= queryReservations(ha9) =");
      ArrayList<Reservation> ha9Reservations = DBQuerent.queryReservations(ha9);
      if (!ha9Reservations.isEmpty())
        for (Reservation reservation : ha9Reservations)
          System.out.println(reservation);
      else
        System.out.println(DBQuerent.error);
      System.out.print("Verify the results and press ENTER to continue.");
      System.in.read();

      // queryReservations(Customer)
      System.out.println("= queryReservations(c5) =");
      ArrayList<Reservation> c5Reservations = DBQuerent.queryReservations(c5);
      if (!c5Reservations.isEmpty())
        for (Reservation reservation : c5Reservations)
          System.out.println(reservation);
      else
        System.out.println(DBQuerent.error);
      System.out.print("Verify the results and press ENTER to continue.");
      System.in.read();

      // queryActiveReservations(Customer)
      System.out.println("= queryActiveReservations(c1) =");
      Customer c1 = DBMapper.getCustomer(1);

      ArrayList<Reservation> c1ActiveReservations =
                                          DBQuerent.queryActiveReservations(c1);
      if (!c1ActiveReservations.isEmpty())
        for (Reservation reservation : c1ActiveReservations)
          System.out.println(reservation);
      else
        System.out.println(DBQuerent.error);
      System.out.print("Verify the results and press ENTER to continue.");
      System.in.read();

      // queryBids(Reservation, Account)
      System.out.println("= queryBids(r3, a5) =");
      Reservation r3 = DBMapper.getReservation(3);
      Account     a5 = DBMapper.getAccount(5);

      ArrayList<Bid> r3a5Bids = DBQuerent.queryBids(r3,
                                                    a5);
      if (!r3a5Bids.isEmpty())
        for (Bid bid : r3a5Bids)
          System.out.println(bid);
      else
        System.out.println(DBQuerent.error);
      System.out.print("Verify the results and press ENTER to continue.");
      System.in.read();

      // queryCurrentBid(Reservation, Account)
      System.out.println("= queryCurrentBid(r3, a5) =");
      Bid r3a5CurrentBid = DBQuerent.queryCurrentBid(r3,
                                                     a5);
      if (r3a5CurrentBid != null)
        System.out.println(r3a5CurrentBid);
      else
        System.out.println(DBQuerent.error);
      System.out.print("Verify the results and press ENTER to continue.");
      System.in.read();

      // queryRevenue(int, int)
      System.out.println("= queryRevenue(12, 2015) =");
      double dec2015Revenue = DBQuerent.queryRevenue(12,
                                                     2015);
      if (dec2015Revenue != 0.0)
        System.out.printf("%.2f\n", dec2015Revenue);
      else
        System.out.println(DBQuerent.error);
      System.out.print("Verify the results and press ENTER to continue.");
      System.in.read();

      // queryRevenue(Flight)
      System.out.println("= queryRevenue(ha9) =");
      double ha9Revenue = DBQuerent.queryRevenue(ha9);
      if (ha9Revenue != 0.0)
        System.out.printf("%.2f\n", ha9Revenue);
      else
        System.out.println(DBQuerent.error);
      System.out.print("Verify the results and press ENTER to continue.");
      System.in.read();

      // queryRevenue(String, String, String)
      System.out.println("= queryRevenue(\"Atlanta\", \"GA\", \"US\") =");
      double atlantaGaUsRevenue = DBQuerent.queryRevenue("Atlanta",
                                                         "GA",
                                                         "US");
      if (atlantaGaUsRevenue != 0.0)
        System.out.printf("%.2f\n", atlantaGaUsRevenue);
      else
        System.out.println(DBQuerent.error);
      System.out.print("Verify the results and press ENTER to continue.");
      System.in.read();

      // queryRevenue(Customer)
      System.out.println("= queryRevenue(c5) =");
      double c5Revenue = DBQuerent.queryRevenue(c5);
      if (c5Revenue != 0.0)
        System.out.printf("%.2f\n", c5Revenue);
      else
        System.out.println(DBQuerent.error);
      System.out.print("Verify the results and press ENTER to continue.");
      System.in.read();

      // queryCustomersWithMostRevenue()
      System.out.println("= queryCustomersWithMostRevenue() =");
      ArrayList<Customer> customersWithMostRevenue =
                                      DBQuerent.queryCustomersWithMostRevenue();
      if (!customersWithMostRevenue.isEmpty())
        for (Customer customer : customersWithMostRevenue)
          System.out.println(customer);
      else
        System.out.println(DBQuerent.error);
      System.out.print("Verify the results and press ENTER to continue.");
      System.in.read();

      // queryEmployeesWithMostRevenue()
      System.out.println("= queryEmployeesWithMostRevenue() =");
      ArrayList<Employee> employeesWithMostRevenue =
                                      DBQuerent.queryEmployeesWithMostRevenue();
      if (!employeesWithMostRevenue.isEmpty())
        for (Employee employee : employeesWithMostRevenue)
          System.out.println(employee);
      else
        System.out.println(DBQuerent.error);
      System.out.print("Verify the results and press ENTER to continue.");
      System.in.read();

      // queryMailingList()
      System.out.println("= queryMailingList() =");
      ArrayList<String> mailingList = DBQuerent.queryMailingList();
      if (!mailingList.isEmpty())
        for (String address : mailingList)
          System.out.println(address);
      else
        System.out.println(DBQuerent.error);
      System.out.print("Verify the results and press ENTER to continue.");
      System.in.read();

      System.out.println("== END METHOD TESTS ==");

      // Close the database connection and clean up.
      DBConnector.disconnect();
    }
    else { // Connection did not succeed.
      System.out.println(DBConnector.error);

      System.exit(1);
    }
  }
}
