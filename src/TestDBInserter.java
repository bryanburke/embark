////////////////////////////////////////////////////////////////////////////////
// Bryan Burke, Joshua Lambert, and Elizabeth Rothermel                       //
// Group 4                                                                    //
// INFX 499                                                                   //
// Fall 2015                                                                  //
// University of Louisiana at Lafayette                                       //
////////////////////////////////////////////////////////////////////////////////

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

/**
 * Performs rudimentary unit testing on <code>DBInserter</code> methods. Should
 * only be run once on a fresh copy of the sample database.
 *
 * @author Bryan Burke
 */
public class TestDBInserter
{
  //////////////////////////////////////////////////////////////////////////////
  // METHODS                                                                  //
  //////////////////////////////////////////////////////////////////////////////

  /**
   * Calls <code>DBInserter</code> methods to insert data into the database.
   *
   * @param args command-line arguments; unused
   */
  public static void main(String[] args) throws ParseException
  {
    // Attempt to connect to the database.
    if (DBConnector.connect()) { // Connection succeeded.
      //////////////////////////////////////////////////////////////////////////
      // TEST METHODS                                                         //
      //////////////////////////////////////////////////////////////////////////

      System.out.println("== BEGIN METHOD TESTS ==");

      SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

      // insertCustomer(Customer)
      System.out.println("= insertCustomer(c8) =");
      Customer c8 = new Customer("Doe",
                                 "John",
                                 "100 Ghoulish Boulevard",
                                 "Nowhere",
                                 "QQ",
                                 "55555",
                                 "5555555555");
      int newCustomerID = DBInserter.insertCustomer(c8);
      if (newCustomerID != 0)
        System.out.printf("New customer inserted with auto-generated ID #%d.\n",
                          newCustomerID);
      else
        System.out.println(DBInserter.error);

      // insertAccount(Account)
      System.out.println("= insertAccount(a9) =");
      Customer doeJ = DBMapper.getCustomer(newCustomerID);
      Account  a9   = new Account(doeJ,
                                  "doe.john@nowhere.com",
                                  "XXXXXXXXXXXXXXXX");
      int newAccountID = DBInserter.insertAccount(a9);
      if (newAccountID != 0)
        System.out.printf("New account inserted with auto-generated ID #%d.\n",
                          newAccountID);
      else
        System.out.println(DBInserter.error);

      // insertEmployee(Employee)
      System.out.println("= insertEmployee(e5) =");
      Employee e5 = new Employee("Doe",
                                 "Jim",
                                 "100 Ghoulish Boulevard",
                                 "Nowhere",
                                 "QQ",
                                 "55555",
                                 "5555555555",
                                 "555555555",
                                 27.0);
      int newManagerID = DBInserter.insertEmployee(e5);
      if (newManagerID != 0)
        System.out.printf("New manager inserted with auto-generated ID #%d.\n",
                          newManagerID);
      else
        System.out.println(DBInserter.error);

      System.out.println("= insertEmployee(e6) =");
      Employee e6 = new Employee("Doe",
                                 "Jason",
                                 "100 Ghoulish Boulevard",
                                 "Nowhere",
                                 "QQ",
                                 "55555",
                                 "5555555555",
                                 "999999999",
                                 9.0,
                                 1);
      int newEmployeeID = DBInserter.insertEmployee(e6);
      if (newEmployeeID != 0)
        System.out.printf("New employee inserted with auto-generated ID #%d.\n",
                          newEmployeeID);
      else
        System.out.println(DBInserter.error);

      // insertReservation(ArrayList<Booked>)
      System.out.println("= insertReservation(r8Bookings) =");

      Employee          customerRep   = DBMapper.getEmployee(newEmployeeID);
      Reservation       r8            =
                                       new Reservation(DBEncoder.FLIGHT_ONE_WAY,
                                                       DBEncoder.CLASS_ECONOMY,
                                                       338.8,
                                                       customerRep);
      Account           newAccount    = DBMapper.getAccount(newAccountID);
      Airline           dl            = DBMapper.getAirline("DL");
      Flight            dl5071        = DBMapper.getFlight("5071", dl);
      Date              departureDate =
                             new Date(dateFormat.parse("2016-01-01").getTime());
      ArrayList<Booked> r8Bookings    = new ArrayList<>();

      r8Bookings.add(new Booked(r8,
                                newAccount,
                                dl5071,
                                1,
                                departureDate,
                                111));

      int newReservationID = DBInserter.insertReservation(r8Bookings);
      if (newReservationID != 0)
        System.out.printf("New reservation with associated bookings inserted "
                        + "with auto-generated ID #%d.\n",
                          newReservationID);
      else
        System.out.println(DBInserter.error);

      // insertBid(Bid)
      System.out.println("= insertBid(r8a9Bid) =");
      Reservation newReservation = DBMapper.getReservation(newReservationID);
      Bid         r8a9Bid        = new Bid(newReservation,
                                           newAccount,
                                           330.0);
      if (DBInserter.insertBid(r8a9Bid))
        System.out.printf("New bid by account #%d for reservation #%d "
                        + "inserted.\n",
                          newAccountID,
                          newReservationID);
      else
        System.out.println(DBInserter.error);

      System.out.println("== END METHOD TESTS ==");

      // Close the database connection and clean up.
      DBConnector.disconnect();
    }
    else { // Connection did not succeed.
      System.out.println(DBConnector.error);

      System.exit(1);
    }
  }
}
