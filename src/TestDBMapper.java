////////////////////////////////////////////////////////////////////////////////
// Bryan Burke, Joshua Lambert, and Elizabeth Rothermel                       //
// Group 4                                                                    //
// INFX 499                                                                   //
// Fall 2015                                                                  //
// University of Louisiana at Lafayette                                       //
////////////////////////////////////////////////////////////////////////////////

import java.io.IOException;

/**
 * Performs rudimentary unit testing on <code>DBMapper</code> methods.
 *
 * @author Bryan Burke
 */
public class TestDBMapper
{
  //////////////////////////////////////////////////////////////////////////////
  // METHODS                                                                  //
  //////////////////////////////////////////////////////////////////////////////

  /**
   * Calls <code>DBMapper</code> methods to create database entity objects.
   *
   * @param args command-line arguments; unused
   */
  public static void main(String[] args) throws IOException
  {
    // Attempt to connect to the database.
    if (DBConnector.connect()) { // Connection succeeded.
      //////////////////////////////////////////////////////////////////////////
      // TEST METHODS                                                         //
      //////////////////////////////////////////////////////////////////////////

      System.out.println("== BEGIN METHOD TESTS ==");

      // getAirline(String)
      System.out.println("= getAirline(\"AA\") =");
      Airline aa = DBMapper.getAirline("AA");
      if (aa != null)
        System.out.println(aa);
      else
        System.out.println(DBMapper.error);
      System.out.print("Verify the results and press ENTER to continue.");
      System.in.read();

      // getAirport(String)
      System.out.println("= getAirport(\"LFT\") =");
      Airport lft = DBMapper.getAirport("LFT");
      if (lft != null)
        System.out.println(lft);
      else
        System.out.println(DBMapper.error);
      System.out.print("Verify the results and press ENTER to continue.");
      System.in.read();

      // getFlight(String, Airline)
      System.out.println("= getFlight(\"3108\", aa) =");
      Flight aa3108 = DBMapper.getFlight("3108",
                                         aa);
      if (aa3108 != null)
        System.out.println(aa3108);
      else
        System.out.println(DBMapper.error);
      System.out.print("Verify the results and press ENTER to continue.");
      System.in.read();

      // getCustomer(int)
      System.out.println("= getCustomer(5) =");
      Customer patelK = DBMapper.getCustomer(5);
      if (patelK != null)
        System.out.println(patelK);
      else
        System.out.println(DBMapper.error);
      System.out.print("Verify the results and press ENTER to continue.");
      System.in.read();

      // getCustomer(String, String, String, String)
      System.out.println("= getCustomer(\"Regina\", \"Dwyer\", "
                       + "\"480 Poling Farm Road\", \"68048\") =");
      Customer dwyerR = DBMapper.getCustomer("Regina",
                                             "Dwyer",
                                             "480 Poling Farm Road",
                                             "68048");
      if (dwyerR != null)
        System.out.println(dwyerR);
      else
        System.out.println(DBMapper.error);
      System.out.print("Verify the results and press ENTER to continue.");
      System.in.read();

      // getAccount(int)
      System.out.println("= getAccount(5) =");
      Account a5 = DBMapper.getAccount(5);
      if (a5 != null)
        System.out.println(a5);
      else
        System.out.println(DBMapper.error);
      System.out.print("Verify the results and press ENTER to continue.");
      System.in.read();

      // getAccount(String)
      System.out.println("= getAccount(\"patel.kimberly@talkscope.com\") =");
      Account a6 = DBMapper.getAccount("patel.kimberly@talkscope.com");
      if (a6 != null)
        System.out.println(a6);
      else
        System.out.println(DBMapper.error);
      System.out.print("Verify the results and press ENTER to continue.");
      System.in.read();

      // getEmployee(int)
      System.out.println("= getEmployee(2) =");
      Employee totaroM = DBMapper.getEmployee(2);
      if (totaroM != null)
        System.out.println(totaroM);
      else
        System.out.println(DBMapper.error);
      System.out.print("Verify the results and press ENTER to continue.");
      System.in.read();

      // getEmployee(String)
      System.out.println("= getEmployee(\"26560XXXX\") =");
      Employee sigdelP = DBMapper.getEmployee("26560XXXX");
      if (sigdelP != null)
        System.out.println(sigdelP);
      else
        System.out.println(DBMapper.error);
      System.out.print("Verify the results and press ENTER to continue.");
      System.in.read();

      // getReservation(int)
      System.out.println("= getReservation(3) =");
      Reservation r3 = DBMapper.getReservation(3);
      if (r3 != null)
        System.out.println(r3);
      else
        System.out.println(DBMapper.error);
      System.out.print("Verify the results and press ENTER to continue.");
      System.in.read();

      // getBid(Reservation, Account, double)
      System.out.println("= getBid(r3, a5, 96.0) =");
      Bid r3a5$96_00 = DBMapper.getBid(r3,
                                       a5,
                                       96.0);
      if (r3a5$96_00 != null)
        System.out.println(r3a5$96_00);
      else
        System.out.println(DBMapper.error);
      System.out.print("Verify the results and press ENTER to continue.");
      System.in.read();

      // getBooked(Reservation, Account, Flight, int)
      System.out.println("= getBooked(r3, a6, aa3108, 1) =");
      Booked r3a6aa3108f1 = DBMapper.getBooked(r3,
                                               a6,
                                               aa3108,
                                               1);
      if (r3a6aa3108f1 != null)
        System.out.println(r3a6aa3108f1);
      else
        System.out.println(DBMapper.error);
      System.out.print("Verify the results and press ENTER to continue.");
      System.in.read();

      System.out.println("== END METHOD TESTS ==");

      // Close the database connection and clean up.
      DBConnector.disconnect();
    }
    else { // Connection did not succeed.
      System.out.println(DBConnector.error);

      System.exit(1);
    }
  }
}
