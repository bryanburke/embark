////////////////////////////////////////////////////////////////////////////////
// Bryan Burke, Joshua Lambert, and Elizabeth Rothermel                       //
// Group 4                                                                    //
// INFX 499                                                                   //
// Fall 2015                                                                  //
// University of Louisiana at Lafayette                                       //
////////////////////////////////////////////////////////////////////////////////

import java.io.IOException;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

/**
 * Performs rudimentary unit testing on <code>Employee</code> objects.
 *
 * @author Bryan Burke
 */
public class TestEmployee
{
  //////////////////////////////////////////////////////////////////////////////
  // METHODS                                                                  //
  //////////////////////////////////////////////////////////////////////////////

  /**
   * Creates and tests <code>Employee</code> objects.
   *
   * @param args command-line arguments; unused
   */
  public static void main(String[] args) throws IOException, ParseException
  {
    ////////////////////////////////////////////////////////////////////////////
    // TEST CONSTRUCTORS                                                      //
    ////////////////////////////////////////////////////////////////////////////

    System.out.println("== BEGIN CONSTRUCTOR TESTS ==");

    ArrayList<Employee> employees  = new ArrayList<>(4);
    SimpleDateFormat    dateFormat = new SimpleDateFormat("yyyy-MM-dd");

    // Employee(int, String, String, String, String, String, String, String,
    //          String, Date, double, int)
    employees.add(new Employee(2,
                               "Sigdel",
                               "Purushottam",
                               "304 Kenwood Place",
                               "Lafayette",
                               "LA",
                               "70504",
                               "3375833335",
                               "26560XXXX",
                             new Date(dateFormat.parse("2008-04-19").getTime()),
                               20.0,
                               1));
    System.out.println("  Employee #2 constructed as employees[0].");

    // Employee(String, String, String, String, String, String, String, String,
    //          double, int)
    employees.add(new Employee("Thomason",
                               "Ruth",
                               "1842 Lowland Drive",
                               "Scott",
                               "LA",
                               "70583",
                               "3374093644",
                               "33904XXXX",
                               10.0,
                               1));
    System.out.println("  New employee constructed as employees[1].");

    // Employee(String, String, String, String, String, String, String, String,
    //          double)
    employees.add(new Employee("Totaro",
                               "Michael",
                               "15 Cody Ridge Road",
                               "Lafayette",
                               "LA",
                               "70506",
                               "3374938066",
                               "44419XXXX",
                               38.0));
    System.out.println("  New manager constructed as employees[2].");

    // Employee(Employee)
    employees.add(new Employee(employees.get(0)));
    System.out.println("  Employee #2 copy constructed as employees[3].");

    System.out.println("== END CONSTRUCTOR TESTS ==");
    System.out.println();



    ////////////////////////////////////////////////////////////////////////////
    // TEST METHODS                                                           //
    ////////////////////////////////////////////////////////////////////////////

    System.out.println("== BEGIN METHOD TESTS ==");

    // getID()
    System.out.println("= getID() =");
    for (int i = 0; i < employees.size(); ++i)
      System.out.printf("  [%d] : %d\n",
                        i,
                        employees.get(i).getID());
    System.out.print("Verify the results and press ENTER to continue.");
    System.in.read();

    // getLastName()
    System.out.println("= getLastName() =");
    for (int i = 0; i < employees.size(); ++i)
      System.out.printf("  [%d] : %s\n",
                        i,
                        employees.get(i).getLastName());
    System.out.print("Verify the results and press ENTER to continue.");
    System.in.read();

    // getFirstName()
    System.out.println("= getFirstName() =");
    for (int i = 0; i < employees.size(); ++i)
      System.out.printf("  [%d] : %s\n",
                        i,
                        employees.get(i).getFirstName());
    System.out.print("Verify the results and press ENTER to continue.");
    System.in.read();

    // getAddress()
    System.out.println("= getAddress() =");
    for (int i = 0; i < employees.size(); ++i)
      System.out.printf("  [%d] : %s\n",
                        i,
                        employees.get(i).getAddress());
    System.out.print("Verify the results and press ENTER to continue.");
    System.in.read();

    // getCity()
    System.out.println("= getCity() =");
    for (int i = 0; i < employees.size(); ++i)
      System.out.printf("  [%d] : %s\n",
                        i,
                        employees.get(i).getCity());
    System.out.print("Verify the results and press ENTER to continue.");
    System.in.read();

    // getState()
    System.out.println("= getState() =");
    for (int i = 0; i < employees.size(); ++i)
      System.out.printf("  [%d] : %s\n",
                        i,
                        employees.get(i).getState());
    System.out.print("Verify the results and press ENTER to continue.");
    System.in.read();

    // getZipCode()
    System.out.println("= getZipCode() =");
    for (int i = 0; i < employees.size(); ++i)
      System.out.printf("  [%d] : %s\n",
                        i,
                        employees.get(i).getZipCode());
    System.out.print("Verify the results and press ENTER to continue.");
    System.in.read();

    // getTelephone()
    System.out.println("= getTelephone() =");
    for (int i = 0; i < employees.size(); ++i)
      System.out.printf("  [%d] : %s\n",
                        i,
                        employees.get(i).getTelephone());
    System.out.print("Verify the results and press ENTER to continue.");
    System.in.read();

    // getSSN()
    System.out.println("= getSSN() =");
    for (int i = 0; i < employees.size(); ++i)
      System.out.printf("  [%d] : %s\n",
                        i,
                        employees.get(i).getSSN());
    System.out.print("Verify the results and press ENTER to continue.");
    System.in.read();

    // getHireDate()
    System.out.println("= getHireDate() =");
    for (int i = 0; i < employees.size(); ++i)
      System.out.printf("  [%d] : %s\n",
                        i,
                        employees.get(i).getHireDate());
    System.out.print("Verify the results and press ENTER to continue.");
    System.in.read();

    // getWage()
    System.out.println("= getWage() =");
    for (int i = 0; i < employees.size(); ++i)
      System.out.printf("  [%d] : %.2f\n",
                        i,
                        employees.get(i).getWage());
    System.out.print("Verify the results and press ENTER to continue.");
    System.in.read();

    // getManagerID()
    System.out.println("= getManagerID() =");
    for (int i = 0; i < employees.size(); ++i)
      System.out.printf("  [%d] : %d\n",
                        i,
                        employees.get(i).getManagerID());
    System.out.print("Verify the results and press ENTER to continue.");
    System.in.read();

    // equals(Employee)
    System.out.println("= equals([0]) =");
    for (int i = 0; i < employees.size(); ++i)
      System.out.printf("  [%d] : %s\n",
                        i,
                        employees.get(i).equals(employees.get(0)));
    System.out.print("Verify the results and press ENTER to continue.");
    System.in.read();

    // toString()
    System.out.println("= toString() =");
    for (int i = 0; i < employees.size(); ++i)
      System.out.printf("  [%d] : %s\n",
                        i,
                        employees.get(i).toString());
    System.out.print("Verify the results and press ENTER to continue.");
    System.in.read();

    // setLastName(String)
    System.out.println("= [1].setLastName(\"Judge\") =");
    employees.get(1).setLastName("Judge");
    // setFirstName(String)
    System.out.println("= [1].setFirstName(\"Hilda\") =");
    employees.get(1).setFirstName("Hilda");
    // setAddress(String)
    System.out.println("= [1].setAddress(\"1234 New York Avenue\") =");
    employees.get(1).setAddress("1234 New York Avenue");
    // setCity(String)
    System.out.println("= [1].setCity(\"Fort Worth\") =");
    employees.get(1).setCity("Fort Worth");
    // setState(String)
    System.out.println("= [1].setState(\"TX\") =");
    employees.get(1).setState("TX");
    // setZipCode(String)
    System.out.println("= [1].setZipCode(\"76110\") =");
    employees.get(1).setZipCode("76110");
    // setTelephone(String)
    System.out.println("= [1].setTelephone(\"8179217713\") =");
    employees.get(1).setTelephone("8179217713");
    // setWage(double)
    System.out.println("= [1].setWage(15.0) =");
    employees.get(1).setWage(15.0);
    // setManagerID(int)
    System.out.println("= [1].setManagerID(2) =");
    employees.get(1).setManagerID(2);

    System.out.printf("  [1] : %s\n",
                      employees.get(1).toString());
    System.out.print("Verify the results and press ENTER to continue.");
    System.in.read();

    System.out.println("== END METHOD TESTS ==");
  }
}
