////////////////////////////////////////////////////////////////////////////////
// Bryan Burke, Joshua Lambert, and Elizabeth Rothermel                       //
// Group 4                                                                    //
// INFX 499                                                                   //
// Fall 2015                                                                  //
// University of Louisiana at Lafayette                                       //
////////////////////////////////////////////////////////////////////////////////

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Provides methods to map tuples from certain database relations to Java
 * objects.
 *
 * @author Bryan Burke
 */
public final class DBMapper
{
  //////////////////////////////////////////////////////////////////////////////
  // CLASS VARIABLES                                                          //
  //////////////////////////////////////////////////////////////////////////////

  /**
   * Error message describing the most recent database access error.
   */
  public static String error = null;



  //////////////////////////////////////////////////////////////////////////////
  // CONSTRUCTORS                                                             //
  //////////////////////////////////////////////////////////////////////////////

  // This class is non-constructable.
  private DBMapper() {}



  //////////////////////////////////////////////////////////////////////////////
  // METHODS                                                                  //
  //////////////////////////////////////////////////////////////////////////////

  /**
   * Maps the tuple from the <code>airline</code> database relation with the
   * matching <code>airline_id</code> attribute.
   * <p>
   * If the tuple exists, returns an <code>Airline</code> object that maps it.
   * <p>
   * If the tuple does not exist, returns a <code>null</code> reference and
   * assigns a user-friendly error message to <code>error</code>.
   *
   * @param id the unique <code>airline</code> identifier describing the tuple
   *           to map
   * @return   a fully instantiated <code>Airline</code> object if the tuple
   *           exists; <code>null</code> otherwise
   */
  public static Airline getAirline(String id)
  {
    try {
      // Ready the SQL statement.
      String sql = String.format("SELECT name\n"
                               + "  FROM airline\n"
                               + " WHERE airline_id = '%s'",
                                 id);

      // Attempt a read from the airline relation if the database connection is
      // active.
      Statement statement = DBConnector.createStatement(false);
      ResultSet results   = null;
      if (statement != null) // Database connection is active.
        results = statement.executeQuery(sql);
      else { // Database connection is not active.
        error = DBConnector.error;

        return null;
      }

      Airline airline = null;

      // Check to see if a matching tuple was found.
      if (results.next()) // Tuple was found.
        airline = new Airline(id,                    // airline_id
                              results.getString(1)); // name
      else // Tuple was not found.
        error = "Airline "
              + id
              + " was not found in our records.";

      // No JDBC errors occurred. Clean up.
      statement.close();

      return airline;
    }
    catch (SQLException e) { // A JDBC error occurred.
      error = "He's dead, Jim. Please report this error.";
      e.printStackTrace(); // For debugging only.

      return null;
    }
  }

  /**
   * Maps the tuple from the <code>airport</code> database relation with the
   * matching <code>airport_id</code> attribute.
   * <p>
   * If the tuple exists, returns an <code>Airport</code> object that maps it.
   * <p>
   * If the tuple does not exist, returns a <code>null</code> reference and
   * assigns a user-friendly error message to <code>error</code>.
   *
   * @param id the unique <code>airport</code> identifier describing the tuple
   *           to map
   * @return   a fully instantiated <code>Airport</code> object if the tuple
   *           exists; <code>null</code> otherwise
   */
  public static Airport getAirport(String id)
  {
    try {
      // Ready the SQL statement.
      String sql = String.format("SELECT name, city, region, country\n"
                               + "  FROM airport\n"
                               + " WHERE airport_id = '%s'",
                                 id);

      // Attempt a read from the airport relation if the database connection is
      // active.
      Statement statement = DBConnector.createStatement(false);
      ResultSet results   = null;
      if (statement != null) // Database connection is active.
        results = statement.executeQuery(sql);
      else { // Database connection is not active.
        error = DBConnector.error;

        return null;
      }

      Airport airport = null;

      // Check to see if a matching tuple was found.
      if (results.next()) // Tuple was found.
        airport = new Airport(id,                    // airport_id
                              results.getString(1),  // name
                              results.getString(2),  // city
                              results.getString(3),  // region
                              results.getString(4)); // country
      else // Tuple was not found.
        error = "Airport "
              + id
              + " was not found in our records.";

      // No JDBC errors occurred. Clean up.
      statement.close();

      return airport;
    }
    catch (SQLException e) { // A JDBC error occurred.
      error = "He's dead, Jim. Please report this error.";
      e.printStackTrace(); // For debugging only.

      return null;
    }
  }

  /**
   * Maps the tuple from the <code>flight</code> database relation with the
   * matching <code>flight_id</code> and <code>airline_id</code> attributes.
   * <p>
   * If the tuple exists, returns a <code>Flight</code> object that maps it.
   * <p>
   * If the tuple does not exist, returns a <code>null</code> reference and
   * assigns a user-friendly error message to <code>error</code>.
   *
   * @param id      the <code>flight</code> identifier that partially describes
   *                the tuple to map
   * @param airline an <code>Airline</code> object containing the
   *                <code>airline</code> identifier that partially describes the
   *                tuple to map
   * @return        a fully instantiated <code>Flight</code> object if the tuple
   *                exists; <code>null</code> otherwise
   */
  public static Flight getFlight(String id,
                                 Airline airline)
  {
    try {
      // Ready the SQL statement.
      String sql = String.format("SELECT origin_airport, destination_airport,"
                               + "       fare, reserve_fare, departure_days,"
                               + "       departure_time, arrival_time,"
                               + "       capacity\n"
                               + "  FROM flight\n"
                               + " WHERE flight_id  = '%s'\n"
                               + "   AND airline_id = '%s'",
                                 id,
                                 airline.getID());

      // Attempt a read from the flight relation if the database connection is
      // active.
      Statement statement = DBConnector.createStatement(false);
      ResultSet results   = null;
      if (statement != null) // Database connection is active.
        results = statement.executeQuery(sql);
      else { // Database connection is not active.
        error = DBConnector.error;

        return null;
      }

      Flight flight = null;

      // Check to see if a matching tuple was found.
      if (results.next()) { // Tuple was found.
        Airport originAirport      = getAirport(results.getString(1));
        Airport destinationAirport = getAirport(results.getString(2));
        double  fare               = results.getBigDecimal(3).doubleValue();
        double  reserveFare        = results.getBigDecimal(4).doubleValue();

        flight = new Flight(id,                   // flight_id
                            airline,              // airline_id
                            originAirport,        // origin_airport
                            destinationAirport,   // destination_airport
                            results.getString(5), // departure_days
                            results.getTime(6),   // departure_time
                            results.getTime(7),   // arrival_time
                            results.getInt(8),    // capacity
                            fare,                 // fare
                            reserveFare);         // reserve_fare
      }
      else // Tuple was not found.
        error = "Flight "
              + airline.getID()
              + " "
              + id
              + " was not found in our records.";

      // No JDBC errors occurred. Clean up.
      statement.close();

      return flight;
    }
    catch (SQLException e) { // A JDBC error occurred.
      error = "He's dead, Jim. Please report this error.";
      e.printStackTrace(); // For debugging only.

      return null;
    }
  }

  /**
   * Maps the tuple from the <code>customer</code> database relation with the
   * matching <code>customer_id</code> attribute.
   * <p>
   * If the tuple exists, returns a <code>Customer</code> object that maps it.
   * <p>
   * If the tuple does not exist, returns a <code>null</code> reference and
   * assigns a user-friendly error message to <code>error</code>.
   *
   * @param id the <code>customer</code> identifier describing the tuple to map
   * @return   a fully instantiated <code>Customer</code> object if the tuple
   *           exists; <code>null</code> otherwise
   */
  public static Customer getCustomer(int id)
  {
    try {
      // Ready the SQL statement.
      String sql = String.format("SELECT last_name, first_name, address, city,"
                               + "       state, zip_code, telephone, rating\n"
                               + "  FROM customer\n"
                               + " WHERE customer_id = %d",
                                 id);

      // Attempt a read from the customer relation if the database connection is
      // active.
      Statement statement = DBConnector.createStatement(false);
      ResultSet results   = null;
      if (statement != null) // Database connection is active.
        results = statement.executeQuery(sql);
      else { // Database connection is not active.
        error = DBConnector.error;

        return null;
      }

      Customer customer = null;

      // Check to see if a matching tuple was found.
      if (results.next()) // Tuple was found.
        customer = new Customer(id,                   // customer_id
                                results.getString(1), // last_name
                                results.getString(2), // first_name
                                results.getString(3), // address
                                results.getString(4), // city
                                results.getString(5), // state
                                results.getString(6), // zip_code
                                results.getString(7), // telephone
                                results.getInt(8));   // rating
      else // Tuple was not found.
        error = "Customer #"
              + id
              + " was not found in our records.";

      // No JDBC errors occurred. Clean up.
      statement.close();

      return customer;
    }
    catch (SQLException e) { // A JDBC error occurred.
      error = "He's dead, Jim. Please report this error.";
      e.printStackTrace(); // For debugging only.

      return null;
    }
  }

  /**
   * Maps the tuple from the <code>customer</code> database relation with the
   * matching <code>first_name</code>, <code>last_name</code>,
   * <code>address</code>, and <code>zip_code</code> attributes.
   * <p>
   * If the tuple exists, returns a <code>Customer</code> object that maps it.
   * <p>
   * If the tuple does not exist, returns a <code>null</code> reference and
   * assigns a user-friendly error message to <code>error</code>.
   *
   * @param firstName the <code>customer</code> first name that partially
   *                  describes the tuple to map
   * @param lastName  the <code>customer</code> last name that partially
   *                  describes the tuple to map
   * @param address   the <code>customer</code> address that partially describes
   *                  the tuple to map
   * @param zipCode   the <code>customer</code> zip code that partially
   *                  describes the tuple to map
   * @return          a fully instantiated <code>Customer</code> object if the
   *                  tuple exists; <code>null</code> otherwise
   */
  public static Customer getCustomer(String firstName,
                                     String lastName,
                                     String address,
                                     String zipCode)
  {
    try {
      // Ready the SQL statement.
      String sql = String.format("SELECT customer_id, city, state, telephone,"
                               + "       rating\n"
                               + "  FROM customer\n"
                               + " WHERE first_name = '%s'\n"
                               + "   AND last_name = '%s'\n"
                               + "   AND address = '%s'\n"
                               + "   AND zip_code = '%s'",
                                 firstName,
                                 lastName,
                                 address,
                                 zipCode);

      // Attempt a read from the customer relation if the database connection is
      // active.
      Statement statement = DBConnector.createStatement(false);
      ResultSet results   = null;
      if (statement != null) // Database connection is active.
        results = statement.executeQuery(sql);
      else { // Database connection is not active.
        error = DBConnector.error;

        return null;
      }

      Customer customer = null;

      // Check to see if a matching tuple was found.
      if (results.next()) // Tuple was found.
        customer = new Customer(results.getInt(1),    // customer_id
                                lastName,             // last_name
                                firstName,            // first_name
                                address,              // address
                                results.getString(2), // city
                                results.getString(3), // state
                                zipCode,              // zip_code
                                results.getString(4), // telephone
                                results.getInt(5));   // rating
      else // Tuple was not found.
        error = "Customer "
              + firstName
              + " "
              + lastName
              + " with address\n  "
              + address
              + ", "
              + zipCode
              + "\nwas not found in our records.";

      // No JDBC errors occurred. Clean up.
      statement.close();

      return customer;
    }
    catch (SQLException e) { // A JDBC error occurred.
      error = "He's dead, Jim. Please report this error.";
      e.printStackTrace(); // For debugging only.

      return null;
    }
  }

  /**
   * Maps the tuple from the <code>account</code> database relation with the
   * matching <code>account_id</code> attribute.
   * <p>
   * If the tuple exists, returns an <code>Account</code> object that maps it.
   * <p>
   * If the tuple does not exist, returns a <code>null</code> reference and
   * assigns a user-friendly error message to <code>error</code>.
   *
   * @param id the <code>account</code> identifier describing the tuple to map
   * @return   a fully instantiated <code>Account</code> object if the tuple
   *           exists; <code>null</code> otherwise
   */
  public static Account getAccount(int id)
  {
    try {
      // Ready the SQL statement.
      String sql = String.format("SELECT customer_id, email, credit_card,"
                               + "       creation_date\n"
                               + "  FROM account\n"
                               + " WHERE account_id = %d",
                                 id);

      // Attempt a read from the account relation if the database connection is
      // active.
      Statement statement = DBConnector.createStatement(false);
      ResultSet results   = null;
      if (statement != null) // Database connection is active.
        results = statement.executeQuery(sql);
      else { // Database connection is not active.
        error = DBConnector.error;

        return null;
      }

      Account account = null;

      // Check to see if a matching tuple was found.
      if (results.next()) { // Tuple was found.
        Customer customer = getCustomer(results.getInt(1));

        account = new Account(id,                   // account_id
                              customer,             // customer_id
                              results.getString(2), // email
                              results.getString(3), // credit_card
                              results.getDate(4));  // creation_date
      }
      else // Tuple was not found.
        error = "Account #"
              + id
              + " was not found in our records.";

      // No JDBC errors occurred. Clean up.
      statement.close();

      return account;
    }
    catch (SQLException e) { // A JDBC error occurred.
      error = "He's dead, Jim. Please report this error.";
      e.printStackTrace(); // For debugging only.

      return null;
    }
  }

  /**
   * Maps the tuple from the <code>account</code> database relation with the
   * matching <code>email</code> attribute.
   * <p>
   * If the tuple exists, returns an <code>Account</code> object that maps it.
   * <p>
   * If the tuple does not exist, returns a <code>null</code> reference and
   * assigns a user-friendly error message to <code>error</code>.
   *
   * @param email the unique <code>account</code> email describing the tuple to
   *              map
   * @return      a fully instantiated <code>Account</code> object if the
   *              tuple exists; <code>null</code> otherwise
   */
  public static Account getAccount(String email)
  {
    try {
      // Ready the SQL statement.
      String sql = String.format("SELECT customer_id, account_id, credit_card,"
                               + "       creation_date\n"
                               + "  FROM account\n"
                               + " WHERE email = '%s'",
                                 email);

      // Attempt a read from the account relation if the database connection is
      // active.
      Statement statement = DBConnector.createStatement(false);
      ResultSet results   = null;
      if (statement != null) // Database connection is active.
        results = statement.executeQuery(sql);
      else { // Database connection is not active.
        error = DBConnector.error;

        return null;
      }

      Account account = null;

      // Check to see if a matching tuple was found.
      if (results.next()) { // Tuple was found.
        Customer customer = getCustomer(results.getInt(1));

        account = new Account(results.getInt(2),    // account_id
                              customer,             // customer_id
                              email,                // email
                              results.getString(3), // credit_card
                              results.getDate(4));  // creation_date
      }
      else // Tuple was not found.
        error = "No account with email address "
              + email
              + " was found in our records.";

      // No JDBC errors occurred. Clean up.
      statement.close();

      return account;
    }
    catch (SQLException e) { // A JDBC error occurred.
      error = "He's dead, Jim. Please report this error.";
      e.printStackTrace(); // For debugging only.

      return null;
    }
  }

  /**
   * Maps the tuple from the <code>employee</code> database relation with the
   * matching <code>employee_id</code> attribute.
   * <p>
   * If the tuple exists, returns an <code>Employee</code> object that maps it.
   * <p>
   * If the tuple does not exist, returns a <code>null</code> reference and
   * assigns a user-friendly error message to <code>error</code>.
   *
   * @param id the <code>employee</code> identifier describing the tuple to map
   * @return   a fully instantiated <code>Employee</code> object if the tuple
   *           exists; <code>null</code> otherwise
   */
  public static Employee getEmployee(int id)
  {
    try {
      // Ready the SQL statement.
      String sql = String.format("SELECT wage, last_name, first_name, address,"
                               + "       city, state, zip_code, telephone,"
                               + "       ssn, hire_date, manager_id\n"
                               + "  FROM employee\n"
                               + " WHERE employee_id = %d",
                                 id);

      // Attempt a read from the employee relation if the database connection is
      // active.
      Statement statement = DBConnector.createStatement(false);
      ResultSet results   = null;
      if (statement != null) // Database connection is active.
        results = statement.executeQuery(sql);
      else { // Database connection is not active.
        error = DBConnector.error;

        return null;
      }

      Employee employee = null;

      // Check to see if a matching tuple was found.
      if (results.next()) { // Tuple was found.
        double wage = results.getBigDecimal(1).doubleValue();

        employee = new Employee(id,                   // employee_id
                                results.getString(2), // last_name
                                results.getString(3), // first_name
                                results.getString(4), // address
                                results.getString(5), // city
                                results.getString(6), // state
                                results.getString(7), // zip_code
                                results.getString(8), // telephone
                                results.getString(9), // ssn
                                results.getDate(10),  // hire_date
                                wage,                 // wage
                                results.getInt(11));  // manager_id
      }
      else // Tuple was not found.
        error = "Employee #"
              + id
              + " was not found in our records.";

      // No JDBC errors occurred. Clean up.
      statement.close();

      return employee;
    }
    catch (SQLException e) { // A JDBC error occurred.
      error = "He's dead, Jim. Please report this error.";
      e.printStackTrace(); // For debugging only.

      return null;
    }
  }

  /**
   * Maps the tuple from the <code>employee</code> database relation with the
   * matching <code>ssn</code> attribute.
   * <p>
   * If the tuple exists, returns an <code>Employee</code> object that maps it.
   * <p>
   * If the tuple does not exist, returns a <code>null</code> reference and
   * assigns a user-friendly error message to <code>error</code>.
   *
   * @param ssn the unique <code>employee</code> social security number
   *            describing the tuple to map
   * @return    a fully instantiated <code>Employee</code> object if the tuple
   *            exists; <code>null</code> otherwise
   */
  public static Employee getEmployee(String ssn)
  {
    try {
      // Ready the SQL statement.
      String sql = String.format("SELECT wage, employee_id, last_name,"
                               + "       first_name, address, city, state,"
                               + "       zip_code, telephone, hire_date,"
                               + "       manager_id\n"
                               + "  FROM employee\n"
                               + " WHERE ssn = '%s'",
                                 ssn);

      // Attempt a read from the employee relation if the database connection is
      // active.
      Statement statement = DBConnector.createStatement(false);
      ResultSet results   = null;
      if (statement != null) // Database connection is active.
        results = statement.executeQuery(sql);
      else { // Database connection is not active.
        error = DBConnector.error;

        return null;
      }

      Employee employee = null;

      // Check to see if a matching tuple was found.
      if (results.next()) { // Tuple was found.
        double wage = results.getBigDecimal(1).doubleValue();

        employee = new Employee(results.getInt(2),    // employee_id
                                results.getString(3), // last_name
                                results.getString(4), // first_name
                                results.getString(5), // address
                                results.getString(6), // city
                                results.getString(7), // state
                                results.getString(8), // zip_code
                                results.getString(9), // telephone
                                ssn,                  // ssn
                                results.getDate(10),  // hire_date
                                wage,                 // wage
                                results.getInt(11));  // manager_id
      }
      else // Tuple was not found.
        error = "No employee with SSN "
              + ssn
              + " was found in our records.";

      // No JDBC errors occurred. Clean up.
      statement.close();

      return employee;
    }
    catch (SQLException e) { // A JDBC error occurred.
      error = "He's dead, Jim. Please report this error.";
      e.printStackTrace(); // For debugging only.

      return null;
    }
  }

  /**
   * Maps the tuple from the <code>reservation</code> database relation with the
   * matching <code>reservation_id</code> attribute.
   * <p>
   * If the tuple exists, returns a <code>Reservation</code> object that maps
   * it.
   * <p>
   * If the tuple does not exist, returns a <code>null</code> reference and
   * assigns a user-friendly error message to <code>error</code>.
   *
   * @param id the <code>reservation</code> identifier describing the tuple to
   *           map
   * @return   a fully instantiated <code>Reservation</code> object if the tuple
   *           exists; <code>null</code> otherwise
   */
  public static Reservation getReservation(int id)
  {
    try {
      // Ready the SQL statement.
      String sql = String.format("SELECT price, fee, employee_id, booking_date,"
                               + "       flight_type, flight_class\n"
                               + "  FROM reservation\n"
                               + " WHERE reservation_id = %d",
                                 id);

      // Attempt a read from the reservation relation if the database connection
      // is active.
      Statement statement = DBConnector.createStatement(false);
      ResultSet results   = null;
      if (statement != null) // Database connection is active.
        results = statement.executeQuery(sql);
      else { // Database connection is not active.
        error = DBConnector.error;

        return null;
      }

      Reservation reservation = null;

      // Check to see if a matching tuple was found.
      if (results.next()) { // Tuple was found.
        double   price    = results.getBigDecimal(1).doubleValue();
        double   fee      = results.getBigDecimal(2).doubleValue();
        Employee employee = getEmployee(results.getInt(3));

        reservation = new Reservation(id,                 // reservation_id
                                      results.getDate(4), // booking_date
                                      results.getInt(5),  // flight_type
                                      results.getInt(6),  // flight_class
                                      price,              // price
                                      fee,                // fee
                                      employee);          // employee_id
      }
      else // Tuple was not found.
        error = "Reservation #"
              + id
              + " was not found in our records.";

      // No JDBC errors occurred. Clean up.
      statement.close();

      return reservation;
    }
    catch (SQLException e) { // A JDBC error occurred.
      error = "He's dead, Jim. Please report this error.";
      e.printStackTrace(); // For debugging only.

      return null;
    }
  }

  /**
   * Maps the tuple from the <code>bid</code> database relation with the
   * matching <code>reservation_id</code>, <code>account_id</code>, and
   * <code>bid_amount</code> attributes.
   * <p>
   * If the tuple exists, returns a <code>Bid</code> object that maps it.
   * <p>
   * If the tuple does not exist, returns a <code>null</code> reference and
   * assigns a user-friendly error message to <code>error</code>.
   *
   * @param reservation a <code>Reservation</code> object containing the
   *                    <code>reservation</code> identifier that partially
   *                    describes the tuple to map
   * @param account     an <code>Account</code> object containing the
   *                    <code>account</code> identifier that partially describes
   *                    the tuple to map
   * @param amount      the amount that partially describes the tuple to map
   * @return            a fully instantiated <code>Bid</code> object if the
   *                    tuple exists; <code>null</code> otherwise
   */
  public static Bid getBid(Reservation reservation,
                           Account account,
                           double amount)
  {
    try {
      // Ready the SQL statement.
      String sql = String.format("SELECT *\n"
                               + "  FROM bid\n"
                               + " WHERE reservation_id = %d\n"
                               + "   AND account_id     = %d\n"
                               + "   AND bid_amount     = %.2f",
                                 reservation.getID(),
                                 account.getID(),
                                 amount);

      // Attempt a read from the bid relation if the database connection is
      // active.
      Statement statement = DBConnector.createStatement(false);
      ResultSet results   = null;
      if (statement != null) // Database connection is active.
        results = statement.executeQuery(sql);
      else { // Database connection is not active.
        error = DBConnector.error;

        return null;
      }

      Bid bid = null;

      // Check to see if a matching tuple was found.
      if (results.next()) // Tuple was found.
        bid = new Bid(reservation, // reservation_id
                      account,     // account_id
                      amount);     // amount
      else // Tuple was not found.
        error = String.format("No $%.2f bid for reservation #%d by account #%d "
                            + "was found in our records.",
                              amount,
                              reservation.getID(),
                              account.getID());

      // No JDBC errors occurred. Clean up.
      statement.close();

      return bid;
    }
    catch (SQLException e) { // A JDBC error occurred.
      error = "He's dead, Jim. Please report this error.";
      e.printStackTrace(); // For debugging only.

      return null;
    }
  }

  /**
   * Maps the tuple from the <code>booked</code> database relation with the
   * matching <code>reservation_id</code>, <code>account_id</code>,
   * <code>flight_id</code>, <code>airline_id</code>, and
   * <code>flight_order</code> attributes.
   * <p>
   * If the tuple exists, returns a <code>Booked</code> object that maps it.
   * <p>
   * If the tuple does not exist, returns a <code>null</code> reference and
   * assigns a user-friendly error message to <code>error</code>.
   *
   * @param reservation a <code>Reservation</code> object containing the
   *                    <code>reservation</code> identifier that partially
   *                    describes the tuple to map
   * @param account     an <code>Account</code> object containing the
   *                    <code>account</code> identifier that partially describes
   *                    the tuple to map
   * @param flight      a <code>Flight</code> object containing the
   *                    <code>flight</code> and <code>airline</code> identifiers
   *                    that partially describe the tuple to map
   * @param flightOrder the ordinal flight position that partially describes the
   *                    tuple to map
   * @return            a fully instantiated <code>Booked</code> object if the
   *                    tuple exists; <code>null</code> otherwise
   */
  public static Booked getBooked(Reservation reservation,
                                 Account account,
                                 Flight flight,
                                 int flightOrder)
  {
    try {
      // Ready the SQL statement.
      String sql = String.format("SELECT departure_date, seat_number,"
                               + "       special_meal\n"
                               + "  FROM booked\n"
                               + " WHERE reservation_id = %d\n"
                               + "   AND account_id     = %d\n"
                               + "   AND flight_id      = '%s'\n"
                               + "   AND airline_id     = '%s'\n"
                               + "   AND flight_order   = %d",
                                 reservation.getID(),
                                 account.getID(),
                                 flight.getID(),
                                 flight.getAirline().getID(),
                                 flightOrder);

      // Attempt a read from the booked relation if the database connection is
      // active.
      Statement statement = DBConnector.createStatement(false);
      ResultSet results   = null;
      if (statement != null) // Database connection is active.
        results = statement.executeQuery(sql);
      else { // Database connection is not active.
        error = DBConnector.error;

        return null;
      }

      Booked booked = null;

      // Check to see if a matching tuple was found.
      if (results.next()) // Tuple was found.
        booked = new Booked(reservation,        // reservation_id
                            account,            // account_id
                            flight,             // flight_id and airline_id
                            flightOrder,        // flight_order
                            results.getDate(1), // departure_date
                            results.getInt(2),  // seat_number
                            results.getInt(3)); // special_meal
      else // Tuple was not found.
        error = "No such booking for reservation #"
              + reservation.getID()
              + " was found in our records.";

      // No JDBC errors occurred. Clean up.
      statement.close();

      return booked;
    }
    catch (SQLException e) { // A JDBC error occurred.
      error = "He's dead, Jim. Please report this error.";
      e.printStackTrace(); // For debugging only.

      return null;
    }
  }
}
