////////////////////////////////////////////////////////////////////////////////
// Bryan Burke, Joshua Lambert, and Elizabeth Rothermel                       //
// Group 4                                                                    //
// INFX 499                                                                   //
// Fall 2015                                                                  //
// University of Louisiana at Lafayette                                       //
////////////////////////////////////////////////////////////////////////////////

import java.sql.Date;

/**
 * Provides an in-memory mapping of a tuple from the <code>reservation</code>
 * database relation.
 *
 * @author Bryan Burke
 */
public class Reservation
{
  //////////////////////////////////////////////////////////////////////////////
  // CLASS VARIABLES                                                          //
  //////////////////////////////////////////////////////////////////////////////

  /**
   * Format specifier for <code>reservation.reservation_id</code>.
   */
  public static final String FORMAT_ID           = "%8d";
  /**
   * Format specifier for <code>reservation.booking_date</code>.
   */
  public static final String FORMAT_BOOKING_DATE = "%10.10s";
  /**
   * Format specifier for <code>reservation.flight_type</code>.
   */
  public static final String FORMAT_FLIGHT_TYPE  = "%1d";
  /**
   * Format specifier for <code>reservation.flight_class</code>.
   */
  public static final String FORMAT_FLIGHT_CLASS = "%1d";
  /**
   * Format specifier for <code>reservation.price</code>.
   */
  public static final String FORMAT_PRICE        = "%9.2f";
  /**
   * Format specifier for <code>reservation.fee</code>.
   */
  public static final String FORMAT_FEE          = "%9.2f";



  //////////////////////////////////////////////////////////////////////////////
  // INSTANCE VARIABLES                                                       //
  //////////////////////////////////////////////////////////////////////////////

  private int      id_;          // Auto-generated reservation identifier.
  private Date     bookingDate_; // Date of booking.
  private int      flightType_;  // Type of flight.
  private int      flightClass_; // Travel class.
  private double   price_;       // Total price for all associated bookings.
  private double   fee_;         // Booking fee.
  private Employee employee_;    // Customer support representative.



  //////////////////////////////////////////////////////////////////////////////
  // CONSTRUCTORS                                                             //
  //////////////////////////////////////////////////////////////////////////////

  /**
   * Creates an object of this class with all instance variables initialized.
   * <p>
   * This constructor is used by <code>DBMapper.getReservation(int)</code> and
   * should not be called directly by frontend code.
   *
   * @param id          the unique reservation identifier
   * @param bookingDate the date of booking
   * @param flightType  the type of the flight
   * @param flightClass the travel class
   * @param price       the total price for all associated bookings
   * @param fee         the booking fee
   * @param employee    the customer support representative
   */
  public Reservation(int      id,
                     Date     bookingDate,
                     int      flightType,
                     int      flightClass,
                     double   price,
                     double   fee,
                     Employee employee)
  {
    id_          = id;
    bookingDate_ = new Date(bookingDate.getTime());
    flightType_  = flightType;
    flightClass_ = flightClass;
    price_       = price;
    fee_         = fee;
    employee_    = new Employee(employee);
  }

  /**
   * Creates an object of this class, initializing only those instance variables
   * necessary to add a new tuple to the <code>reservation</code> relation.
   * <p>
   * This constructor should be called by frontend code when instantiating an
   * object to pass to <code>DBInserter.insertReservation(Reservation)</code>.
   *
   * @param flightType  the type of the flight
   * @param flightClass the travel class
   * @param price       the total price for all associated bookings
   * @param employee    the customer support representative
   */
  public Reservation(int      flightType,
                     int      flightClass,
                     double   price,
                     Employee employee)
  {
    id_          = 0;    // Default database value.
    bookingDate_ = null; // Default database value.
    flightType_  = flightType;
    flightClass_ = flightClass;
    price_       = price;
    fee_         = 0.0;  // Default database value.
    employee_    = new Employee(employee);
  }

  /**
   * Creates a deep copy of an object of this class.
   *
   * @param other the object whose state should be copied
   */
  public Reservation(Reservation other)
  {
    this.id_          = other.id_;
    this.flightType_  = other.flightType_;
    this.flightClass_ = other.flightClass_;
    this.price_       = other.price_;
    this.fee_         = other.fee_;
    this.employee_    = new Employee(other.employee_);

    // bookingDate_ can be null, so it requires special handling.
    if (other.bookingDate_ != null)
      this.bookingDate_ = new Date(other.bookingDate_.getTime());
    else
      this.bookingDate_ = null;
  }



  //////////////////////////////////////////////////////////////////////////////
  // METHODS                                                                  //
  //////////////////////////////////////////////////////////////////////////////

  /**
   * Returns the reservation number auto-generated by the database.
   *
   * @return the reservation identifier
   */
  public int getID()
  {
    return id_;
  }

  /**
   * Returns a <code>java.sql.Date</code> object that maps the booking date's
   * ISO 8601 (yyyy-mm-dd) representation.
   *
   * @return the date of booking
   */
  public Date getBookingDate()
  {
    // bookingDate_ can be null, so it requires special handling.
    if (bookingDate_ != null)
      return new Date(bookingDate_.getTime());
    else
      return null;
  }

  /**
   * Returns the flight type encoded as follows:
   * <ul>
   * <li>0 = one-way
   * <li>1 = round-trip
   * <li>2 = multi-city
   * </ul>
   *
   * @return the type of the flight
   */
  public int getFlightType()
  {
    return flightType_;
  }

  /**
   * Returns the travel class encoded as follows:
   * <ul>
   * <li>0 = first
   * <li>1 = business
   * <li>2 = economy
   * </ul>
   *
   * @return the travel class
   */
  public int getFlightClass()
  {
    return flightClass_;
  }

  /**
   * Returns the USD total price for all flights booked as part of this
   * reservation.
   *
   * @return the total price for all associated bookings
   */
  public double getPrice()
  {
    return price_;
  }

  /**
   * Returns the USD amount calculated as a service fee.
   *
   * @return the booking fee
   */
  public double getFee()
  {
    return fee_;
  }

  /**
   * Returns an <code>Employee</code> object that maps the customer service
   * representative's relational data.
   *
   * @return the customer service representative
   */
  public Employee getEmployee()
  {
    return new Employee(employee_);
  }



  /**
   * Peforms an equality comparison on two objects of this class.
   *
   * @param other the object whose state should be compared with the state of
   *              this object
   * @return      <code>true</code> if the object states are equal;
   *              <code>false</code> otherwise
   */
  public boolean equals(Reservation other)
  {
    boolean equalsID          = this.id_ == other.id_;
    boolean equalsFlightType  = this.flightType_ == other.flightType_;
    boolean equalsFlightClass = this.flightClass_ == other.flightClass_;
    boolean equalsPrice       = this.price_ == other.price_;
    boolean equalsFee         = this.fee_ == other.fee_;
    boolean equalsEmployee    = this.employee_.equals(other.employee_);

    // bookingDate_ can be null, so it requires special handling.
    boolean equalsBookingDate = false;
    if (this.bookingDate_ != null && other.bookingDate_ != null)
      equalsBookingDate = this.bookingDate_.equals(other.bookingDate_);
    else if (this.bookingDate_ == null)
      equalsBookingDate = other.bookingDate_ == null;
    else if (other.bookingDate_ == null)
      equalsBookingDate = this.bookingDate_ == null;

    if (equalsID
        && equalsBookingDate
        && equalsFlightType
        && equalsFlightClass
        && equalsPrice
        && equalsFee
        && equalsEmployee)
      return true;
    else
      return false;
  }

  /**
   * Returns a string representation of this object.
   *
   * @return a string describing the state of this object
   */
  public String toString()
  {
    return String.format(FORMAT_ID, id_)
         + "   "
         + String.format(FORMAT_BOOKING_DATE, bookingDate_)
         + "   "
         + String.format(FORMAT_FLIGHT_TYPE, flightType_)
         + "   "
         + String.format(FORMAT_FLIGHT_CLASS, flightClass_)
         + "   "
         + String.format(FORMAT_PRICE, price_)
         + "   "
         + String.format(FORMAT_FEE, fee_)
         + "   "
         + String.format(Employee.FORMAT_ID, employee_.getID());
  }
}
