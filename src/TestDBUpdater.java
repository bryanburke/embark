////////////////////////////////////////////////////////////////////////////////
// Bryan Burke, Joshua Lambert, and Elizabeth Rothermel                       //
// Group 4                                                                    //
// INFX 499                                                                   //
// Fall 2015                                                                  //
// University of Louisiana at Lafayette                                       //
////////////////////////////////////////////////////////////////////////////////

/**
 * Performs rudimentary unit testing on <code>DBUpdater</code> methods. Should
 * only be run once on a fresh copy of the sample database.
 *
 * @author Bryan Burke
 */
public class TestDBUpdater
{
  //////////////////////////////////////////////////////////////////////////////
  // METHODS                                                                  //
  //////////////////////////////////////////////////////////////////////////////

  /**
   * Calls <code>DBUpdater</code> methods to update data in the database.
   *
   * @param args command-line arguments; unused
   */
  public static void main(String[] args)
  {
    // Attempt to connect to the database.
    if (DBConnector.connect()) { // Connection succeeded.
      //////////////////////////////////////////////////////////////////////////
      // TEST METHODS                                                         //
      //////////////////////////////////////////////////////////////////////////

      System.out.println("== BEGIN METHOD TESTS ==");

      // updateCustomer(Customer)
      System.out.println("= updateCustomer(c3) =");
      Customer c3 = DBMapper.getCustomer(3);
      c3.setRating(1);
      if (DBUpdater.updateCustomer(c3))
        System.out.println("Customer #3 updated with rating 1.");
      else
        System.out.println(DBUpdater.error);

      // updateAccount(Account)
      System.out.println("= updateAccount(a3) =");
      Account a3 = DBMapper.getAccount(3);
      a3.setCreditCard("557093550381XXXX");
      if (DBUpdater.updateAccount(a3))
        System.out.println("Account #3 updated with credit card number "
                         + "557093550381XXXX.");
      else
        System.out.println(DBUpdater.error);

      // updateEmployee(Employee)
      System.out.println("= updateEmployee(e4) =");
      Employee e4 = DBMapper.getEmployee(4);
      e4.setWage(15.0);
      if (DBUpdater.updateEmployee(e4))
        System.out.println("Employee #4 updated with wage $15.");
      else
        System.out.println(DBUpdater.error);

      System.out.println("== END METHOD TESTS ==");

      // Close the database connection and clean up.
      DBConnector.disconnect();
    }
    else { // Connection did not succeed.
      System.out.println(DBConnector.error);

      System.exit(1);
    }
  }
}
