////////////////////////////////////////////////////////////////////////////////
// Bryan Burke, Joshua Lambert, and Elizabeth Rothermel                       //
// Group 4                                                                    //
// INFX 499                                                                   //
// Fall 2015                                                                  //
// University of Louisiana at Lafayette                                       //
////////////////////////////////////////////////////////////////////////////////

import java.sql.SQLException;
import java.sql.SQLWarning;
import java.sql.Statement;

/**
 * Provides methods to delete tuples from certain database relations.
 *
 * @author Bryan Burke
 */
public final class DBDeleter
{
  //////////////////////////////////////////////////////////////////////////////
  // CLASS VARIABLES                                                          //
  //////////////////////////////////////////////////////////////////////////////

  /**
   * Error message describing the most recent database access error.
   */
  public static String error = null;



  //////////////////////////////////////////////////////////////////////////////
  // CONSTRUCTORS                                                             //
  //////////////////////////////////////////////////////////////////////////////

  // This class is non-constructable.
  private DBDeleter() {}



  //////////////////////////////////////////////////////////////////////////////
  // METHODS                                                                  //
  //////////////////////////////////////////////////////////////////////////////

  /**
   * Deletes the tuple from the <code>customer</code> database relation with the
   * matching <code>customer_id</code> attribute.
   * <p>
   * If deletion is successful, returns <code>true</code>.
   * <p>
   * If deletion fails, returns <code>false</code> and assigns a user-friendly
   * error message to <code>error</code>.
   *
   * @param customer a <code>Customer</code> object representing the tuple to
   *                 delete
   * @return         <code>true</code> if deletion succeeded; <code>false</code>
   *                 otherwise
   */
  public static boolean deleteCustomer(Customer customer)
  {
    try {
      // Ready the SQL statement.
      String sql = String.format("DELETE FROM customer\n"
                               + " WHERE customer_id = %d",
                                 customer.getID());

      // Attempt a delete from the customer relation if the database connection
      // is active.
      Statement statement = DBConnector.createStatement(false);
      int       nRows     = 0;
      if (statement != null) // Database connection is active.
        nRows = statement.executeUpdate(sql);
      else { // Database connection is not active.
        error = DBConnector.error;

        return false;
      }

      boolean isDeleted = true;

      // Check for JDBC warnings.
      SQLWarning w = statement.getWarnings();
      if (w != null) {
        if (w.getSQLState().equals("02000")) {
          error = "Customer #"
                + customer.getID()
                + " was not found in our records.";

          isDeleted = false;
        }
      }

      // No JDBC errors occurred. Clean up.
      statement.close();

      return isDeleted;
    }
    catch (SQLException e) { // A JDBC error occurred.
      if (e.getSQLState().equals("23503"))
        error = "Customer #"
              + customer.getID()
              + " cannot be deleted because one or more associated accounts "
              + "exist in our records.";
      else {
        error = "He's dead, Jim. Please report this error.";
        e.printStackTrace(); // For debugging only.
      }

      return false;
    }
  }

  /**
   * Deletes the tuple from the <code>account</code> database relation with the
   * matching <code>account_id</code> attribute.
   * <p>
   * If deletion is successful, returns <code>true</code>.
   * <p>
   * If deletion fails, returns <code>false</code> and assigns a user-friendly
   * error message to <code>error</code>.
   *
   * @param account an <code>Account</code> object representing the tuple to
   *                delete
   * @return        <code>true</code> if deletion succeeded; <code>false</code>
   *                otherwise
   */
  public static boolean deleteAccount(Account account)
  {
    try {
      // Ready the SQL statement.
      String sql = String.format("DELETE FROM account\n"
                               + " WHERE account_id = %d",
                                 account.getID());

      // Attempt a delete from the account relation if the database connection
      // is active.
      Statement statement = DBConnector.createStatement(false);
      int       nRows     = 0;
      if (statement != null) // Database connection is active.
        nRows = statement.executeUpdate(sql);
      else { // Database connection is not active.
        error = DBConnector.error;

        return false;
      }

      boolean isDeleted = true;

      // Check for JDBC warnings.
      SQLWarning w = statement.getWarnings();
      if (w != null) {
        if (w.getSQLState().equals("02000")) {
          error = "Account #"
                + account.getID()
                + " was not found in our records.";

          isDeleted = false;
        }
      }

      // No JDBC errors occurred. Clean up.
      statement.close();

      return isDeleted;
    }
    catch (SQLException e) { // A JDBC error occurred.
      if (e.getSQLState().equals("23503"))
        error = "Account #"
              + account.getID()
              + " cannot be deleted because one or more associated "
              + "reservations exist in our records.";
      else {
        error = "He's dead, Jim. Please report this error.";
        e.printStackTrace(); // For debugging only.
      }

      return false;
    }
  }

  /**
   * Deletes the tuple from the <code>employee</code> database relation with the
   * matching <code>employee_id</code> attribute.
   * <p>
   * If deletion is successful, returns <code>true</code>.
   * <p>
   * If deletion fails, returns <code>false</code> and assigns a user-friendly
   * error message to <code>error</code>.
   *
   * @param employee an <code>Employee</code> object representing the tuple to
   *                 delete
   * @return         <code>true</code> if deletion succeeded; <code>false</code>
   *                 otherwise
   */
  public static boolean deleteEmployee(Employee employee)
  {
    try {
      // Ready the SQL statement.
      String sql = String.format("DELETE FROM employee\n"
                               + " WHERE employee_id = %d",
                                 employee.getID());

      // Attempt a delete from the employee relation if the database connection
      // is active.
      Statement statement = DBConnector.createStatement(false);
      int       nRows     = 0;
      if (statement != null) // Database connection is active.
        nRows = statement.executeUpdate(sql);
      else { // Database connection is not active.
        error = DBConnector.error;

        return false;
      }

      boolean isDeleted = true;

      // Check for JDBC warnings.
      SQLWarning w = statement.getWarnings();
      if (w != null) {
        if (w.getSQLState().equals("02000")) {
          error = "Employee #"
                + employee.getID()
                + " was not found in our records.";

          isDeleted = false;
        }
      }

      // No JDBC errors occurred. Clean up.
      statement.close();

      return isDeleted;
    }
    catch (SQLException e) { // A JDBC error occurred.
      if (e.getSQLState().equals("23503"))
        error = "Employee #"
              + employee.getID()
              + " cannot be deleted because one or more associated "
              + "reservations exist in our records.";
      else {
        error = "He's dead, Jim. Please report this error.";
        e.printStackTrace(); // For debugging only.
      }

      return false;
    }
  }

  /**
   * Deletes the tuple from the <code>reservation</code> database relation with
   * the matching <code>reservation_id</code> attribute.
   * <p>
   * If deletion is successful, returns <code>true</code>.
   * <p>
   * If deletion fails, returns <code>false</code> and assigns a user-friendly
   * error message to <code>error</code>.
   *
   * @param reservation an <code>Employee</code> object representing the tuple
   *                    to delete
   * @return            <code>true</code> if deletion succeeded;
   *                    <code>false</code> otherwise
   */
  public static boolean deleteReservation(Reservation reservation)
  {
    try {
      // Ready the SQL statement.
      String sql = String.format("DELETE FROM reservation\n"
                               + " WHERE reservation_id = %d",
                                 reservation.getID());

      // Attempt a delete from the reservation relation if the database
      // connection is active.
      Statement statement = DBConnector.createStatement(false);
      int       nRows     = 0;
      if (statement != null) // Database connection is active.
        nRows = statement.executeUpdate(sql);
      else { // Database connection is not active.
        error = DBConnector.error;

        return false;
      }

      boolean isDeleted = true;

      // Check for JDBC warnings.
      SQLWarning w = statement.getWarnings();
      if (w != null) {
        if (w.getSQLState().equals("02000")) {
          error = "Reservation #"
                + reservation.getID()
                + " was not found in our records.";

          isDeleted = false;
        }
      }

      // No JDBC errors occurred. Clean up.
      statement.close();

      return isDeleted;
    }
    catch (SQLException e) { // A JDBC error occurred.
      error = "He's dead, Jim. Please report this error.";
      e.printStackTrace(); // For debugging only.

      return false;
    }
  }
}
