////////////////////////////////////////////////////////////////////////////////
// Bryan Burke, Joshua Lambert, and Elizabeth Rothermel                       //
// Group 4                                                                    //
// INFX 499                                                                   //
// Fall 2015                                                                  //
// University of Louisiana at Lafayette                                       //
////////////////////////////////////////////////////////////////////////////////

import java.io.IOException;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

/**
 * Performs rudimentary unit testing on <code>Account</code> objects.
 *
 * @author Bryan Burke
 */
public class TestAccount
{
  //////////////////////////////////////////////////////////////////////////////
  // METHODS                                                                  //
  //////////////////////////////////////////////////////////////////////////////

  /**
   * Creates and tests <code>Account</code> objects.
   *
   * @param args command-line arguments; unused
   */
  public static void main(String[] args) throws IOException, ParseException
  {
    ////////////////////////////////////////////////////////////////////////////
    // TEST CONSTRUCTORS                                                      //
    ////////////////////////////////////////////////////////////////////////////

    System.out.println("== BEGIN CONSTRUCTOR TESTS ==");

    // Prerequisite objects.
    Customer patelK    = new Customer(5,
                                      "Patel",
                                      "Kimberly",
                                      "3548 Orchard Street",
                                      "Osseo",
                                      "MN",
                                      "55369",
                                      "9528552611",
                                      2);
    Customer phillipsV = new Customer("Phillips",
                                      "Valeria",
                                      "4254 Gladwell Street",
                                      "Dallas",
                                      "TX",
                                      "75207",
                                      "9032214245");

    ArrayList<Account> accounts   = new ArrayList<>(3);
    SimpleDateFormat   dateFormat = new SimpleDateFormat("yyyy-MM-dd");

    // Account(int, Customer, String, String, Date)
    accounts.add(new Account(5,
                             patelK,
                             "patel.kimberly@locationcalculator.com",
                             "542802306410XXXX",
                           new Date(dateFormat.parse("2009-07-19").getTime())));
    System.out.println("  Account #5 constructed as accounts[0].");

    // Account(Customer, String, String)
    accounts.add(new Account(phillipsV,
                             "phillips.valeria@searchingpays.com",
                             "455633472753XXXX"));
    System.out.println("  New account constructed as accounts[1].");

    // Account(Account)
    accounts.add(new Account(accounts.get(0)));
    System.out.println("  Account #5 copy constructed as accounts[2].");

    System.out.println("== END CONSTRUCTOR TESTS ==");
    System.out.println();



    ////////////////////////////////////////////////////////////////////////////
    // TEST METHODS                                                           //
    ////////////////////////////////////////////////////////////////////////////

    System.out.println("== BEGIN METHOD TESTS ==");

    // getID()
    System.out.println("= getID() =");
    for (int i = 0; i < accounts.size(); ++i)
      System.out.printf("  [%d] : %d\n",
                        i,
                        accounts.get(i).getID());
    System.out.print("Verify the results and press ENTER to continue.");
    System.in.read();

    // getCustomer()
    System.out.println("= getCustomer() =");
    for (int i = 0; i < accounts.size(); ++i)
      System.out.printf("  [%d] : %s\n",
                        i,
                        accounts.get(i).getCustomer());
    System.out.print("Verify the results and press ENTER to continue.");
    System.in.read();

    // getEmail()
    System.out.println("= getEmail() =");
    for (int i = 0; i < accounts.size(); ++i)
      System.out.printf("  [%d] : %s\n",
                        i,
                        accounts.get(i).getEmail());
    System.out.print("Verify the results and press ENTER to continue.");
    System.in.read();

    // getCreditCard()
    System.out.println("= getCreditCard() =");
    for (int i = 0; i < accounts.size(); ++i)
      System.out.printf("  [%d] : %s\n",
                        i,
                        accounts.get(i).getCreditCard());
    System.out.print("Verify the results and press ENTER to continue.");
    System.in.read();

    // getCreationDate()
    System.out.println("= getCreationDate() =");
    for (int i = 0; i < accounts.size(); ++i) {
      System.out.printf("  [%d] : %s\n",
                        i,
                        accounts.get(i).getCreationDate());
    }
    System.out.print("Verify the results and press ENTER to continue.");
    System.in.read();

    // equals(Account)
    System.out.println("= equals([0]) =");
    for (int i = 0; i < accounts.size(); ++i)
      System.out.printf("  [%d] : %s\n",
                        i,
                        accounts.get(i).equals(accounts.get(0)));
    System.out.print("Verify the results and press ENTER to continue.");
    System.in.read();

    // toString()
    System.out.println("= toString() =");
    for (int i = 0; i < accounts.size(); ++i)
      System.out.printf("  [%d] : %s\n",
                        i,
                        accounts.get(i).toString());
    System.out.print("Verify the results and press ENTER to continue.");
    System.in.read();

    // setEmail(String)
    System.out.println("= [1].setEmail(\"phillips.valeria@teleworm.us\") =");
    accounts.get(1).setEmail("phillips.valeria@teleworm.us");
    // setCreditCard(String)
    System.out.println("= [1].setCreditCard(\"471645990653XXXX\") =");
    accounts.get(1).setCreditCard("471645990653XXXX");

    System.out.printf("  [1] : %s\n",
                      accounts.get(1).toString());
    System.out.print("Verify the results and press ENTER to continue.");
    System.in.read();

    System.out.println("== END METHOD TESTS ==");
  }
}
