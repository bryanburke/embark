////////////////////////////////////////////////////////////////////////////////
// Bryan Burke, Joshua Lambert, and Elizabeth Rothermel                       //
// Group 4                                                                    //
// INFX 499                                                                   //
// Fall 2015                                                                  //
// University of Louisiana at Lafayette                                       //
////////////////////////////////////////////////////////////////////////////////

/**
 * Performs rudimentary unit testing on <code>DBDeleter</code> methods. Should
 * only be run once after running <code>TestDBInserter</code>.
 *
 * @author Bryan Burke
 */
public class TestDBDeleter
{
  //////////////////////////////////////////////////////////////////////////////
  // METHODS                                                                  //
  //////////////////////////////////////////////////////////////////////////////

  /**
   * Calls <code>DBDeleter</code> methods to delete data from the database.
   *
   * @param args command-line arguments; unused
   */
  public static void main(String[] args)
  {
    // Attempt to connect to the database.
    if (DBConnector.connect()) { // Connection succeeded.
      //////////////////////////////////////////////////////////////////////////
      // TEST METHODS                                                         //
      //////////////////////////////////////////////////////////////////////////

      System.out.println("== BEGIN METHOD TESTS ==");

      // deleteReservation(Reservation)
      System.out.println("= deleteReservation(r8) =");
      Reservation r8 = DBMapper.getReservation(8);
      if (DBDeleter.deleteReservation(r8))
        System.out.println("Reservation #"
                         + r8.getID()
                         + " deleted successfully.");
      else
        System.out.println(DBDeleter.error);

      // deleteEmployee(Employee)
      System.out.println("= deleteEmployee(e5) =");
      Employee e5 = DBMapper.getEmployee(5);
      if (DBDeleter.deleteEmployee(e5))
        System.out.println("Employee #"
                         + e5.getID()
                         + " deleted successfully.");
      else
        System.out.println(DBDeleter.error);

      System.out.println("= deleteEmployee(e6) =");
      Employee e6 = DBMapper.getEmployee(6);
      if (DBDeleter.deleteEmployee(e6))
        System.out.println("Employee #"
                         + e6.getID()
                         + " deleted successfully.");
      else
        System.out.println(DBDeleter.error);

      // deleteAccount(Account)
      System.out.println("= deleteAccount(a9) =");
      Account a9 = DBMapper.getAccount(9);
      if (DBDeleter.deleteAccount(a9))
        System.out.println("Account #"
                         + a9.getID()
                         + " deleted successfully.");
      else
        System.out.println(DBDeleter.error);

      // deleteCustomer(Customer)
      System.out.println("= deleteCustomer(c8) =");
      Customer c8 = DBMapper.getCustomer(8);
      if (DBDeleter.deleteCustomer(c8))
        System.out.println("Customer #"
                         + c8.getID()
                         + " deleted successfully.");
      else
        System.out.println(DBDeleter.error);

      System.out.println("== END METHOD TESTS ==");

      // Close the database connection and clean up.
      DBConnector.disconnect();
    }
    else { // Connection did not succeed.
      System.out.println(DBConnector.error);

      System.exit(1);
    }
  }
}
