////////////////////////////////////////////////////////////////////////////////
// Bryan Burke, Joshua Lambert, and Elizabeth Rothermel                       //
// Group 4                                                                    //
// INFX 499                                                                   //
// Fall 2015                                                                  //
// University of Louisiana at Lafayette                                       //
////////////////////////////////////////////////////////////////////////////////

import java.sql.Date;

/**
 * Provides an in-memory mapping of a tuple from the <code>employee</code>
 * database relation.
 *
 * @author Bryan Burke
 */
public class Employee
{
  //////////////////////////////////////////////////////////////////////////////
  // CLASS VARIABLES                                                          //
  //////////////////////////////////////////////////////////////////////////////

  /**
   * Format specifier for <code>employee.employee_id</code>.
   */
  public static final String FORMAT_ID         = "%8d";
  /**
   * Format specifier for <code>employee.last_name</code>.
   */
  public static final String FORMAT_LAST_NAME  = "%20.20s";
  /**
   * Format specifier for <code>employee.first_name</code>.
   */
  public static final String FORMAT_FIRST_NAME = "%20.20s";
  /**
   * Format specifier for <code>employee.address</code>.
   */
  public static final String FORMAT_ADDRESS    = "%30.30s";
  /**
   * Format specifier for <code>employee.city</code>.
   */
  public static final String FORMAT_CITY       = "%20.20s";
  /**
   * Format specifier for <code>employee.state</code>.
   */
  public static final String FORMAT_STATE      = "%2.2s";
  /**
   * Format specifier for <code>employee.zip_code</code>.
   */
  public static final String FORMAT_ZIP_CODE   = "%5.5s";
  /**
   * Format specifier for <code>employee.telephone</code>.
   */
  public static final String FORMAT_TELEPHONE  = "%10.10s";
  /**
   * Format specifier for <code>employee.ssn</code>.
   */
  public static final String FORMAT_SSN        = "%9.9s";
  /**
   * Format specifier for <code>employee.hire_date</code>.
   */
  public static final String FORMAT_HIRE_DATE  = "%10.10s";
  /**
   * Format specifier for <code>employee.wage</code>.
   */
  public static final String FORMAT_WAGE       = "%6.2f";



  /**
   * Maximum string length for <code>employee.last_name</code>.
   */
  public static final int MAX_LENGTH_LAST_NAME  = 50;
  /**
   * Maximum string length for <code>employee.first_name</code>.
   */
  public static final int MAX_LENGTH_FIRST_NAME = 50;
  /**
   * Maximum string length for <code>employee.address</code>.
   */
  public static final int MAX_LENGTH_ADDRESS    = 100;
  /**
   * Maximum string length for <code>employee.city</code>.
   */
  public static final int MAX_LENGTH_CITY       = 40;
  /**
   * Maximum string length for <code>employee.state</code>.
   */
  public static final int MAX_LENGTH_STATE      = 2;
  /**
   * Maximum string length for <code>employee.zip_code</code>.
   */
  public static final int MAX_LENGTH_ZIP_CODE   = 5;
  /**
   * Maximum string length for <code>employee.telephone</code>.
   */
  public static final int MAX_LENGTH_TELEPHONE  = 10;
  /**
   * Maximum string length for <code>employee.ssn</code>.
   */
  public static final int MAX_LENGTH_SSN        = 9;



  //////////////////////////////////////////////////////////////////////////////
  // INSTANCE VARIABLES                                                       //
  //////////////////////////////////////////////////////////////////////////////

  private int    id_;        // Auto-generated employee identifier.
  private String lastName_;  // Family name.
  private String firstName_; // Given name.
  private String address_;   // Mailing address.
  private String city_;      // City of residence.
  private String state_;     // State of residence.
  private String zipCode_;   // Mailing zip code.
  private String telephone_; // Phone number.
  private String ssn_;       // Social security number.
  private Date   hireDate_;  // Date of hire.
  private double wage_;      // Hourly pay rate.
  private int    managerID_; // Manager identifier.



  //////////////////////////////////////////////////////////////////////////////
  // CONSTRUCTORS                                                             //
  //////////////////////////////////////////////////////////////////////////////

  /**
   * Creates an object of this class with all instance variables initialized.
   * <p>
   * This constructor is used by <code>DBMapper.getEmployee(int)</code> and
   * should not be called directly by frontend code.
   *
   * @param id        the unique employee identifier
   * @param lastName  the family name
   * @param firstName the given name
   * @param address   the mailing address
   * @param city      the city of residence
   * @param state     the state of residence
   * @param zipCode   the mailing zip code
   * @param telephone the phone number
   * @param ssn       the social security number
   * @param hireDate  the date of hire
   * @param wage      the hourly pay rate
   * @param managerID the manager identifier
   */
  public Employee(int    id,
                  String lastName,
                  String firstName,
                  String address,
                  String city,
                  String state,
                  String zipCode,
                  String telephone,
                  String ssn,
                  Date   hireDate,
                  double wage,
                  int    managerID)
  {
    id_        = id;
    lastName_  = lastName;
    firstName_ = firstName;
    address_   = address;
    city_      = city;
    state_     = state;
    zipCode_   = zipCode;
    telephone_ = telephone;
    ssn_       = ssn;
    hireDate_  = new Date(hireDate.getTime());
    wage_      = wage;
    managerID_ = managerID;
  }

  /**
   * Creates an object of this class, initializing only those instance variables
   * necessary to add a new tuple describing a non-manager employee to the
   * <code>employee</code> relation.
   * <p>
   * This constructor should be called by frontend code when instantiating an
   * object to pass to <code>DBInserter.insertEmployee(Employee)</code>.
   *
   * @param lastName  the family name
   * @param firstName the given name
   * @param address   the mailing address
   * @param city      the city of residence
   * @param state     the state of residence
   * @param zipCode   the mailing zip code
   * @param telephone the phone number
   * @param ssn       the social security number
   * @param wage      the hourly pay rate
   * @param managerID the manager identifier
   */
  public Employee(String lastName,
                  String firstName,
                  String address,
                  String city,
                  String state,
                  String zipCode,
                  String telephone,
                  String ssn,
                  double wage,
                  int    managerID)
  {
    id_        = 0;         // Default database value.
    lastName_  = lastName;
    firstName_ = firstName;
    address_   = address;
    city_      = city;
    state_     = state;
    zipCode_   = zipCode;
    telephone_ = telephone;
    ssn_       = ssn;
    hireDate_  = null;      // Default database value.
    wage_      = wage;
    managerID_ = managerID;
  }

  /**
   * Creates an object of this class, initializing only those instance variables
   * necessary to add a new tuple describing a manager employee to the
   * <code>employee</code> relation.
   * <p>
   * This constructor should be called by frontend code when instantiating an
   * object to pass to <code>DBInserter.insertEmployee(Employee)</code>.
   *
   * @param lastName  the family name
   * @param firstName the given name
   * @param address   the mailing address
   * @param city      the city of residence
   * @param state     the state of residence
   * @param zipCode   the mailing zip code
   * @param telephone the phone number
   * @param ssn       the social security number
   * @param wage      the hourly pay rate
   */
  public Employee(String lastName,
                  String firstName,
                  String address,
                  String city,
                  String state,
                  String zipCode,
                  String telephone,
                  String ssn,
                  double wage)
  {
    id_        = 0;         // Default database value.
    lastName_  = lastName;
    firstName_ = firstName;
    address_   = address;
    city_      = city;
    state_     = state;
    zipCode_   = zipCode;
    telephone_ = telephone;
    ssn_       = ssn;
    hireDate_  = null;      // Default database value.
    wage_      = wage;
    managerID_ = 0;         // Default database value.
  }

  /**
   * Creates a deep copy of an object of this class.
   *
   * @param other the object whose state should be copied
   */
  public Employee(Employee other)
  {
    this.id_        = other.id_;
    this.lastName_  = other.lastName_;
    this.firstName_ = other.firstName_;
    this.address_   = other.address_;
    this.city_      = other.city_;
    this.state_     = other.state_;
    this.zipCode_   = other.zipCode_;
    this.telephone_ = other.telephone_;
    this.ssn_       = other.ssn_;
    this.wage_      = other.wage_;
    this.managerID_ = other.managerID_;

    // hireDate_ can be null, so it requires special handling.
    if (other.hireDate_ != null)
      this.hireDate_ = new Date(other.hireDate_.getTime());
    else
      this.hireDate_ = null;
  }



  //////////////////////////////////////////////////////////////////////////////
  // METHODS                                                                  //
  //////////////////////////////////////////////////////////////////////////////

  /**
   * Returns the employee number auto-generated by the database.
   *
   * @return the employee identifier
   */
  public int getID()
  {
    return id_;
  }

  /**
   * Returns the employee's legal surname.
   *
   * @return the family name
   */
  public String getLastName()
  {
    return lastName_;
  }

  /**
   * Returns the employee's legal forename.
   *
   * @return the given name
   */
  public String getFirstName()
  {
    return firstName_;
  }

  /**
   * Returns the USPS mailing address.
   *
   * @return the mailing address
   */
  public String getAddress()
  {
    return address_;
  }

  /**
   * Returns the city's name.
   *
   * @return the city of residence
   */
  public String getCity()
  {
    return city_;
  }

  /**
   * Returns the state's ISO 3166-2:US code.
   *
   * @return the state of residence
   */
  public String getState()
  {
    return state_;
  }

  /**
   * Returns the USPS postal code.
   *
   * @return the mailing zip code
   */
  public String getZipCode()
  {
    return zipCode_;
  }

  /**
   * Returns the NANP telephone number.
   *
   * @return the phone number
   */
  public String getTelephone()
  {
    return telephone_;
  }

  /**
   * Returns the number assigned by the US Social Security Administration.
   *
   * @return the social security number
   */
  public String getSSN()
  {
    return ssn_;
  }

  /**
   * Returns a <code>java.sql.Date</code> object that maps the hire date's ISO
   * 8601 (yyyy-mm-dd) representation.
   *
   * @return the date of hire
   */
  public Date getHireDate()
  {
    // hireDate_ can be null, so it requires special handling.
    if (hireDate_ != null)
      return new Date(hireDate_.getTime());
    else
      return null;
  }

  /**
   * Returns the USD per-hour pay amount.
   *
   * @return the hourly pay rate
   */
  public double getWage()
  {
    return wage_;
  }

  /**
   * Returns the employee number belonging to this employee's manager.
   *
   * @return the manager identifier if this employee has a manager;
   *         <code>0</code> otherwise
   */
  public int getManagerID()
  {
    return managerID_;
  }



  /**
   * Sets the employee's family name.
   *
   * @param lastName the legal surname
   */
  public void setLastName(String lastName)
  {
    lastName_ = lastName;
  }

  /**
   * Sets the employee's given name.
   *
   * @param firstName the legal forename
   */
  public void setFirstName(String firstName)
  {
    firstName_ = firstName;
  }

  /**
   * Sets the mailing address.
   *
   * @param address the USPS mailing address
   */
  public void setAddress(String address)
  {
    address_ = address;
  }

  /**
   * Sets the city of residence.
   *
   * @param city the city's name
   */
  public void setCity(String city)
  {
    city_ = city;
  }

  /**
   * Sets the state of residence.
   *
   * @param state the state's ISO 3166-2:US code
   */
  public void setState(String state)
  {
    state_ = state;
  }

  /**
   * Sets the mailing zip code.
   *
   * @param zipCode the USPS postal code
   */
  public void setZipCode(String zipCode)
  {
    zipCode_ = zipCode;
  }

  /**
   * Sets the phone number.
   *
   * @param telephone the NANP telephone number
   */
  public void setTelephone(String telephone)
  {
    telephone_ = telephone;
  }

  /**
   * Sets the hourly pay rate.
   *
   * @param wage the USD per-hour pay amount
   */
  public void setWage(double wage)
  {
    wage_ = wage;
  }

  /**
   * Sets the manager identifier.
   *
   * @param managerID the employee number belonging to this employee's manager
   */
  public void setManagerID(int managerID)
  {
    managerID_ = managerID;
  }



  /**
   * Peforms an equality comparison on two objects of this class.
   *
   * @param other the object whose state should be compared with the state of
   *              this object
   * @return      <code>true</code> if the object states are equal;
   *              <code>false</code> otherwise
   */
  public boolean equals(Employee other)
  {
    boolean equalsID        = this.id_ == other.id_;
    boolean equalsLastName  = this.lastName_.equals(other.lastName_);
    boolean equalsFirstName = this.firstName_.equals(other.firstName_);
    boolean equalsAddress   = this.address_.equals(other.address_);
    boolean equalsCity      = this.city_.equals(other.city_);
    boolean equalsState     = this.state_.equals(other.state_);
    boolean equalsZipCode   = this.zipCode_.equals(other.zipCode_);
    boolean equalsTelephone = this.telephone_.equals(other.telephone_);
    boolean equalsSSN       = this.ssn_.equals(other.ssn_);
    boolean equalsWage      = this.wage_ == other.wage_;
    boolean equalsManagerID = this.managerID_ == other.managerID_;

    // hireDate_ can be null, so it requires special handling.
    boolean equalsHireDate = false;
    if (this.hireDate_ != null && other.hireDate_ != null)
      equalsHireDate = this.hireDate_.equals(other.hireDate_);
    else if (this.hireDate_ == null)
      equalsHireDate = other.hireDate_ == null;
    else if (other.hireDate_ == null)
      equalsHireDate = this.hireDate_ == null;

    if (equalsID
        && equalsLastName
        && equalsFirstName
        && equalsAddress
        && equalsCity
        && equalsState
        && equalsZipCode
        && equalsTelephone
        && equalsSSN
        && equalsHireDate
        && equalsWage
        && equalsManagerID)
      return true;
    else
      return false;
  }

  /**
   * Returns a string representation of this object.
   *
   * @return a string describing the state of this object
   */
  public String toString()
  {
    return String.format(FORMAT_ID, id_)
         + "   "
         + String.format(FORMAT_LAST_NAME, lastName_)
         + "   "
         + String.format(FORMAT_FIRST_NAME, firstName_)
         + "   "
         + String.format(FORMAT_ADDRESS, address_)
         + "   "
         + String.format(FORMAT_CITY, city_)
         + "   "
         + String.format(FORMAT_STATE, state_)
         + "   "
         + String.format(FORMAT_ZIP_CODE, zipCode_)
         + "   "
         + String.format(FORMAT_TELEPHONE, telephone_)
         + "   "
         + String.format(FORMAT_SSN, ssn_)
         + "   "
         + String.format(FORMAT_HIRE_DATE, hireDate_)
         + "   "
         + String.format(FORMAT_WAGE, wage_)
         + "   "
         + String.format(FORMAT_ID, managerID_);
  }
}
