////////////////////////////////////////////////////////////////////////////////
// Bryan Burke, Joshua Lambert, and Elizabeth Rothermel                       //
// Group 4                                                                    //
// INFX 499                                                                   //
// Fall 2015                                                                  //
// University of Louisiana at Lafayette                                       //
////////////////////////////////////////////////////////////////////////////////

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Performs rudimentary unit testing on <code>DBConnector</code> methods.
 *
 * @author Bryan Burke
 */
public class TestDBConnector
{
  //////////////////////////////////////////////////////////////////////////////
  // METHODS                                                                  //
  //////////////////////////////////////////////////////////////////////////////

  /**
   * Calls <code>DBConnector</code> methods to establish a connection to the
   * database.
   *
   * @param args command-line arguments; unused
   */
  public static void main(String[] args) throws SQLException
  {
    ////////////////////////////////////////////////////////////////////////////
    // TEST METHODS                                                           //
    ////////////////////////////////////////////////////////////////////////////

    System.out.println("== BEGIN METHOD TESTS ==");

    // connect()
    System.out.println("= connect() =");
    if (DBConnector.connect())
      System.out.println("Connection established successfully.");
    else {
      System.out.println(DBConnector.error);

      System.exit(1);
    }

    // createStatement(boolean)
    System.out.println("= createStatement(true) =");
    Statement statement1 = DBConnector.createStatement(true);
    if (statement1 != null) {
      System.out.println("Statement supporting scrollable results created "
                       + "successfully.");
      statement1.close();
    }
    else
      System.out.println(DBConnector.error);

    System.out.println("= createStatement(false) =");
    Statement statement2 = DBConnector.createStatement(false);
    if (statement2 != null) {
      System.out.println("Statement not supporting scrollable results created "
                       + "successfully.");
      statement2.close();
    }
    else
      System.out.println(DBConnector.error);

    // createPreparedStatement(String)
    System.out.println("= createPreparedStatement(\"SELECT * FROM flight\") =");
    Statement statement3 = DBConnector.createPreparedStatement("SELECT *"
                                                             + "  FROM flight");
    if (statement3 != null) {
      System.out.println("PreparedStatement created successfully.");
      statement3.close();
    }
    else
      System.out.println(DBConnector.error);

    // enableTransactions()
    System.out.println("= enableTransactions() =");
    if (DBConnector.enableTransactions())
      System.out.println("Transactions enabled successfully.");
    else
      System.out.println(DBConnector.error);

    // disableTransactions()
    System.out.println("= disableTransactions() =");
    if (DBConnector.disableTransactions())
      System.out.println("Transactions disabled successfully.");
    else
      System.out.println(DBConnector.error);

    // disconnect()
    System.out.println("= disconnect() =");
    DBConnector.disconnect();

    System.out.println("== END METHOD TESTS ==");
  }
}
