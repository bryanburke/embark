////////////////////////////////////////////////////////////////////////////////
// Bryan Burke, Joshua Lambert, and Elizabeth Rothermel                       //
// Group 4                                                                    //
// INFX 499                                                                   //
// Fall 2015                                                                  //
// University of Louisiana at Lafayette                                       //
////////////////////////////////////////////////////////////////////////////////

import java.io.IOException;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

/**
 * Performs rudimentary unit testing on <code>Bid</code> objects.
 *
 * @author Bryan Burke
 */
public class TestBid
{
  //////////////////////////////////////////////////////////////////////////////
  // METHODS                                                                  //
  //////////////////////////////////////////////////////////////////////////////

  /**
   * Creates and tests <code>Bid</code> objects.
   *
   * @param args command-line arguments; unused
   */
  public static void main(String[] args) throws IOException, ParseException
  {
    ////////////////////////////////////////////////////////////////////////////
    // TEST CONSTRUCTORS                                                      //
    ////////////////////////////////////////////////////////////////////////////

    System.out.println("== BEGIN CONSTRUCTOR TESTS ==");

    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

    // Prerequisite objects.
    Employee    sigdelP = new Employee(2,
                                       "Sigdel",
                                       "Purushottam",
                                       "304 Kenwood Place",
                                       "Lafayette",
                                       "LA",
                                       "70504",
                                       "3375833335",
                                       "26560XXXX",
                             new Date(dateFormat.parse("2008-04-19").getTime()),
                                       20.0,
                                       1);
    Reservation r1      = new Reservation(1,
                             new Date(dateFormat.parse("2009-07-19").getTime()),
                                          DBEncoder.FLIGHT_ONE_WAY,
                                          DBEncoder.CLASS_ECONOMY,
                                          101.0,
                                          10.1,
                                          sigdelP);
    Customer    patelK  = new Customer(5,
                                       "Patel",
                                       "Kimberly",
                                       "3548 Orchard Street",
                                       "Osseo",
                                       "MN",
                                       "55369",
                                       "9528552611",
                                       2);
    Account     a5      = new Account(5,
                                      patelK,
                                      "patel.kimberly@locationcalculator.com",
                                      "542802306410XXXX",
                            new Date(dateFormat.parse("2009-07-19").getTime()));

    ArrayList<Bid> bids = new ArrayList<>(3);

    // Bid(Reservation, Account, double)
    bids.add(new Bid(r1,
                     a5,
                     84.0));
    System.out.println("  $84.00 bid for reservation #1 and account #5 "
                     + "constructed as bids[0].");

    bids.add(new Bid(r1,
                     a5,
                     87.0));
    System.out.println("  New bid constructed as bids[1].");

    // Bid(Bid)
    bids.add(new Bid(bids.get(0)));
    System.out.println("  $84.00 bid for reservation #1 and account #5 copy "
                     + "constructed as bids[2].");

    System.out.println("== END CONSTRUCTOR TESTS ==");
    System.out.println();



    ////////////////////////////////////////////////////////////////////////////
    // TEST METHODS                                                           //
    ////////////////////////////////////////////////////////////////////////////

    System.out.println("== BEGIN METHOD TESTS ==");

    // getReservation()
    System.out.println("= getReservation() =");
    for (int i = 0; i < bids.size(); ++i)
      System.out.printf("  [%d] : %s\n",
                        i,
                        bids.get(i).getReservation());
    System.out.print("Verify the results and press ENTER to continue.");
    System.in.read();

    // getAccount()
    System.out.println("= getAccount() =");
    for (int i = 0; i < bids.size(); ++i)
      System.out.printf("  [%d] : %s\n",
                        i,
                        bids.get(i).getAccount());
    System.out.print("Verify the results and press ENTER to continue.");
    System.in.read();

    // getAmount()
    System.out.println("= getAmount() =");
    for (int i = 0; i < bids.size(); ++i)
      System.out.printf("  [%d] : %.2f\n",
                        i,
                        bids.get(i).getAmount());
    System.out.print("Verify the results and press ENTER to continue.");
    System.in.read();

    // equals(Bid)
    System.out.println("= equals([0]) =");
    for (int i = 0; i < bids.size(); ++i)
      System.out.printf("  [%d] : %s\n",
                        i,
                        bids.get(i).equals(bids.get(0)));
    System.out.print("Verify the results and press ENTER to continue.");
    System.in.read();

    // toString()
    System.out.println("= toString() =");
    for (int i = 0; i < bids.size(); ++i)
      System.out.printf("  [%d] : %s\n",
                        i,
                        bids.get(i).toString());
    System.out.print("Verify the results and press ENTER to continue.");
    System.in.read();

    System.out.println("== END METHOD TESTS ==");
  }
}
