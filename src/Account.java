////////////////////////////////////////////////////////////////////////////////
// Bryan Burke, Joshua Lambert, and Elizabeth Rothermel                       //
// Group 4                                                                    //
// INFX 499                                                                   //
// Fall 2015                                                                  //
// University of Louisiana at Lafayette                                       //
////////////////////////////////////////////////////////////////////////////////

import java.sql.Date;

/**
 * Provides an in-memory mapping of a tuple from the <code>account</code>
 * database relation.
 *
 * @author Bryan Burke
 */
public class Account
{
  //////////////////////////////////////////////////////////////////////////////
  // CLASS VARIABLES                                                          //
  //////////////////////////////////////////////////////////////////////////////

  /**
   * Format specifier for <code>account.account_id</code>.
   */
  public static final String FORMAT_ID            = "%8d";
  /**
   * Format specifier for <code>account.email</code>.
   */
  public static final String FORMAT_EMAIL         = "%50.50s";
  /**
   * Format specifier for <code>account.credit_card</code>.
   */
  public static final String FORMAT_CREDIT_CARD   = "%16.16s";
  /**
   * Format specifier for <code>account.creation_date</code>.
   */
  public static final String FORMAT_CREATION_DATE = "%10.10s";



  /**
   * Maximum string length for <code>account.email</code>.
   */
  public static final int MAX_LENGTH_EMAIL       = 255;
  /**
   * Maximum string length for <code>account.credit_card</code>.
   */
  public static final int MAX_LENGTH_CREDIT_CARD = 16;



  //////////////////////////////////////////////////////////////////////////////
  // INSTANCE VARIABLES                                                       //
  //////////////////////////////////////////////////////////////////////////////

  private int      id_;           // Auto-generated account identifier.
  private Customer customer_;     // Associated customer.
  private String   email_;        // Unique account email address.
  private String   creditCard_;   // Credit card number.
  private Date     creationDate_; // Date of account creation.



  //////////////////////////////////////////////////////////////////////////////
  // CONSTRUCTORS                                                             //
  //////////////////////////////////////////////////////////////////////////////

  /**
   * Creates an object of this class with all instance variables initialized.
   * <p>
   * This constructor is used by <code>DBMapper.getAccount(int)</code> and
   * should not be called directly by frontend code.
   *
   * @param id           the unique account identifier
   * @param customer     the associated customer
   * @param email        the unique account email address
   * @param creditCard   the credit card number
   * @param creationDate the date of account creation
   */
  public Account(int      id,
                 Customer customer,
                 String   email,
                 String   creditCard,
                 Date     creationDate)
  {
    id_           = id;
    customer_     = new Customer(customer);
    email_        = email;
    creditCard_   = creditCard;
    creationDate_ = new Date(creationDate.getTime());
  }

  /**
   * Creates an object of this class, initializing only those instance variables
   * necessary to add a new tuple to the <code>account</code> relation.
   * <p>
   * This constructor should be called by frontend code when instantiating an
   * object to pass to <code>DBInserter.insertAccount(Account)</code>.
   *
   * @param customer     the associated customer
   * @param email        the unique account email address
   * @param creditCard   the credit card number
   */
  public Account(Customer customer,
                 String   email,
                 String   creditCard)
  {
    id_           = 0;    // Default database value.
    customer_     = new Customer(customer);
    email_        = email;
    creditCard_   = creditCard;
    creationDate_ = null; // Default database value.
  }

  /**
   * Creates a deep copy of an object of this class.
   *
   * @param other the object whose state should be copied
   */
  public Account(Account other)
  {
    this.id_         = other.id_;
    this.customer_   = new Customer(other.customer_);
    this.email_      = other.email_;
    this.creditCard_ = other.creditCard_;

    // creationDate_ can be null, so it requires special handling.
    if (other.creationDate_ != null)
      this.creationDate_ = new Date(other.creationDate_.getTime());
    else
      this.creationDate_ = null;
  }



  //////////////////////////////////////////////////////////////////////////////
  // METHODS                                                                  //
  //////////////////////////////////////////////////////////////////////////////

  /**
   * Returns the account number auto-generated by the database.
   *
   * @return the account identifier
   */
  public int getID()
  {
    return id_;
  }

  /**
   * Returns a <code>Customer</code> object that maps the associated customer's
   * relational data.
   *
   * @return the associated customer
   */
  public Customer getCustomer()
  {
    return new Customer(customer_);
  }

  /**
   * Returns the RFC 5321-compliant email address.
   *
   * @return the account email address
   */
  public String getEmail()
  {
    return email_;
  }

  /**
   * Returns the 13- or 16-digit payment card number.
   *
   * @return the credit card number
   */
  public String getCreditCard()
  {
    return creditCard_;
  }

  /**
   * Returns a <code>java.sql.Date</code> object that maps the account creation
   * date's ISO 8601 (yyyy-mm-dd) representation.
   *
   * @return the date of account creation
   */
  public Date getCreationDate()
  {
    // creationDate_ can be null, so it requires special handling.
    if (creationDate_ != null)
      return new Date(creationDate_.getTime());
    else
      return null;
  }



  /**
   * Sets the account email address.
   *
   * @param email the RFC 5321-compliant email address
   */
  public void setEmail(String email)
  {
    email_ = email;
  }

  /**
   * Sets the credit card number.
   *
   * @param creditCard the 13- or 16-digit payment card number
   */
  public void setCreditCard(String creditCard)
  {
    creditCard_ = creditCard;
  }



  /**
   * Peforms an equality comparison on two objects of this class.
   *
   * @param other the object whose state should be compared with the state of
   *              this object
   * @return      <code>true</code> if the object states are equal;
   *              <code>false</code> otherwise
   */
  public boolean equals(Account other)
  {
    boolean equalsID         = this.id_ == other.id_;
    boolean equalsCustomer   = this.customer_.equals(other.customer_);
    boolean equalsEmail      = this.email_.equals(other.email_);
    boolean equalsCreditCard = this.creditCard_.equals(other.creditCard_);

    // creationDate_ can be null, so it requires special handling.
    boolean equalsCreationDate = false;
    if (this.creationDate_ != null && other.creationDate_ != null)
      equalsCreationDate = this.creationDate_.equals(other.creationDate_);
    else if (this.creationDate_ == null)
      equalsCreationDate = other.creationDate_ == null;
    else if (other.creationDate_ == null)
      equalsCreationDate = this.creationDate_ == null;

    if (equalsID
        && equalsCustomer
        && equalsEmail
        && equalsCreditCard
        && equalsCreationDate)
      return true;
    else
      return false;
  }

  /**
   * Returns a string representation of this object.
   *
   * @return a string describing the state of this object
   */
  public String toString()
  {
    return String.format(FORMAT_ID, id_)
         + "   "
         + String.format(Customer.FORMAT_ID, customer_.getID())
         + "   "
         + String.format(FORMAT_EMAIL, email_)
         + "   "
         + String.format(FORMAT_CREDIT_CARD, creditCard_)
         + "   "
         + String.format(FORMAT_CREATION_DATE, creationDate_);
  }
}
