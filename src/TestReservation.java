////////////////////////////////////////////////////////////////////////////////
// Bryan Burke, Joshua Lambert, and Elizabeth Rothermel                       //
// Group 4                                                                    //
// INFX 499                                                                   //
// Fall 2015                                                                  //
// University of Louisiana at Lafayette                                       //
////////////////////////////////////////////////////////////////////////////////

import java.io.IOException;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

/**
 * Performs rudimentary unit testing on <code>Reservation</code> objects.
 *
 * @author Bryan Burke
 */
public class TestReservation
{
  //////////////////////////////////////////////////////////////////////////////
  // METHODS                                                                  //
  //////////////////////////////////////////////////////////////////////////////

  /**
   * Creates and tests <code>Reservation</code> objects.
   *
   * @param args command-line arguments; unused
   */
  public static void main(String[] args) throws IOException, ParseException
  {
    ////////////////////////////////////////////////////////////////////////////
    // TEST CONSTRUCTORS                                                      //
    ////////////////////////////////////////////////////////////////////////////

    System.out.println("== BEGIN CONSTRUCTOR TESTS ==");

    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

    // Prerequisite objects.
    Employee sigdelP   = new Employee(2,
                                      "Sigdel",
                                      "Purushottam",
                                      "304 Kenwood Place",
                                      "Lafayette",
                                      "LA",
                                      "70504",
                                      "3375833335",
                                      "26560XXXX",
                             new Date(dateFormat.parse("2008-04-19").getTime()),
                                      20.0,
                                      1);
    Employee thomasonR = new Employee(3,
                                      "Thomason",
                                      "Ruth",
                                      "1842 Lowland Drive",
                                      "Scott",
                                      "LA",
                                      "70583",
                                      "3374093644",
                                      "33904XXXX",
                             new Date(dateFormat.parse("2015-11-17").getTime()),
                                      10.0,
                                      1);

    ArrayList<Reservation> reservations = new ArrayList<>(3);

    // Reservation(int, Date, int, int, double, double, Employee)
    reservations.add(new Reservation(1,
                             new Date(dateFormat.parse("2009-07-19").getTime()),
                                     DBEncoder.FLIGHT_ONE_WAY,
                                     DBEncoder.CLASS_ECONOMY,
                                     101.0,
                                     10.1,
                                     sigdelP));
    System.out.println("  Reservation #1 constructed as reservations[0].");

    // Reservation(int, int, double, Employee)
    reservations.add(new Reservation(DBEncoder.FLIGHT_MULTI_CITY,
                                     DBEncoder.CLASS_FIRST,
                                     706.2,
                                     thomasonR));
    System.out.println("  New reservation constructed as reservations[1].");

    // Reservation(Reservation)
    reservations.add(new Reservation(reservations.get(0)));
    System.out.println("  Reservation #1 copy constructed as reservations[2].");

    System.out.println("== END CONSTRUCTOR TESTS ==");
    System.out.println();



    ////////////////////////////////////////////////////////////////////////////
    // TEST METHODS                                                           //
    ////////////////////////////////////////////////////////////////////////////

    System.out.println("== BEGIN METHOD TESTS ==");

    // getID()
    System.out.println("= getID() =");
    for (int i = 0; i < reservations.size(); ++i)
      System.out.printf("  [%d] : %d\n",
                        i,
                        reservations.get(i).getID());
    System.out.print("Verify the results and press ENTER to continue.");
    System.in.read();

    // getBookingDate()
    System.out.println("= getBookingDate() =");
    for (int i = 0; i < reservations.size(); ++i)
      System.out.printf("  [%d] : %s\n",
                        i,
                        reservations.get(i).getBookingDate());
    System.out.print("Verify the results and press ENTER to continue.");
    System.in.read();

    // getFlightType()
    System.out.println("= getFlightType() =");
    for (int i = 0; i < reservations.size(); ++i)
      System.out.printf("  [%d] : %d\n",
                        i,
                        reservations.get(i).getFlightType());
    System.out.print("Verify the results and press ENTER to continue.");
    System.in.read();

    // getFlightClass()
    System.out.println("= getFlightClass() =");
    for (int i = 0; i < reservations.size(); ++i)
      System.out.printf("  [%d] : %d\n",
                        i,
                        reservations.get(i).getFlightClass());
    System.out.print("Verify the results and press ENTER to continue.");
    System.in.read();

    // getPrice()
    System.out.println("= getPrice() =");
    for (int i = 0; i < reservations.size(); ++i)
      System.out.printf("  [%d] : %.2f\n",
                        i,
                        reservations.get(i).getPrice());
    System.out.print("Verify the results and press ENTER to continue.");
    System.in.read();

    // getFee()
    System.out.println("= getFee() =");
    for (int i = 0; i < reservations.size(); ++i)
      System.out.printf("  [%d] : %.2f\n",
                        i,
                        reservations.get(i).getFee());
    System.out.print("Verify the results and press ENTER to continue.");
    System.in.read();

    // getEmployee()
    System.out.println("= getEmployee() =");
    for (int i = 0; i < reservations.size(); ++i)
      System.out.printf("  [%d] : %s\n",
                        i,
                        reservations.get(i).getEmployee());
    System.out.print("Verify the results and press ENTER to continue.");
    System.in.read();

    // equals(Reservation)
    System.out.println("= equals([0]) =");
    for (int i = 0; i < reservations.size(); ++i)
      System.out.printf("  [%d] : %s\n",
                        i,
                        reservations.get(i).equals(reservations.get(0)));
    System.out.print("Verify the results and press ENTER to continue.");
    System.in.read();

    // toString()
    System.out.println("= toString() =");
    for (int i = 0; i < reservations.size(); ++i)
      System.out.printf("  [%d] : %s\n",
                        i,
                        reservations.get(i).toString());
    System.out.print("Verify the results and press ENTER to continue.");
    System.in.read();

    System.out.println("== END METHOD TESTS ==");
  }
}
