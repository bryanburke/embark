////////////////////////////////////////////////////////////////////////////////
// Bryan Burke, Joshua Lambert, and Elizabeth Rothermel                       //
// Group 4                                                                    //
// INFX 499                                                                   //
// Fall 2015                                                                  //
// University of Louisiana at Lafayette                                       //
////////////////////////////////////////////////////////////////////////////////

import java.io.IOException;
import java.util.ArrayList;

/**
 * Performs rudimentary unit testing on <code>Airline</code> objects.
 *
 * @author Bryan Burke
 */
public class TestAirline
{
  //////////////////////////////////////////////////////////////////////////////
  // METHODS                                                                  //
  //////////////////////////////////////////////////////////////////////////////

  /**
   * Creates and tests <code>Airline</code> objects.
   *
   * @param args command-line arguments; unused
   */
  public static void main(String[] args) throws IOException
  {
    ////////////////////////////////////////////////////////////////////////////
    // TEST CONSTRUCTORS                                                      //
    ////////////////////////////////////////////////////////////////////////////

    System.out.println("== BEGIN CONSTRUCTOR TESTS ==");

    ArrayList<Airline> airlines = new ArrayList<>(3);

    // Airline(String, String)
    airlines.add(new Airline("AA",
                             "American Airlines"));
    System.out.println("  Airline AA constructed as airlines[0].");

    airlines.add(new Airline("UA",
                             "United Airlines"));
    System.out.println("  Airline UA constructed as airlines[1].");

    // Airline(Airline)
    airlines.add(new Airline(airlines.get(0)));
    System.out.println("  Airline AA copy constructed as airlines[2].");

    System.out.println("== END CONSTRUCTOR TESTS ==");
    System.out.println();



    ////////////////////////////////////////////////////////////////////////////
    // TEST METHODS                                                           //
    ////////////////////////////////////////////////////////////////////////////

    System.out.println("== BEGIN METHOD TESTS ==");

    // getID()
    System.out.println("= getID() =");
    for (int i = 0; i < airlines.size(); ++i)
      System.out.printf("  [%d] : %s\n",
                        i,
                        airlines.get(i).getID());
    System.out.print("Verify the results and press ENTER to continue.");
    System.in.read();

    // getName()
    System.out.println("= getName() =");
    for (int i = 0; i < airlines.size(); ++i)
      System.out.printf("  [%d] : %s\n",
                        i,
                        airlines.get(i).getName());
    System.out.print("Verify the results and press ENTER to continue.");
    System.in.read();

    // equals(Airline)
    System.out.println("= equals([0]) =");
    for (int i = 0; i < airlines.size(); ++i)
      System.out.printf("  [%d] : %s\n",
                        i,
                        airlines.get(i).equals(airlines.get(0)));
    System.out.print("Verify the results and press ENTER to continue.");
    System.in.read();

    // toString()
    System.out.println("= toString() =");
    for (int i = 0; i < airlines.size(); ++i)
      System.out.printf("  [%d] : %s\n",
                        i,
                        airlines.get(i).toString());
    System.out.print("Verify the results and press ENTER to continue.");
    System.in.read();

    System.out.println("== END METHOD TESTS ==");
  }
}
