--------------------------------------------------------------------------------
-- Bryan Burke, Joshua Lambert, and Elizabeth Rothermel                       --
-- Group 4                                                                    --
-- INFX 499                                                                   --
-- Fall 2015                                                                  --
-- University of Louisiana at Lafayette                                       --
--------------------------------------------------------------------------------

CONNECT 'jdbc:derby:db;create=true';



/*
 * CONSTRAINTS
 *
 * pk_airline                 primary key for airline
 * pk_airport                 primary key for airport
 * pk_flight                  primary key for flight
 * pk_route                   primary key for route
 * pk_fare_restriction        primary key for fare_restriction
 * pk_customer                primary key for customer
 * pk_preference              primary key for preference
 * pk_account                 primary key for account
 * pk_employee                primary key for employee
 * pk_reservation             primary key for reservation
 * pk_bid                     primary key for bid
 * pk_booked                  primary key for booked
 * fk_airline1flight          primary key for airline as foreign key in flight
 * fk_airport1flight          primary key for airport as foreign key in flight
 *                            (1)
 * fk_airport2flight          primary key for airport as foreign key in flight
 *                            (2)
 * fk_flight1route            primary key for flight as foreign key in route (1)
 * fk_flight2route            primary key for flight as foreign key in route (2)
 * fk_flight1fare_restriction primary key for flight as foreign key in
 *                            fare_restriction
 * fk_customer1preference     primary key for customer as foreign key in
 *                            preference
 * fk_customer1account        primary key for customer as foreign key in account
 * fk_employee1employee       primary key for employee as foreign key in
 *                            employee
 * fk_employee1reservation    primary key for employee as foreign key in
 *                            reservation
 * fk_reservation1bid         primary key for reservation as foreign key in bid
 * fk_account1bid             primary key for account as foreign key in bid
 * fk_reservation1booked      primary key for reservation as foreign key in
 *                            booked
 * fk_account1booked          primary key for account as foreign key in booked
 * fk_flight1booked           primary key for flight as foreign key in booked
 * un_customer                unique non-key attributes for customer
 * un_account                 unique non-key attribute for account
 * un_employee                unique non-key attribute for employee
 * ck_capacity_rg             range check for flight.capacity
 * ck_fare_rg                 range check for flight.fare
 * ck_airports_ne             inequality check for airport references in flight
 * ck_leg_order_rg            range check for route.leg_order
 * ck_flight_ne               inequality check for flight references in route
 * ck_restriction_type_rg     range check for fare_restriction.restriction_type
 * ck_advance_days_rg         range check for fare_restriction.advance_days
 * ck_minimum_stay_rg         range check for fare_restriction.minimum_stay
 * ck_maximum_stay_rg         range check for fare_restriction.maximum_stay
 * ck_rating_rg               range check for customer.rating
 * ck_preference_type_rg      range check for preference.preference_type
 * ck_seat_choice_rg          range check for preference.seat_choice
 * ck_meal_choice_rg          range check for preference.meal_choice
 * ck_email_ft                format check for account.email
 * ck_wage_rg                 range check for employee.wage
 * ck_flight_type_rg          range check for reservation.flight_type
 * ck_flight_class_rg         range check for reservation.flight_class
 * ck_price_rg                range check for reservation.price
 * ck_bid_amount_rg           range check for bid.bid_amount
 * ck_flight_order_rg         range check for booked.flight_order
 * ck_seat_number_rg          range check for booked.seat_number
 * ck_special_meal_rg         range check for booked.special_meal
 */



/*
 * REFERENTIAL INTEGRITY RULES
 *
 * fk_airline1flight          restrict on delete, restrict on update
 * fk_airport1flight          restrict on delete, restrict on update
 * fk_airport2flight          restrict on delete, restrict on update
 * fk_flight1route            restrict on delete, restrict on update
 * fk_flight2route            restrict on delete, restrict on update
 * fk_flight1fare_restriction restrict on delete, restrict on update
 * fk_customer1preference     cascade on delete, restrict on update
 * fk_customer1account        restrict on delete, restrict on update
 * fk_employee1employee       restrict on delete, restrict on update
 * fk_employee1reservation    restrict on delete, restrict on update
 * fk_reservation1bid         cascade on delete, restrict on update
 * fk_account1bid             cascade on delete, restrict on update
 * fk_reservation1booked      cascade on delete, restrict on update
 * fk_account1booked          restrict on delete, restrict on update
 * fk_flight1booked           restrict on delete, restrict on update
 */



/*
 * DEFAULT VALUES
 *
 * flight.capacity               NULL
 * fare_restriction.advance_days NULL
 * fare_restriction.minimum_stay NULL
 * fare_restriction.maximum_stay NULL
 * customer.rating               0
 * preference.seat_choice        NULL
 * preference.meal_choice        NULL
 * account.creation_date         CURRENT_DATE
 * employee.hire_date            CURRENT_DATE
 * employee.manager_id           NULL
 * reservation.booking_date      CURRENT_DATE
 * booked.special_meal           0
 */



/*
 * GENERATED VALUES
 *
 * flight.reserve_fare        90% of flight.fare
 * customer.customer_id       auto-incremented
 * account.account_id         auto-incremented
 * employee.employee_id       auto-incremented
 * reservation.reservation_id auto-incremented
 * reservation.fee            10% of reservation.price
 */



CREATE TABLE airline (
  CONSTRAINT pk_airline
  PRIMARY KEY (airline_id),
  airline_id CHAR(2)     NOT NULL,
  name       VARCHAR(50) NOT NULL
);



CREATE TABLE airport (
  CONSTRAINT pk_airport
  PRIMARY KEY (airport_id),
  airport_id CHAR(3)     NOT NULL,
  name       VARCHAR(70) NOT NULL,
  city       VARCHAR(60) NOT NULL,
  region     CHAR(2)     NOT NULL,
  country    CHAR(2)     NOT NULL
);



CREATE TABLE flight (
  CONSTRAINT pk_flight
  PRIMARY KEY (flight_id, airline_id),
  flight_id           VARCHAR(4)    NOT NULL,
  airline_id          CHAR(2)       NOT NULL
                      CONSTRAINT fk_airline1flight
                      REFERENCES airline (airline_id)
                      ON DELETE RESTRICT
                      ON UPDATE RESTRICT,
  origin_airport      CHAR(3)       NOT NULL
                      CONSTRAINT fk_airport1flight
                      REFERENCES airport (airport_id)
                      ON DELETE RESTRICT
                      ON UPDATE RESTRICT,
  destination_airport CHAR(3)       NOT NULL
                      CONSTRAINT fk_airport2flight
                      REFERENCES airport (airport_id)
                      ON DELETE RESTRICT
                      ON UPDATE RESTRICT,
  departure_days      CHAR(7)       NOT NULL,
  departure_time      TIME          NOT NULL,
  arrival_time        TIME          NOT NULL,
  capacity            INT           DEFAULT NULL
                      CONSTRAINT ck_capacity_rg
                      CHECK (capacity > 0),
  fare                DECIMAL(8, 2) NOT NULL
                      CONSTRAINT ck_fare_rg
                      CHECK (fare > 0.0),
  reserve_fare        DECIMAL(8, 2) NOT NULL
                      GENERATED ALWAYS AS (fare * 0.9),
  CONSTRAINT ck_airports_ne
  CHECK (origin_airport <> destination_airport)
);



CREATE TABLE route (
  CONSTRAINT pk_route
  PRIMARY KEY (flight_id, flight_airline, leg_id, leg_airline),
  flight_id      VARCHAR(4) NOT NULL,
  flight_airline CHAR(2)    NOT NULL,
  leg_id         VARCHAR(4) NOT NULL,
  leg_airline    CHAR(2)    NOT NULL,
  leg_order      INT        NOT NULL
                 CONSTRAINT ck_leg_order_rg
                 CHECK (leg_order > 0),
  CONSTRAINT fk_flight1route FOREIGN KEY (flight_id, flight_airline)
  REFERENCES flight (flight_id, airline_id)
  ON DELETE RESTRICT
  ON UPDATE RESTRICT,
  CONSTRAINT fk_flight2route FOREIGN KEY (leg_id, leg_airline)
  REFERENCES flight (flight_id, airline_id)
  ON DELETE RESTRICT
  ON UPDATE RESTRICT,
  CONSTRAINT ck_flights_ne
  CHECK ((flight_id <> leg_id) OR (flight_airline <> leg_airline))
);



CREATE TABLE fare_restriction (
  CONSTRAINT pk_fare_restriction
  PRIMARY KEY (restriction_type, flight_id, airline_id),
  restriction_type INT        NOT NULL
                   CONSTRAINT ck_restriction_type_rg
                   CHECK (restriction_type IN (0, 1, 2)),
  flight_id        VARCHAR(4) NOT NULL,
  airline_id       CHAR(2)    NOT NULL,
  advance_days     INT        DEFAULT NULL
                   CONSTRAINT ck_advance_days_rg
                   CHECK (advance_days > 0),
  minimum_stay     INT        DEFAULT NULL
                   CONSTRAINT ck_minimum_stay_rg
                   CHECK (minimum_stay > 0),
  maximum_stay     INT        DEFAULT NULL
                   CONSTRAINT ck_maximum_stay_rg
                   CHECK (maximum_stay > 0),
  CONSTRAINT fk_flight1fare_restriction
  FOREIGN KEY (flight_id, airline_id)
  REFERENCES flight (flight_id, airline_id)
  ON DELETE RESTRICT
  ON UPDATE RESTRICT
);



CREATE TABLE customer (
  CONSTRAINT pk_customer
  PRIMARY KEY (customer_id),
  customer_id INT          NOT NULL
              GENERATED ALWAYS AS IDENTITY,
  last_name   VARCHAR(50)  NOT NULL,
  first_name  VARCHAR(50)  NOT NULL,
  address     VARCHAR(100) NOT NULL,
  city        VARCHAR(40)  NOT NULL,
  state       CHAR(2)      NOT NULL,
  zip_code    CHAR(5)      NOT NULL,
  telephone   CHAR(10)     NOT NULL,
  rating      INT          DEFAULT 0 NOT NULL
              CONSTRAINT ck_rating_rg
              CHECK (rating IN (0, 1, 2)),
  CONSTRAINT un_customer
  UNIQUE (last_name, first_name, address, zip_code)
);



CREATE TABLE preference (
  CONSTRAINT pk_preference
  PRIMARY KEY (preference_type, customer_id),
  preference_type INT NOT NULL
                  CONSTRAINT ck_preference_type_rg
                  CHECK (preference_type IN (0, 1)),
  customer_id     INT NOT NULL
                  CONSTRAINT fk_customer1preference
                  REFERENCES customer (customer_id)
                  ON DELETE CASCADE
                  ON UPDATE RESTRICT,
  seat_choice     INT DEFAULT NULL
                  CONSTRAINT ck_seat_choice_rg
                  CHECK (seat_choice IN (0, 1)),
  meal_choice     INT DEFAULT NULL
                  CONSTRAINT ck_meal_choice_rg
                  CHECK (meal_choice IN (0, 1, 2, 3, 4, 5, 6))
);



CREATE TABLE account (
  CONSTRAINT pk_account
  PRIMARY KEY (account_id),
  account_id    INT          NOT NULL
                GENERATED ALWAYS AS IDENTITY,
  customer_id   INT          NOT NULL
                CONSTRAINT fk_customer1account
                REFERENCES customer (customer_id)
                ON DELETE RESTRICT
                ON UPDATE RESTRICT,
  email         VARCHAR(255) NOT NULL
                CONSTRAINT ck_email_ft
                CHECK (email LIKE '%@%.%'),
  credit_card   VARCHAR(16)  NOT NULL,
  creation_date DATE         DEFAULT CURRENT_DATE NOT NULL,
  CONSTRAINT un_account
  UNIQUE (email)
);



CREATE TABLE employee (
  CONSTRAINT pk_employee
  PRIMARY KEY (employee_id),
  employee_id INT           NOT NULL
              GENERATED ALWAYS AS IDENTITY,
  last_name   VARCHAR(50)   NOT NULL,
  first_name  VARCHAR(50)   NOT NULL,
  address     VARCHAR(100)  NOT NULL,
  city        VARCHAR(40)   NOT NULL,
  state       CHAR(2)       NOT NULL,
  zip_code    CHAR(5)       NOT NULL,
  telephone   CHAR(10)      NOT NULL,
  ssn         CHAR(9)       NOT NULL,
  hire_date   DATE          DEFAULT CURRENT_DATE NOT NULL,
  wage        DECIMAL(5, 2) NOT NULL
              CONSTRAINT ck_wage_rg
              CHECK (wage > 0.0),
  manager_id  INT           DEFAULT NULL
              CONSTRAINT fk_employee1employee
              REFERENCES employee (employee_id)
              ON DELETE RESTRICT
              ON UPDATE RESTRICT,
  CONSTRAINT un_employee
  UNIQUE (ssn)
);



CREATE TABLE reservation (
  CONSTRAINT pk_reservation
  PRIMARY KEY (reservation_id),
  reservation_id INT           NOT NULL
                 GENERATED ALWAYS AS IDENTITY,
  booking_date   DATE          DEFAULT CURRENT_DATE NOT NULL,
  flight_type    INT           NOT NULL
                 CONSTRAINT ck_flight_type_rg
                 CHECK (flight_type IN (0, 1, 2)),
  flight_class   INT           NOT NULL
                 CONSTRAINT ck_flight_class_rg
                 CHECK (flight_class IN (0, 1, 2)),
  price          DECIMAL(8, 2) NOT NULL
                 CONSTRAINT ck_price_rg
                 CHECK (price > 0.0),
  fee            DECIMAL(8, 2) NOT NULL
                 GENERATED ALWAYS AS (price * 0.1),
  employee_id    INT           NOT NULL
                 CONSTRAINT fk_employee1reservation
                 REFERENCES employee (employee_id)
                 ON DELETE RESTRICT
                 ON UPDATE RESTRICT
);



CREATE TABLE bid (
  CONSTRAINT pk_bid
  PRIMARY KEY (reservation_id, account_id, bid_amount),
  reservation_id INT           NOT NULL
                 CONSTRAINT fk_reservation1bid
                 REFERENCES reservation (reservation_id)
                 ON DELETE CASCADE
                 ON UPDATE RESTRICT,
  account_id     INT           NOT NULL
                 CONSTRAINT fk_account1bid
                 REFERENCES account (account_id)
                 ON DELETE CASCADE
                 ON UPDATE RESTRICT,
  bid_amount     DECIMAL(8, 2) NOT NULL
                 CONSTRAINT ck_bid_amount_rg
                 CHECK (bid_amount > 0.0)
);



CREATE TABLE booked (
  CONSTRAINT pk_booked
  PRIMARY KEY (reservation_id, account_id, flight_id, airline_id, flight_order),
  reservation_id INT        NOT NULL
                 CONSTRAINT fk_reservation1booked
                 REFERENCES reservation (reservation_id)
                 ON DELETE CASCADE
                 ON UPDATE RESTRICT,
  account_id     INT        NOT NULL
                 CONSTRAINT fk_account1booked
                 REFERENCES account (account_id)
                 ON DELETE RESTRICT
                 ON UPDATE RESTRICT,
  flight_id      VARCHAR(4) NOT NULL,
  airline_id     CHAR(2)    NOT NULL,
  flight_order   INT        NOT NULL
                 CONSTRAINT ck_flight_order_rg
                 CHECK (flight_order > 0),
  departure_date DATE       NOT NULL,
  seat_number    INT        NOT NULL
                 CONSTRAINT ck_seat_number_rg
                 CHECK (seat_number >= 0),
  special_meal   INT        DEFAULT 0 NOT NULL
                 CONSTRAINT ck_special_meal_rg
                 CHECK (special_meal IN (0, 1, 2, 3, 4, 5, 6)),
  CONSTRAINT fk_flight1booked
  FOREIGN KEY (flight_id, airline_id)
  REFERENCES flight (flight_id, airline_id)
  ON DELETE RESTRICT
  ON UPDATE RESTRICT
);



DISCONNECT ALL;

EXIT;
