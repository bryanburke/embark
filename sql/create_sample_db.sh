#!/bin/sh

java -Djdbc.drivers=org.apache.derby.jdbc.EmbeddedDriver org.apache.derby.tools.ij $(dirname $(readlink -f "$0"))/schema.sql
java -Djdbc.drivers=org.apache.derby.jdbc.EmbeddedDriver org.apache.derby.tools.ij $(dirname $(readlink -f "$0"))/data.sql
