--------------------------------------------------------------------------------
-- Bryan Burke, Joshua Lambert, and Elizabeth Rothermel                       --
-- Group 4                                                                    --
-- INFX 499                                                                   --
-- Fall 2015                                                                  --
-- University of Louisiana at Lafayette                                       --
--------------------------------------------------------------------------------

CONNECT 'jdbc:derby:db';



/*
 * AIRLINE RELATION
 */
INSERT INTO airline (airline_id, name)
VALUES ('AA', 'American Airlines');
INSERT INTO airline (airline_id, name)
VALUES ('AS', 'Alaska Airlines, Inc.');
INSERT INTO airline (airline_id, name)
VALUES ('AX', 'Trans States Airlines');
INSERT INTO airline (airline_id, name)
VALUES ('B6', 'JetBlue Airways');
INSERT INTO airline (airline_id, name)
VALUES ('C5', 'CommutAir');
INSERT INTO airline (airline_id, name)
VALUES ('CP', 'Compass Airlines');
INSERT INTO airline (airline_id, name)
VALUES ('DL', 'Delta Air Lines');
INSERT INTO airline (airline_id, name)
VALUES ('EV', 'ExpressJet');
INSERT INTO airline (airline_id, name)
VALUES ('F9', 'Frontier Airlines');
INSERT INTO airline (airline_id, name)
VALUES ('G4', 'Allegiant Air');
INSERT INTO airline (airline_id, name)
VALUES ('G7', 'GoJet Airlines');
INSERT INTO airline (airline_id, name)
VALUES ('HA', 'Hawaiian Airlines');
INSERT INTO airline (airline_id, name)
VALUES ('NK', 'Spirit Airlines');
INSERT INTO airline (airline_id, name)
VALUES ('OO', 'SkyWest Airlines');
INSERT INTO airline (airline_id, name)
VALUES ('RW', 'Republic Airlines');
INSERT INTO airline (airline_id, name)
VALUES ('S5', 'Shuttle America');
INSERT INTO airline (airline_id, name)
VALUES ('SY', 'Sun Country Airlines');
INSERT INTO airline (airline_id, name)
VALUES ('UA', 'United Airlines');
INSERT INTO airline (airline_id, name)
VALUES ('VX', 'Virgin America');
INSERT INTO airline (airline_id, name)
VALUES ('WN', 'Southwest Airlines');
INSERT INTO airline (airline_id, name)
VALUES ('YV', 'Mesa Airlines');
INSERT INTO airline (airline_id, name)
VALUES ('ZW', 'Air Wisconsin');
INSERT INTO airline (airline_id, name)
VALUES ('ZZ', 'MULTIPLE LEGS');



/*
 * AIRPORT RELATION
 */
INSERT INTO airport (airport_id, name, city, region, country)
VALUES ('ABQ', 'Albuquerque International Sunport', 'Albuquerque', 'NM', 'US');
INSERT INTO airport (airport_id, name, city, region, country)
VALUES ('ANC', 'Ted Stevens Anchorage International Airport', 'Anchorage', 'AK',
        'US');
INSERT INTO airport (airport_id, name, city, region, country)
VALUES ('ATL', 'Hartsfield–Jackson Atlanta International Airport', 'Atlanta',
        'GA', 'US');
INSERT INTO airport (airport_id, name, city, region, country)
VALUES ('AUS', 'Austin-Bergstrom International Airport', 'Austin', 'TX', 'US');
INSERT INTO airport (airport_id, name, city, region, country)
VALUES ('BDL', 'Bradley International Airport', 'Hartford', 'CT', 'US');
INSERT INTO airport (airport_id, name, city, region, country)
VALUES ('BNA', 'Nashville International Airport', 'Nashville', 'TN', 'US');
INSERT INTO airport (airport_id, name, city, region, country)
VALUES ('BOS', 'Logan International Airport', 'Boston', 'MA', 'US');
INSERT INTO airport (airport_id, name, city, region, country)
VALUES ('BUF', 'Buffalo Niagara International Airport', 'Buffalo', 'NY', 'US');
INSERT INTO airport (airport_id, name, city, region, country)
VALUES ('BUR', 'Bob Hope Airport', 'Burbank', 'CA', 'US');
INSERT INTO airport (airport_id, name, city, region, country)
VALUES ('BWI', 'Baltimore–Washington International Airport',
        'Baltimore/Washington, D.C.', 'MD', 'US');
INSERT INTO airport (airport_id, name, city, region, country)
VALUES ('CLE', 'Cleveland Hopkins International Airport', 'Cleveland', 'OH',
        'US');
INSERT INTO airport (airport_id, name, city, region, country)
VALUES ('CLT', 'Charlotte Douglas International Airport', 'Charlotte', 'NC',
        'US');
INSERT INTO airport (airport_id, name, city, region, country)
VALUES ('CMH', 'Port Columbus International Airport', 'Columbus', 'OH', 'US');
INSERT INTO airport (airport_id, name, city, region, country)
VALUES ('CVG', 'Cincinnati/Northern Kentucky International Airport',
        'Cincinnati', 'KY', 'US');
INSERT INTO airport (airport_id, name, city, region, country)
VALUES ('DAL', 'Dallas Love Field', 'Dallas', 'TX', 'US');
INSERT INTO airport (airport_id, name, city, region, country)
VALUES ('DCA', 'Ronald Reagan Washington National Airport', 'Washington, D.C.',
        'VA', 'US');
INSERT INTO airport (airport_id, name, city, region, country)
VALUES ('DEN', 'Denver International Airport', 'Denver', 'CO', 'US');
INSERT INTO airport (airport_id, name, city, region, country)
VALUES ('DFW', 'Dallas/Fort Worth International Airport', 'Dallas/Fort Worth',
        'TX', 'US');
INSERT INTO airport (airport_id, name, city, region, country)
VALUES ('DTW', 'Detroit Metropolitan Wayne County Airport', 'Detroit', 'MI',
        'US');
INSERT INTO airport (airport_id, name, city, region, country)
VALUES ('EWR', 'Newark Liberty International Airport', 'Newark/New York', 'NJ',
        'US');
INSERT INTO airport (airport_id, name, city, region, country)
VALUES ('FLL', 'Fort Lauderdale–Hollywood International Airport',
        'Fort Lauderdale', 'FL', 'US');
INSERT INTO airport (airport_id, name, city, region, country)
VALUES ('HNL', 'Honolulu International Airport', 'Honolulu', 'HI', 'US');
INSERT INTO airport (airport_id, name, city, region, country)
VALUES ('HOU', 'William P. Hobby Airport', 'Houston', 'TX', 'US');
INSERT INTO airport (airport_id, name, city, region, country)
VALUES ('IAD', 'Washington Dulles International Airport', 'Washington, D.C.',
        'VA', 'US');
INSERT INTO airport (airport_id, name, city, region, country)
VALUES ('IAH', 'George Bush Intercontinental Airport', 'Houston', 'TX', 'US');
INSERT INTO airport (airport_id, name, city, region, country)
VALUES ('IND', 'Indianapolis International Airport', 'Indianapolis', 'IN',
        'US');
INSERT INTO airport (airport_id, name, city, region, country)
VALUES ('JAX', 'Jacksonville International Airport', 'Jacksonville', 'FL',
        'US');
INSERT INTO airport (airport_id, name, city, region, country)
VALUES ('JFK', 'John F. Kennedy International Airport', 'New York', 'NY', 'US');
INSERT INTO airport (airport_id, name, city, region, country)
VALUES ('LAS', 'McCarran International Airport', 'Las Vegas', 'NV', 'US');
INSERT INTO airport (airport_id, name, city, region, country)
VALUES ('LAX', 'Los Angeles International Airport', 'Los Angeles', 'CA', 'US');
INSERT INTO airport (airport_id, name, city, region, country)
VALUES ('LFT', 'Lafayette Regional Airport', 'Lafayette', 'LA', 'US');
INSERT INTO airport (airport_id, name, city, region, country)
VALUES ('LGA', 'LaGuardia Airport', 'New York', 'NY', 'US');
INSERT INTO airport (airport_id, name, city, region, country)
VALUES ('MCI', 'Kansas City International Airport', 'Kansas City', 'MO', 'US');
INSERT INTO airport (airport_id, name, city, region, country)
VALUES ('MCO', 'Orlando International Airport', 'Orlando', 'FL', 'US');
INSERT INTO airport (airport_id, name, city, region, country)
VALUES ('MDW', 'Midway International Airport', 'Chicago', 'IL', 'US');
INSERT INTO airport (airport_id, name, city, region, country)
VALUES ('MIA', 'Miami International Airport', 'Miami', 'FL', 'US');
INSERT INTO airport (airport_id, name, city, region, country)
VALUES ('MKE', 'General Mitchell International Airport', 'Milwaukee', 'WI',
        'US');
INSERT INTO airport (airport_id, name, city, region, country)
VALUES ('MSP', 'Minneapolis–Saint Paul International Airport',
        'Minneapolis/St. Paul', 'MN', 'US');
INSERT INTO airport (airport_id, name, city, region, country)
VALUES ('MSY', 'Louis Armstrong New Orleans International Airport',
        'New Orleans', 'LA', 'US');
INSERT INTO airport (airport_id, name, city, region, country)
VALUES ('OAK', 'Oakland International Airport', 'Oakland', 'CA', 'US');
INSERT INTO airport (airport_id, name, city, region, country)
VALUES ('OGG', 'Kahului Airport', 'Kahului', 'HI', 'US');
INSERT INTO airport (airport_id, name, city, region, country)
VALUES ('OMA', 'Eppley Airfield', 'Omaha', 'NE', 'US');
INSERT INTO airport (airport_id, name, city, region, country)
VALUES ('ONT', 'Ontario International Airport', 'Ontario', 'CA', 'US');
INSERT INTO airport (airport_id, name, city, region, country)
VALUES ('ORD', 'O''Hare International Airport', 'Chicago', 'IL', 'US');
INSERT INTO airport (airport_id, name, city, region, country)
VALUES ('PBI', 'Palm Beach International Airport', 'West Palm Beach', 'FL',
        'US');
INSERT INTO airport (airport_id, name, city, region, country)
VALUES ('PDX', 'Portland International Airport', 'Portland', 'OR', 'US');
INSERT INTO airport (airport_id, name, city, region, country)
VALUES ('PHL', 'Philadelphia International Airport', 'Philadelphia', 'PA',
        'US');
INSERT INTO airport (airport_id, name, city, region, country)
VALUES ('PHX', 'Phoenix Sky Harbor International Airport', 'Phoenix', 'AZ',
        'US');
INSERT INTO airport (airport_id, name, city, region, country)
VALUES ('PIT', 'Pittsburgh International Airport', 'Pittsburgh', 'PA', 'US');
INSERT INTO airport (airport_id, name, city, region, country)
VALUES ('RDU', 'Raleigh–Durham International Airport', 'Raleigh', 'NC', 'US');
INSERT INTO airport (airport_id, name, city, region, country)
VALUES ('RSW', 'Southwest Florida International Airport', 'Fort Myers', 'FL',
        'US');
INSERT INTO airport (airport_id, name, city, region, country)
VALUES ('SAN', 'San Diego International Airport', 'San Diego', 'CA', 'US');
INSERT INTO airport (airport_id, name, city, region, country)
VALUES ('SAT', 'San Antonio International Airport', 'San Antonio', 'TX', 'US');
INSERT INTO airport (airport_id, name, city, region, country)
VALUES ('SEA', 'Seattle–Tacoma International Airport', 'Seattle', 'WA', 'US');
INSERT INTO airport (airport_id, name, city, region, country)
VALUES ('SFO', 'San Francisco International Airport', 'San Francisco', 'CA',
        'US');
INSERT INTO airport (airport_id, name, city, region, country)
VALUES ('SJC', 'San Jose International Airport', 'San Jose', 'CA', 'US');
INSERT INTO airport (airport_id, name, city, region, country)
VALUES ('SJU', 'Luis Muñoz Marín International Airport', 'San Juan', 'PR',
        'US');
INSERT INTO airport (airport_id, name, city, region, country)
VALUES ('SLC', 'Salt Lake City International Airport', 'Salt Lake City', 'UT',
        'US');
INSERT INTO airport (airport_id, name, city, region, country)
VALUES ('SMF', 'Sacramento International Airport', 'Sacramento', 'CA', 'US');
INSERT INTO airport (airport_id, name, city, region, country)
VALUES ('SNA', 'John Wayne Airport', 'Santa Ana', 'CA', 'US');
INSERT INTO airport (airport_id, name, city, region, country)
VALUES ('STL', 'Lambert–St. Louis International Airport', 'St. Louis', 'MO',
        'US');
INSERT INTO airport (airport_id, name, city, region, country)
VALUES ('TPA', 'Tampa International Airport', 'Tampa', 'FL', 'US');



/*
 * FLIGHT RELATION
 */
INSERT INTO flight (flight_id, airline_id, origin_airport, destination_airport,
                    departure_days, departure_time, arrival_time, capacity,
                    fare)
VALUES ('5071', 'DL', 'LFT', 'ATL', 'NMTWRFS', '14:17', '16:49', 175, 308.00);
INSERT INTO flight (flight_id, airline_id, origin_airport, destination_airport,
                    departure_days, departure_time, arrival_time, capacity,
                    fare)
VALUES ('2497', 'DL', 'LFT', 'ATL', 'NMTWRFS', '6:00', '8:34', 97, 308.00);
INSERT INTO flight (flight_id, airline_id, origin_airport, destination_airport,
                    departure_days, departure_time, arrival_time, capacity,
                    fare)
VALUES ('5072', 'DL', 'ATL', 'LFT', 'NMTWRFS', '13:05', '13:52', 123, 313.00);
INSERT INTO flight (flight_id, airline_id, origin_airport, destination_airport,
                    departure_days, departure_time, arrival_time, capacity,
                    fare)
VALUES ('5598', 'DL', 'ATL', 'LFT', 'NMTWRFS', '10:05', '10:55', 121, 313.00);
INSERT INTO flight (flight_id, airline_id, origin_airport, destination_airport,
                    departure_days, departure_time, arrival_time, capacity,
                    fare)
VALUES ('976', 'DL', 'ATL', 'JFK', 'NMTWRFS', '10:31', '12:48', 96, 333.00);
INSERT INTO flight (flight_id, airline_id, origin_airport, destination_airport,
                    departure_days, departure_time, arrival_time, capacity,
                    fare)
VALUES ('1296', 'DL', 'ATL', 'JFK', 'NMTWRFS', '17:43', '20:09', 151, 333.00);
INSERT INTO flight (flight_id, airline_id, origin_airport, destination_airport,
                    departure_days, departure_time, arrival_time, capacity,
                    fare)
VALUES ('449', 'DL', 'JFK', 'ATL', 'NMTWRFS', '12:00', '14:36', 95, 333.00);
INSERT INTO flight (flight_id, airline_id, origin_airport, destination_airport,
                    departure_days, departure_time, arrival_time, capacity,
                    fare)
VALUES ('440', 'DL', 'JFK', 'ATL', 'NMTWRFS', '6:15', '8:59', 120, 333.00);
INSERT INTO flight (flight_id, airline_id, origin_airport, destination_airport,
                    departure_days, departure_time, arrival_time, capacity,
                    fare)
VALUES ('9131', 'ZZ', 'LFT', 'JFK', 'NMTWRFS', '14:17', '20:09', NULL, 513.00);
INSERT INTO flight (flight_id, airline_id, origin_airport, destination_airport,
                    departure_days, departure_time, arrival_time, capacity,
                    fare)
VALUES ('8513', 'ZZ', 'LFT', 'JFK', 'NMTWRFS', '6:00', '12:48', NULL, 513.00);
INSERT INTO flight (flight_id, airline_id, origin_airport, destination_airport,
                    departure_days, departure_time, arrival_time, capacity,
                    fare)
VALUES ('7976', 'ZZ', 'JFK', 'LFT', 'NMTWRFS', '12:00', '13:52', NULL, 517.00);
INSERT INTO flight (flight_id, airline_id, origin_airport, destination_airport,
                    departure_days, departure_time, arrival_time, capacity,
                    fare)
VALUES ('7622', 'ZZ', 'JFK', 'LFT', 'NMTWRFS', '6:15', '10:55', NULL, 517.00);
INSERT INTO flight (flight_id, airline_id, origin_airport, destination_airport,
                    departure_days, departure_time, arrival_time, capacity,
                    fare)
VALUES ('1777', 'DL', 'ATL', 'ORD', 'NMTWRFS', '17:29', '18:34', 135, 172.00);
INSERT INTO flight (flight_id, airline_id, origin_airport, destination_airport,
                    departure_days, departure_time, arrival_time, capacity,
                    fare)
VALUES ('241', 'UA', 'ATL', 'ORD', '-MTWRF-', '10:10', '11:15', 191, 88.00);
INSERT INTO flight (flight_id, airline_id, origin_airport, destination_airport,
                    departure_days, departure_time, arrival_time, capacity,
                    fare)
VALUES ('3612', 'UA', 'ORD', 'ATL', 'NMTWRFS', '9:15', '12:11', 106, 163.00);
INSERT INTO flight (flight_id, airline_id, origin_airport, destination_airport,
                    departure_days, departure_time, arrival_time, capacity,
                    fare)
VALUES ('421', 'UA', 'ORD', 'ATL', 'NMTWRFS', '6:00', '9:04', 172, 88.00);
INSERT INTO flight (flight_id, airline_id, origin_airport, destination_airport,
                    departure_days, departure_time, arrival_time, capacity,
                    fare)
VALUES ('7129', 'ZZ', 'LFT', 'ORD', 'NMTWRFS', '14:17', '18:34', NULL, 401.00);
INSERT INTO flight (flight_id, airline_id, origin_airport, destination_airport,
                    departure_days, departure_time, arrival_time, capacity,
                    fare)
VALUES ('6394', 'ZZ', 'LFT', 'ORD', '-MTWRF-', '6:00', '11:15', NULL, 401.00);
INSERT INTO flight (flight_id, airline_id, origin_airport, destination_airport,
                    departure_days, departure_time, arrival_time, capacity,
                    fare)
VALUES ('9643', 'ZZ', 'ORD', 'LFT', 'NMTWRFS', '9:15', '13:52', NULL, 405.00);
INSERT INTO flight (flight_id, airline_id, origin_airport, destination_airport,
                    departure_days, departure_time, arrival_time, capacity,
                    fare)
VALUES ('8606', 'ZZ', 'ORD', 'LFT', 'NMTWRFS', '6:00', '10:55', NULL, 405.00);
INSERT INTO flight (flight_id, airline_id, origin_airport, destination_airport,
                    departure_days, departure_time, arrival_time, capacity,
                    fare)
VALUES ('3108', 'AA', 'LFT', 'DFW', 'NMTWRFS', '17:55', '19:32', 101, 144.00);
INSERT INTO flight (flight_id, airline_id, origin_airport, destination_airport,
                    departure_days, departure_time, arrival_time, capacity,
                    fare)
VALUES ('3367', 'AA', 'LFT', 'DFW', 'NMTWRFS', '6:10', '7:47', 165, 144.00);
INSERT INTO flight (flight_id, airline_id, origin_airport, destination_airport,
                    departure_days, departure_time, arrival_time, capacity,
                    fare)
VALUES ('3109', 'AA', 'DFW', 'LFT', 'NMTWRFS', '15:55', '17:12', 161, 149.00);
INSERT INTO flight (flight_id, airline_id, origin_airport, destination_airport,
                    departure_days, departure_time, arrival_time, capacity,
                    fare)
VALUES ('2748', 'AA', 'DFW', 'LFT', 'NMTWRFS', '8:50', '10:12', 91, 149.00);
INSERT INTO flight (flight_id, airline_id, origin_airport, destination_airport,
                    departure_days, departure_time, arrival_time, capacity,
                    fare)
VALUES ('2378', 'AA', 'DFW', 'ORD', 'NMTWRFS', '22:00', '0:27', 107, 108.00);
INSERT INTO flight (flight_id, airline_id, origin_airport, destination_airport,
                    departure_days, departure_time, arrival_time, capacity,
                    fare)
VALUES ('2335', 'AA', 'DFW', 'ORD', 'NMTWRFS', '10:15', '12:41', 172, 108.00);
INSERT INTO flight (flight_id, airline_id, origin_airport, destination_airport,
                    departure_days, departure_time, arrival_time, capacity,
                    fare)
VALUES ('2334', 'AA', 'ORD', 'DFW', 'NMTWRFS', '12:05', '14:48', 116, 108.00);
INSERT INTO flight (flight_id, airline_id, origin_airport, destination_airport,
                    departure_days, departure_time, arrival_time, capacity,
                    fare)
VALUES ('2301', 'AA', 'ORD', 'DFW', 'NMTWRFS', '5:00', '7:37', 191, 156.00);
INSERT INTO flight (flight_id, airline_id, origin_airport, destination_airport,
                    departure_days, departure_time, arrival_time, capacity,
                    fare)
VALUES ('8179', 'ZZ', 'LFT', 'ORD', 'NMTWRFS', '17:55', '0:27', NULL, 217.00);
INSERT INTO flight (flight_id, airline_id, origin_airport, destination_airport,
                    departure_days, departure_time, arrival_time, capacity,
                    fare)
VALUES ('6525', 'ZZ', 'LFT', 'ORD', 'NMTWRFS', '6:10', '12:41', NULL, 217.00);
INSERT INTO flight (flight_id, airline_id, origin_airport, destination_airport,
                    departure_days, departure_time, arrival_time, capacity,
                    fare)
VALUES ('6487', 'ZZ', 'ORD', 'LFT', 'NMTWRFS', '12:05', '17:12', NULL, 217.00);
INSERT INTO flight (flight_id, airline_id, origin_airport, destination_airport,
                    departure_days, departure_time, arrival_time, capacity,
                    fare)
VALUES ('8973', 'ZZ', 'ORD', 'LFT', 'NMTWRFS', '5:00', '10:12', NULL, 217.00);
INSERT INTO flight (flight_id, airline_id, origin_airport, destination_airport,
                    departure_days, departure_time, arrival_time, capacity,
                    fare)
VALUES ('2495', 'AA', 'DFW', 'LAX', 'NMTWRFS', '20:30', '22:00', 182, 143.00);
INSERT INTO flight (flight_id, airline_id, origin_airport, destination_airport,
                    departure_days, departure_time, arrival_time, capacity,
                    fare)
VALUES ('2250', 'AA', 'DFW', 'LAX', 'NMTWRFS', '8:25', '9:49', 136, 108.00);
INSERT INTO flight (flight_id, airline_id, origin_airport, destination_airport,
                    departure_days, departure_time, arrival_time, capacity,
                    fare)
VALUES ('5726', 'DL', 'LAX', 'DFW', 'NMTWRFS', '9:45', '14:55', 178, 111.00);
INSERT INTO flight (flight_id, airline_id, origin_airport, destination_airport,
                    departure_days, departure_time, arrival_time, capacity,
                    fare)
VALUES ('2399', 'AA', 'LAX', 'DFW', 'NMTWRFS', '1:20', '6:12', 192, 143.00);
INSERT INTO flight (flight_id, airline_id, origin_airport, destination_airport,
                    departure_days, departure_time, arrival_time, capacity,
                    fare)
VALUES ('7245', 'ZZ', 'LFT', 'LAX', 'NMTWRFS', '17:55', '22:00', NULL, 262.00);
INSERT INTO flight (flight_id, airline_id, origin_airport, destination_airport,
                    departure_days, departure_time, arrival_time, capacity,
                    fare)
VALUES ('8498', 'ZZ', 'LFT', 'LAX', 'NMTWRFS', '6:10', '9:49', NULL, 262.00);
INSERT INTO flight (flight_id, airline_id, origin_airport, destination_airport,
                    departure_days, departure_time, arrival_time, capacity,
                    fare)
VALUES ('9685', 'ZZ', 'LAX', 'LFT', 'NMTWRFS', '9:45', '17:12', NULL, 266.00);
INSERT INTO flight (flight_id, airline_id, origin_airport, destination_airport,
                    departure_days, departure_time, arrival_time, capacity,
                    fare)
VALUES ('7267', 'ZZ', 'LAX', 'LFT', 'NMTWRFS', '1:20', '10:12', NULL, 266.00);
INSERT INTO flight (flight_id, airline_id, origin_airport, destination_airport,
                    departure_days, departure_time, arrival_time, capacity,
                    fare)
VALUES ('9', 'HA', 'LAX', 'HNL', 'NMTWRFS', '17:05', '21:00', 194, 454.00);
INSERT INTO flight (flight_id, airline_id, origin_airport, destination_airport,
                    departure_days, departure_time, arrival_time, capacity,
                    fare)
VALUES ('4', 'HA', 'HNL', 'LAX', 'NMTWRFS', '22:10', '5:30', 181, 300.00);
INSERT INTO flight (flight_id, airline_id, origin_airport, destination_airport,
                    departure_days, departure_time, arrival_time, capacity,
                    fare)
VALUES ('7521', 'ZZ', 'LFT', 'HNL', 'NMTWRFS', '6:10', '21:00', NULL, 642.00);
INSERT INTO flight (flight_id, airline_id, origin_airport, destination_airport,
                    departure_days, departure_time, arrival_time, capacity,
                    fare)
VALUES ('6141', 'ZZ', 'HNL', 'LFT', 'NMTWRFS', '22:10', '17:12', NULL, 584.00);



/*
 * ROUTE RELATION
 */
INSERT INTO route (flight_id, flight_airline, leg_id, leg_airline, leg_order)
VALUES ('9131', 'ZZ', '5071', 'DL', 1);
INSERT INTO route (flight_id, flight_airline, leg_id, leg_airline, leg_order)
VALUES ('9131', 'ZZ', '1296', 'DL', 2);
INSERT INTO route (flight_id, flight_airline, leg_id, leg_airline, leg_order)
VALUES ('8513', 'ZZ', '2497', 'DL', 1);
INSERT INTO route (flight_id, flight_airline, leg_id, leg_airline, leg_order)
VALUES ('8513', 'ZZ', '976', 'DL', 2);
INSERT INTO route (flight_id, flight_airline, leg_id, leg_airline, leg_order)
VALUES ('7976', 'ZZ', '449', 'DL', 1);
INSERT INTO route (flight_id, flight_airline, leg_id, leg_airline, leg_order)
VALUES ('7976', 'ZZ', '5072', 'DL', 2);
INSERT INTO route (flight_id, flight_airline, leg_id, leg_airline, leg_order)
VALUES ('7622', 'ZZ', '440', 'DL', 1);
INSERT INTO route (flight_id, flight_airline, leg_id, leg_airline, leg_order)
VALUES ('7622', 'ZZ', '5598', 'DL', 2);
INSERT INTO route (flight_id, flight_airline, leg_id, leg_airline, leg_order)
VALUES ('7129', 'ZZ', '5071', 'DL', 1);
INSERT INTO route (flight_id, flight_airline, leg_id, leg_airline, leg_order)
VALUES ('7129', 'ZZ', '1777', 'DL', 2);
INSERT INTO route (flight_id, flight_airline, leg_id, leg_airline, leg_order)
VALUES ('6394', 'ZZ', '2497', 'DL', 1);
INSERT INTO route (flight_id, flight_airline, leg_id, leg_airline, leg_order)
VALUES ('6394', 'ZZ', '241', 'UA', 2);
INSERT INTO route (flight_id, flight_airline, leg_id, leg_airline, leg_order)
VALUES ('9643', 'ZZ', '3612', 'UA', 1);
INSERT INTO route (flight_id, flight_airline, leg_id, leg_airline, leg_order)
VALUES ('9643', 'ZZ', '5072', 'DL', 2);
INSERT INTO route (flight_id, flight_airline, leg_id, leg_airline, leg_order)
VALUES ('8606', 'ZZ', '421', 'UA', 1);
INSERT INTO route (flight_id, flight_airline, leg_id, leg_airline, leg_order)
VALUES ('8606', 'ZZ', '5598', 'DL', 2);
INSERT INTO route (flight_id, flight_airline, leg_id, leg_airline, leg_order)
VALUES ('8179', 'ZZ', '3108', 'AA', 1);
INSERT INTO route (flight_id, flight_airline, leg_id, leg_airline, leg_order)
VALUES ('8179', 'ZZ', '2378', 'AA', 2);
INSERT INTO route (flight_id, flight_airline, leg_id, leg_airline, leg_order)
VALUES ('6525', 'ZZ', '3367', 'AA', 1);
INSERT INTO route (flight_id, flight_airline, leg_id, leg_airline, leg_order)
VALUES ('6525', 'ZZ', '2335', 'AA', 2);
INSERT INTO route (flight_id, flight_airline, leg_id, leg_airline, leg_order)
VALUES ('6487', 'ZZ', '2334', 'AA', 1);
INSERT INTO route (flight_id, flight_airline, leg_id, leg_airline, leg_order)
VALUES ('6487', 'ZZ', '3109', 'AA', 2);
INSERT INTO route (flight_id, flight_airline, leg_id, leg_airline, leg_order)
VALUES ('8973', 'ZZ', '2301', 'AA', 1);
INSERT INTO route (flight_id, flight_airline, leg_id, leg_airline, leg_order)
VALUES ('8973', 'ZZ', '2748', 'AA', 2);
INSERT INTO route (flight_id, flight_airline, leg_id, leg_airline, leg_order)
VALUES ('7245', 'ZZ', '3108', 'AA', 1);
INSERT INTO route (flight_id, flight_airline, leg_id, leg_airline, leg_order)
VALUES ('7245', 'ZZ', '2495', 'AA', 2);
INSERT INTO route (flight_id, flight_airline, leg_id, leg_airline, leg_order)
VALUES ('8498', 'ZZ', '3367', 'AA', 1);
INSERT INTO route (flight_id, flight_airline, leg_id, leg_airline, leg_order)
VALUES ('8498', 'ZZ', '2250', 'AA', 2);
INSERT INTO route (flight_id, flight_airline, leg_id, leg_airline, leg_order)
VALUES ('9685', 'ZZ', '5726', 'DL', 1);
INSERT INTO route (flight_id, flight_airline, leg_id, leg_airline, leg_order)
VALUES ('9685', 'ZZ', '3109', 'AA', 2);
INSERT INTO route (flight_id, flight_airline, leg_id, leg_airline, leg_order)
VALUES ('7267', 'ZZ', '2399', 'AA', 1);
INSERT INTO route (flight_id, flight_airline, leg_id, leg_airline, leg_order)
VALUES ('7267', 'ZZ', '2748', 'AA', 2);
INSERT INTO route (flight_id, flight_airline, leg_id, leg_airline, leg_order)
VALUES ('7521', 'ZZ', '3367', 'AA', 1);
INSERT INTO route (flight_id, flight_airline, leg_id, leg_airline, leg_order)
VALUES ('7521', 'ZZ', '2250', 'AA', 2);
INSERT INTO route (flight_id, flight_airline, leg_id, leg_airline, leg_order)
VALUES ('7521', 'ZZ', '9', 'HA', 3);
INSERT INTO route (flight_id, flight_airline, leg_id, leg_airline, leg_order)
VALUES ('6141', 'ZZ', '4', 'HA', 1);
INSERT INTO route (flight_id, flight_airline, leg_id, leg_airline, leg_order)
VALUES ('6141', 'ZZ', '5726', 'DL', 2);
INSERT INTO route (flight_id, flight_airline, leg_id, leg_airline, leg_order)
VALUES ('6141', 'ZZ', '3109', 'AA', 3);



/*
 * FARE_RESTRICTION RELATION
 */
INSERT INTO fare_restriction (restriction_type, flight_id, airline_id,
                              advance_days, minimum_stay, maximum_stay)
VALUES (0, '7521', 'ZZ', 14, NULL, NULL);
INSERT INTO fare_restriction (restriction_type, flight_id, airline_id,
                              advance_days, minimum_stay, maximum_stay)
VALUES (1, '8179', 'ZZ', NULL, 7, NULL);



/*
 * CUSTOMER RELATION
 */
INSERT INTO customer (last_name, first_name, address, city, state, zip_code,
                      telephone, rating)
VALUES ('Burke', 'Bryan', '2579 Virgil Street', 'Tallahassee', 'FL', '32301',
        '8502421582', 0);
INSERT INTO customer (last_name, first_name, address, city, state, zip_code,
                      telephone, rating)
VALUES ('Dwyer', 'Regina', '480 Poling Farm Road', 'Plattsmouth', 'NE', '68048',
        '4022981490', 1);
INSERT INTO customer (last_name, first_name, address, city, state, zip_code,
                      telephone, rating)
VALUES ('Glass', 'James', '215 Penn Street', 'Manchester', 'MO', '63011',
        '5732597952', 0);
INSERT INTO customer (last_name, first_name, address, city, state, zip_code,
                      telephone, rating)
VALUES ('Lambert', 'Joshua', '4043 Rogers Street', 'Cincinnati', 'OH', '45229',
        '5135697099', 0);
INSERT INTO customer (last_name, first_name, address, city, state, zip_code,
                      telephone, rating)
VALUES ('Patel', 'Kimberly', '3548 Orchard Street', 'Osseo', 'MN', '55369',
        '9528552611', 2);
INSERT INTO customer (last_name, first_name, address, city, state, zip_code,
                      telephone, rating)
VALUES ('Phillips', 'Valeria', '4254 Gladwell Street', 'Dallas', 'TX', '75207',
        '9032214245', 0);
INSERT INTO customer (last_name, first_name, address, city, state, zip_code,
                      telephone, rating)
VALUES ('Rothermel', 'Elizabeth', '2285 Buffalo Creek Road', 'Nashville', 'TN',
        '37210', '6159217823', 0);



/*
 * PREFERENCE RELATION
 */
INSERT INTO preference (preference_type, customer_id, seat_choice, meal_choice)
VALUES (0, 5, 1, NULL);
INSERT INTO preference (preference_type, customer_id, seat_choice, meal_choice)
VALUES (1, 5, NULL, 1);



/*
 * ACCOUNT RELATION
 */
INSERT INTO account (customer_id, email, credit_card, creation_date)
VALUES (1, 'burke.bryan@lawsuitaid.com', '492923344707XXXX', '2015-12-03');
INSERT INTO account (customer_id, email, credit_card, creation_date)
VALUES (2, 'dwyer.regina@campmissions.com', '471632025424XXXX', '2013-06-11');
INSERT INTO account (customer_id, email, credit_card, creation_date)
VALUES (3, 'glass.james@typofail.com', '491694881947XXXX', '2015-04-27');
INSERT INTO account (customer_id, email, credit_card, creation_date)
VALUES (4, 'lambert.joshua@lovemedics.com', '492961983783XXXX', '2015-12-03');
INSERT INTO account (customer_id, email, credit_card, creation_date)
VALUES (5, 'patel.kimberly@locationcalculator.com', '542802306410XXXX',
        '2009-07-19');
INSERT INTO account (customer_id, email, credit_card, creation_date)
VALUES (5, 'patel.kimberly@talkscope.com', '559652950074XXXX', '2012-02-28');
INSERT INTO account (customer_id, email, credit_card, creation_date)
VALUES (6, 'phillips.valeria@searchingpays.com', '455633472753XXXX',
        '2014-12-12');
INSERT INTO account (customer_id, email, credit_card, creation_date)
VALUES (7, 'rothermel.elizabeth@petroleumfinder.com', '471655147289XXXX',
        '2015-12-03');



/*
 * EMPLOYEE RELATION
 */
INSERT INTO employee (last_name, first_name, address, city, state, zip_code,
                      telephone, ssn, hire_date, wage, manager_id)
VALUES ('SERVICE', 'SELF', 'N/A', 'N/A', 'XX', 'XXXXX', 'XXXXXXXXXX',
        'XXXXXXXXX', '2007-05-01', 0.01, NULL);
INSERT INTO employee (last_name, first_name, address, city, state, zip_code,
                      telephone, ssn, hire_date, wage, manager_id)
VALUES ('Totaro', 'Michael', '15 Cody Ridge Road', 'Lafayette', 'LA', '70506',
        '3374938066', '44419XXXX', '2007-05-01', 38.00, NULL);
INSERT INTO employee (last_name, first_name, address, city, state, zip_code,
                      telephone, ssn, hire_date, wage, manager_id)
VALUES ('Sigdel', 'Purushottam', '304 Kenwood Place', 'Lafayette', 'LA',
        '70504', '3375833335', '26560XXXX', '2008-04-19', 20.00, 2);
INSERT INTO employee (last_name, first_name, address, city, state, zip_code,
                      telephone, ssn, hire_date, wage, manager_id)
VALUES ('Thomason', 'Ruth', '1842 Lowland Drive', 'Scott', 'LA', '70583',
        '3374093644', '33904XXXX', '2015-11-30', 10.00, 2);



/*
 * RESERVATION RELATION
 */
INSERT INTO reservation (booking_date, flight_type, flight_class, price,
                         employee_id)
VALUES ('2009-07-19', 0, 2, 101.00, 2);
INSERT INTO reservation (booking_date, flight_type, flight_class, price,
                         employee_id)
VALUES ('2010-04-04', 1, 2, 806.00, 1);
INSERT INTO reservation (booking_date, flight_type, flight_class, price,
                         employee_id)
VALUES ('2012-02-28', 0, 2, 96.00, 2);
INSERT INTO reservation (booking_date, flight_type, flight_class, price,
                         employee_id)
VALUES ('2013-06-11', 2, 0, 706.20, 2);
INSERT INTO reservation (booking_date, flight_type, flight_class, price,
                         employee_id)
VALUES ('2014-12-12', 0, 2, 642.00, 1);
INSERT INTO reservation (booking_date, flight_type, flight_class, price,
                         employee_id)
VALUES ('2015-04-27', 0, 1, 538.65, 2);
INSERT INTO reservation (booking_date, flight_type, flight_class, price,
                         employee_id)
VALUES ('2015-12-03', 0, 2, 924.00, 3);



/*
 * BID RELATION
 */
INSERT INTO bid (reservation_id, account_id, bid_amount)
VALUES (3, 5, 85.00);
INSERT INTO bid (reservation_id, account_id, bid_amount)
VALUES (3, 5, 90.00);
INSERT INTO bid (reservation_id, account_id, bid_amount)
VALUES (3, 5, 96.00);



/*
 * BOOKED RELATION
 */
INSERT INTO booked (reservation_id, account_id, flight_id, airline_id,
                    flight_order, departure_date, seat_number, special_meal)
VALUES (1, 5, '3108', 'AA', 1, '2009-07-24', 84, 0);
INSERT INTO booked (reservation_id, account_id, flight_id, airline_id,
                    flight_order, departure_date, seat_number, special_meal)
VALUES (2, 5, '5071', 'DL', 1, '2010-05-01', 48, 0);
INSERT INTO booked (reservation_id, account_id, flight_id, airline_id,
                    flight_order, departure_date, seat_number, special_meal)
VALUES (2, 5, '1777', 'DL', 2, '2010-05-01', 53, 0);
INSERT INTO booked (reservation_id, account_id, flight_id, airline_id,
                    flight_order, departure_date, seat_number, special_meal)
VALUES (2, 5, '421', 'UA', 3, '2010-05-09', 77, 0);
INSERT INTO booked (reservation_id, account_id, flight_id, airline_id,
                    flight_order, departure_date, seat_number, special_meal)
VALUES (2, 5, '5598', 'DL', 4, '2010-05-09', 14, 0);
INSERT INTO booked (reservation_id, account_id, flight_id, airline_id,
                    flight_order, departure_date, seat_number, special_meal)
VALUES (3, 6, '3108', 'AA', 1, '2012-03-19', 45, 1);
INSERT INTO booked (reservation_id, account_id, flight_id, airline_id,
                    flight_order, departure_date, seat_number, special_meal)
VALUES (4, 2, '3367', 'AA', 1, '2013-12-20', 150, 6);
INSERT INTO booked (reservation_id, account_id, flight_id, airline_id,
                    flight_order, departure_date, seat_number, special_meal)
VALUES (4, 2, '2250', 'AA', 2, '2013-12-22', 114, 0);
INSERT INTO booked (reservation_id, account_id, flight_id, airline_id,
                    flight_order, departure_date, seat_number, special_meal)
VALUES (4, 2, '9', 'HA', 3, '2013-12-26', 177, 0);
INSERT INTO booked (reservation_id, account_id, flight_id, airline_id,
                    flight_order, departure_date, seat_number, special_meal)
VALUES (5, 7, '3367', 'AA', 1, '2015-01-01', 104, 0);
INSERT INTO booked (reservation_id, account_id, flight_id, airline_id,
                    flight_order, departure_date, seat_number, special_meal)
VALUES (5, 7, '2250', 'AA', 2, '2015-01-01', 41, 0);
INSERT INTO booked (reservation_id, account_id, flight_id, airline_id,
                    flight_order, departure_date, seat_number, special_meal)
VALUES (5, 7, '9', 'HA', 3, '2015-01-01', 62, 2);
INSERT INTO booked (reservation_id, account_id, flight_id, airline_id,
                    flight_order, departure_date, seat_number, special_meal)
VALUES (6, 3, '5071', 'DL', 1, '2015-05-01', 175, 0);
INSERT INTO booked (reservation_id, account_id, flight_id, airline_id,
                    flight_order, departure_date, seat_number, special_meal)
VALUES (6, 3, '1296', 'DL', 2, '2015-05-01', 91, 0);
INSERT INTO booked (reservation_id, account_id, flight_id, airline_id,
                    flight_order, departure_date, seat_number, special_meal)
VALUES (7, 1, '3367', 'AA', 1, '2015-12-12', 84, 0);
INSERT INTO booked (reservation_id, account_id, flight_id, airline_id,
                    flight_order, departure_date, seat_number, special_meal)
VALUES (7, 1, '2250', 'AA', 2, '2015-12-12', 43, 0);
INSERT INTO booked (reservation_id, account_id, flight_id, airline_id,
                    flight_order, departure_date, seat_number, special_meal)
VALUES (7, 1, '9', 'HA', 3, '2015-12-12', 115, 0);
INSERT INTO booked (reservation_id, account_id, flight_id, airline_id,
                    flight_order, departure_date, seat_number, special_meal)
VALUES (7, 4, '3367', 'AA', 1, '2015-12-12', 85, 0);
INSERT INTO booked (reservation_id, account_id, flight_id, airline_id,
                    flight_order, departure_date, seat_number, special_meal)
VALUES (7, 4, '2250', 'AA', 2, '2015-12-12', 44, 0);
INSERT INTO booked (reservation_id, account_id, flight_id, airline_id,
                    flight_order, departure_date, seat_number, special_meal)
VALUES (7, 4, '9', 'HA', 3, '2015-12-12', 116, 0);
INSERT INTO booked (reservation_id, account_id, flight_id, airline_id,
                    flight_order, departure_date, seat_number, special_meal)
VALUES (7, 8, '3367', 'AA', 1, '2015-12-12', 86, 0);
INSERT INTO booked (reservation_id, account_id, flight_id, airline_id,
                    flight_order, departure_date, seat_number, special_meal)
VALUES (7, 8, '2250', 'AA', 2, '2015-12-12', 45, 0);
INSERT INTO booked (reservation_id, account_id, flight_id, airline_id,
                    flight_order, departure_date, seat_number, special_meal)
VALUES (7, 8, '9', 'HA', 3, '2015-12-12', 117, 0);



DISCONNECT ALL;

EXIT;
