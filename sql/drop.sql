--------------------------------------------------------------------------------
-- Bryan Burke, Joshua Lambert, and Elizabeth Rothermel                       --
-- Group 4                                                                    --
-- INFX 499                                                                   --
-- Fall 2015                                                                  --
-- University of Louisiana at Lafayette                                       --
--------------------------------------------------------------------------------

CONNECT 'jdbc:derby:db';

DROP TABLE booked;
DROP TABLE bid;
DROP TABLE reservation;
DROP TABLE employee;
DROP TABLE preference;
DROP TABLE account;
DROP TABLE customer;
DROP TABLE route;
DROP TABLE fare_restriction;
DROP TABLE flight;
DROP TABLE airline;
DROP TABLE airport;

DISCONNECT ALL;

EXIT;
